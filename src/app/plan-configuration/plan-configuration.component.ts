import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FormValidationService } from '../form-validation.service';
import { not } from 'rxjs/internal/util/not';
declare let $: any;

@Component({
  selector: 'app-plan-configuration',
  templateUrl: './plan-configuration.component.html',
  styleUrls: ['./plan-configuration.component.scss',
    '../create-page/create-page.component.scss'
  ]
})
export class PlanConfigurationComponent implements OnInit {

  public form: FormGroup;
  public routepage: any;
  public page: any;
  public operation: any;
  public panels: any;
  public panelData: any;
  public formHandler: any;
  public id: any;
  public responseData: any;
  lookAheadInfo: any;
  userRole: boolean;
  notification: string;

  constructor(public restservice: RestserviceService, private router: Router, public fb: FormBuilder, public formValidation: FormValidationService) {

    this.formHandler = fb;
  }

  /* Initialize the component */
  ngOnInit() {
    this.id = JSON.parse(sessionStorage.getItem("listId"))
    let ss = this.router.url.split('/');
    //
    this.routepage = ss[ss.length - 1];
    this.page = ss[2];

    this.lookAheadInfo = JSON.parse(sessionStorage.getItem('projectDetails'));
    // 
    if (this.lookAheadInfo.projectRole == 'SC-Manager') {
      this.userRole = true;

    } else if (this.lookAheadInfo.projectRole == 'GC-Observer') {
      this.userRole = true;
    } else if (this.lookAheadInfo.projectRole == 'Owner') {
      this.userRole = true;
    } else if (this.lookAheadInfo.projectRole == 'SC-Observer') {
      this.userRole = true;
    } else {
      this.userRole = false;
    }
    this.getJsonDataForProject(this.page, this.routepage);
  }

  /*************************
   * Function for CheckBox 
   * @Param subItem - innerItem
   * @Param panelItems  - panel in configuartion
   * @Param subCheckedItem - checked ,unchecked Item 
   *************************/

  checkChekbox(subItem, panelItems, subCheckedItem?, node?) {

    // Check all checkbox when click select all #1
    if(subCheckedItem) {
      console.log('sub checked item',subCheckedItem.name,'======',subCheckedItem.value);
      if(subCheckedItem.name === 'ProjectTaskSummary' && !subCheckedItem.value) {
        subItem.forEach(item => {
          if(item.name !== 'ProjectTaskSummary') {
            item.value = true;
          }
        });
      } else if(subCheckedItem.name === 'ProjectTaskSummary' && subCheckedItem.value) {
        subItem.forEach(item => {
          if(item.name !== 'ProjectTaskSummary') {
            item.value = false;
          }
        });
      } else if(subCheckedItem.name !== 'ProjectTaskSummary' && subCheckedItem.value) {
        subItem.forEach(item => {
          if(item.name === 'ProjectTaskSummary') {
            item.value = false;
          }
        });
      } 
      if(subCheckedItem !== 'ProjectTaskSummary') {
        let checkedAll = subItem.filter(item => {
          return item.value == true;
        });
        console.log('checkedAll',checkedAll);
        if(checkedAll.length == 3) {
          subItem.forEach(item => {
            if(item.name === 'ProjectTaskSummary') {
              item.value = true;
            }
          });
        }
      }
    }
    
    let val = true;
    setTimeout(() => {
      let x = subItem.filter(item => {
        // 
        return item.value === true;
      });

      // let unChecked = subItem.filter(item => {
      //   return item.value === false && item.name != 'ProjectTaskSummary';
      // });

      // let checkedVal = subItem.filter(item => {
      //   return item.value === true && item.name != 'ProjectTaskSummary';
      // })
      // console.log('sub item,sub',subItem)
      // console.log("unChecked", unChecked)
      // console.log("checkedVal", checkedVal)

      // if (subCheckedItem.name == 'ProjectTaskSummary') {
      //   subItem.forEach(item => {
      //     item.value = true;
      //   });
      // }

      // if (checkedVal.length == 0 ) {

      //   if (subCheckedItem.name == 'ProjectTaskSummary') {
      //     subCheckedItem.value = true;
      //   }
      // }

      // if (unChecked.length > 0) {
      //   subItem.forEach(item => {
      //     if (item.name == "ProjectTaskSummary" && !item.value)
      //       item.value = true;
      //   })
      // }
      // if (checkedVal.length == 4) {
      //   subItem.forEach(item => {
      //     if (item.name == "ProjectTaskSummary")
      //       item.value = true;
      //   })
      // }
      // if (unChecked.length == 0 && subCheckedItem.name == 'ProjectTaskSummary') {
      //   subItem.forEach(item => {

      //     item.value = false;
      //   })
      // }


      /* new logic */
      // let parent = subItem.filter(item =>{
      //   item.node == 'parent'  && item.value; 
      // })
      // console.log("parent", parent);
      // let child = subItem.filter(item =>{
      //   item.node == 'child' && item.value;
      // })
      // console.log("child", child);

     
      
      // if (node == 'parent' && subCheckedItem.value) {
      //   subItem.forEach(item => {
      //     if (item.node == 'child') {
      //       item.value = true;
      //     }
      //   })
      // }
      // else {
      //   subItem.forEach(item => {
      //     if (item.node == 'child') {
      //       item.value = false;
      //     }
      //   })
      // }
      




      /*Disable checkBox if length === 2 for only Offdays*/
      if (x.length === 2 && subItem.length > 4 && panelItems.name == 'offdays') {
        subItem.forEach(sub => {
          /*Set boolean value for check box */
          if (sub.value == '') {
            sub["disableBox"] = true;
          } else {
            sub["disableBox"] = false;
          }
        });
      }
      else {
        subItem.forEach(elseSub => {

          elseSub["disableBox"] = false;
        });
      }

     

    });
  }


  /* Merge Json Data and DB Data */
  getResponseJsonData() {
    // alert("hi")

    let params = {};
    // if(this.id){
    //   params[this.id.field]= this.id.value;
    // }else{
    let project = JSON.parse(sessionStorage.getItem("projectCreate")) ? JSON.parse(sessionStorage.getItem("projectCreate")) : JSON.parse(sessionStorage.getItem("projectDetails"));
    params['projectId'] = project.projectId;
    // }
    this.restservice.getResponseData(this.panelData.fetchRestServiceUrl, params, {}).subscribe(success => {
      // 
      this.responseData = success['payload'];
      this.notification = this.responseData.notification;
      if (this.notification) {
        $("#ProjectTaskSummary").prop("disabled", false);
        $("#ThroughEmail").prop("disabled", false);
        $("#TasksPastsDue").prop("disabled", false);
        $("#TaskInAlert").prop("disabled", false);
        $("#NewlyAddedTask").prop("disabled", false);
        $("#ModifiedTask").prop("disabled", false);
        $("#FromApplication").prop("disabled", false);
        $("#Daily").attr("disabled", false);
        $("#Weekly").attr("disabled", false);
        $("#dayOfWeek").prop("disabled", false);
      } else {
        $("#ProjectTaskSummary").prop("disabled", true);
        $("#ThroughEmail").prop("disabled", true);
        $("#TasksPastsDue").prop("disabled", true);
        $("#TaskInAlert").prop("disabled", true);
        $("#NewlyAddedTask").prop("disabled", true);
        $("#ModifiedTask").prop("disabled", true);
        $("#FromApplication").prop("disabled", true);
        $("#Daily").attr("disabled", true);
        $("#Weekly").attr("disabled", true);
        $("#dayOfWeek").prop("disabled", true);

      }
      this.panels.forEach(item => {

        if (item.type == "simple" || item.type == 'radioButton') {
          item['items'].forEach(panelItems => {
            console.log("panelItems", panelItems)
            for (let obj in this.responseData) {
              console.log("obj ", obj)
              if (panelItems.bindingDbFieldName == obj) {
                //  console.log(panelItems.bindingDbFieldName == obj)
                panelItems['value'] = this.responseData[obj];
              }
            }
          })
        }
        if (item.type == "multichekbox") {
          item['items'].forEach(panelItems => {
            // 
            panelItems.subItem.forEach(subItem => {
              // 
              this.responseData.leaveDays.forEach(leaveDay => {
                // 
                // 
                if (leaveDay == subItem.checkedValue) {
                  subItem.value = true;
                  this.checkChekbox(panelItems.subItem, panelItems);
                }
              })

              if (panelItems.name == "informations") {
                this.responseData.information.forEach(information => {
                  // 
                  // 
                  if (information == subItem.checkedValue) {

                    subItem.value = true;
                  }
                  this.checkChekbox(panelItems.subItem, panelItems);
                })
              }
              else if (panelItems.name == "information") {
                this.responseData.type.forEach(type => {
                  // 
                  if (type == subItem.checkedValue) {
                    subItem.value = true;
                    // this.checkChekbox(panelItems.subItem);
                  }
                });
              }
              //
            })
          });
        }
      })
    }, (err) => {

    });
  }

  /* Rendering the Json  for Project Plan Configuration */
  getJsonDataForProject(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      this.operation = success;
      this.panelData = success;
      this.panels = this.panelData['panels'];
      //
      const projectRole = JSON.parse(sessionStorage.getItem('projectDetails')).projectRole;
      //
      // if (projectRole != 'GC-Project Admin' && projectRole != 'GC-Project Manager') {
      //   success['tools']['ComponentItem'] = [];
      // }
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
      if (this.page == 'edit') {
        this.getResponseJsonData();
      }
    }, (err) => {

    });
  }
}
