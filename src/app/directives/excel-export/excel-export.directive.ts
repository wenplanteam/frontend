import { Directive, Input, ElementRef, HostListener } from '@angular/core';
//import { AscDescPipe } from '../../pipes/asc-desc/asc-desc.pipe'
//import { ExcelExportService } from '../../services/excel-export/excel-export.service'
import { EmitterService } from '../../emitter.service';
import { DatePipe } from '@angular/common';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';


const EXCEL_TYPE = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8';
const EXCEL_EXTENSION = '.xls';

@Directive({
  selector: '[appExcelExport]',
  providers: [DatePipe]
})
export class ExcelExportDirective {

  @Input() exportData: any;
  @Input() exportFilename: any;

  constructor(private datePipe: DatePipe, private emitter: EmitterService) { }

  @HostListener('click') onClick() {
    //this.excelService.exportAsExcelFile(this.exportData, 'sample');
    let json = this.exportData;
    let currentData = new Date();
    // console.log('currentData', currentData);
    let excelFileName = this.exportFilename+'-'+this.datePipe.transform(currentData, 'yyyy-MM-dd');
    //console.log('excelFileName', excelFileName);
    if(this.exportData && this.exportData.length>0){
      this.exportAsExcelFile(this.exportData, excelFileName);
    }else{
      this.emitter.getMessage("No scheduled Task and sub Task in Lookahead");
    }
  }

  public exportAsExcelFile(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = { Sheets: { 'data': worksheet }, SheetNames: ['data'] };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    this.saveAsExcelFile(excelBuffer, excelFileName);
  }

  private saveAsExcelFile(buffer: any, fileName: string): void {
    //console.log('fileName', fileName);
    const data: Blob = new Blob([buffer], {type: EXCEL_TYPE});
    FileSaver.saveAs(data, fileName + EXCEL_EXTENSION);
 }

}
