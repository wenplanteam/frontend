export interface Validator {
  name: string;
  validator: any;
  message: string;
}
export interface FieldConfig {
  label?: string;
  name?: string;
  inputType?: string;
  options?: string[];
  collections?: any;
  type: string;
  value?: any;
  float:string;
  validations?: Validator[];
}
export interface  GanttData{
  taskId:number;
 startDate:string;
 taskName:string;
 float:string;
 taskParent:number;
 scheduledDates:any;
 }
 export interface  GanttDataPrint{
  taskId:number;
 startDate:string;
 taskName:string;
 float:string;
 taskParent:number;
 scheduledDates:any;
 }
