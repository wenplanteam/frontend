FROM httpd:2.4

RUN rm -rf /usr/local/apache2/htdocs/*

RUN rm -rf /usr/local/apache2/conf/httpd.conf

RUN rm -rf /usr/local/apache2/conf/extra/proxy-html.conf

COPY httpd.conf /usr/local/apache2/conf/

COPY proxy-html.conf /usr/local/apache2/conf/extra/

COPY /dist/wenplan /usr/local/apache2/htdocs/

COPY .htaccess /usr/local/apache2/htdocs/.htaccess

COPY mod_deflate.conf /usr/local/apache2/conf/

RUN apachectl configtest && apachectl restart