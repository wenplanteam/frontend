import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import * as moment from 'moment';
import { start } from 'repl';
declare var $: any;

@Component({
  selector: 'app-date-range-picker',
  templateUrl: './date-range-picker.component.html',
  styleUrls: ['./date-range-picker.component.scss']
})
export class DateRangePickerComponent implements OnInit {
  form: FormGroup
  public _startDate: any;
  public _endDate: any;

  public selectedOption: any = 'Month';
  public selectedArray = ['Month', 'Week', 'Day', 'Date Range'];
  @Input() selectedDateRange: any;
  @Output() dateRangeSelected: EventEmitter<any> = new EventEmitter<any>();
  public project_startDate: string;
  public updatesDate: any;
  public updatedMonth: any;
  public updatedWeek: any;
  public updatedDAy: any;
  // daterangepickerOptions = {
  //   startDate: this._startDate,
  //   endDate: this._endDate,
  //   format: 'DD MMM, YYYY',
  //   labelText: '',
  //   clickOutsideAllowed: false,
  //   position: 'left',
  //   startingToDate: this._endDate,
  //   startingFromDate: this._startDate,
  //   placeholder: "xxxxs",


  // }

  constructor(public fb: FormBuilder) { }

  ngOnInit() {
     let startDate = new Date();
     startDate.setDate(new Date().getDate() + 1);
    this._startDate = startDate;
    let endDate = new Date();
    endDate.setDate(new Date().getDate() + 2);
    this.selectedDateRange = { 'start': moment(), 'end': moment().add('days', 28) };
    console.log("project StartDate",this.project_startDate);
    this._endDate = endDate;
    console.log("this._endDate",this._startDate,this._endDate);
    //   console.log("selectedValue", this.selectedOption)
    //   //.myDateRange =this._startDate;
    //   this.form =this.fb.group({
    //     'myDateRange':['','']
    //   });
     this.form.controls["myDateRange"].setValue({'start':"04 Sep, 2019",'end':"06 Sep, 2019"});
    //   console.log("this.form",this.form);
    //  this.daterangepickerOptions['startingToDate']=moment(new Date());
    //   this.daterangepickerOptions['startingFromDate']=moment(new Date());
    if (this.selectedOption == 'Month') {
      (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
    } else {
           
      (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'block';
    }
    this.project_startDate = sessionStorage.getItem('startDate');
    setTimeout(() => {
      
      console.log("this.project_startDate", this.project_startDate)
      this.selectedDateRange = { 'start': moment(this.project_startDate), 'end': moment(this.project_startDate).add('days', 28) };
      console.log("selectedDateRange2", this.selectedDateRange);
      this.datesRangeUpdated(this.selectedDateRange);
    }, 300);



  }

  /* rangeSelected(data) {
    let startDate: Date;
    let endDate: Date;
    console.log("selectedValue", this.selectedOption)
    // console.log("data", data);
    console.log("selectedOption", this.selectedOption);
    this._startDate = data.start['_d'];
    this._endDate = data.end['_d'];
    //let ending = this._endDate.setDate(this._startDate.getDate() + 30);
    console.log("startDate", this._startDate)
    console.log("emndate", this._endDate)
    console.log("this.form", this.form);
  } */

  // changeDropdown() {
  //   console.log(".......", this.updatesDate)    // alert(this.selectedOption );
  //   if (this.selectedOption == 'Month') {
  //     // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
  //     $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
  //     this.selectedDateRange = { 'start': moment(this.project_startDate), 'end': moment(this.project_startDate).add('days', 30) };
  //     // console.log("from date",'start');
  //     // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
  //     $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
  //   } else if (this.selectedOption == 'Week') {
  //     this.selectedDateRange = { 'start': moment(this.project_startDate), 'end': moment(this.project_startDate).add('days', 7) };
  //     // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
  //     $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
  //   } else if (this.selectedOption == 'Day') {
  //     this.selectedDateRange = {
  //       'start': moment(this.project_startDate), 'end': moment(this.project_startDate)
  //     };
  //     $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
  //     // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';

  //   } else {
  //     $('md-drppicker').removeClass("hidden").addClass("shown");
  //     // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'block';
  //   }
  //   this.dateRangeSelected.emit(this.selectedDateRange);
  // }

  changeDropdown() {
    console.log(".......", this.updatesDate) // alert(this.selectedOption );
    if (this.selectedOption == 'Month') {
    // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
    $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
    this.selectedDateRange = { 'start': moment(this.project_startDate), 'end': moment(this.project_startDate).add('days', 30) };
    // console.log("from date",'start');
    // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
    $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
    } else if (this.selectedOption == 'Week') {
    this.selectedDateRange = { 'start': moment(this.project_startDate), 'end': moment(this.project_startDate).add('days', 7) };
    // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
    $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
    } else if (this.selectedOption == 'Day') {
    this.selectedDateRange = {
    'start': moment(this.project_startDate), 'end': moment(this.project_startDate)
    };
    // (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'none';
    $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
    } else {
    // let p = this.el.nativeElement.querySelector(".md-drppicker");
    // p.remove('.md-drppicker.hidden').add('.md-drppicker.shown');
    // (document.querySelector('.md-drppicker.shown') as HTMLElement).style.display = 'block';
    $( ".md-drppicker" ).removeClass( "hidden" ).addClass( "shown" );
    
    }
    // .md-drppicker.shown
    this.dateRangeSelected.emit(this.selectedDateRange);
    }
  /**
   * function for navigating previous month, previous week and previousDay 
   * @param previous 
   */
  previousDay(previous) {
    console.log("selcted date", this.project_startDate)
    if (this.selectedOption == 'Month') {
      this.selectedDateRange = { 'start': moment(this.updatedMonth).subtract(28, 'days'), 'end': moment(this.updatedMonth).subtract('days', 0) };
      this.updatedMonth = this.selectedDateRange.start['_d'];
      console.log(this.updatesDate)
    } else if (this.selectedOption == 'Week') {
      this.selectedDateRange = { 'start': moment(this.updatedWeek).subtract('days', 6), 'end': moment(this.updatedWeek).subtract('days', 0) };
      this.updatedWeek = this.selectedDateRange.start['_d'];
    } else if (this.selectedOption == 'Day'){
      this.selectedDateRange = { 'start': moment(this.updatedDAy).subtract('days', 1), 'end': moment(this.updatedDAy).subtract('days', 1) };
      this.updatedDAy = this.selectedDateRange.start['_d'];
    }else{
      (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'block';
    }
    this.dateRangeSelected.emit(this.selectedDateRange);
  }

  nextDay(next) {
    if (this.selectedOption == 'Month') {
      this.selectedDateRange = { 'start': moment(this.updatedMonth).add('days', 0), 'end': moment(this.updatedMonth).add('days', 28) };
      this.updatedMonth = this.selectedDateRange.end['_d'];
    } else if (this.selectedOption == 'Week') {
      this.selectedDateRange = { 'start': moment(this.updatedWeek).add('days', 0), 'end': moment(this.updatedWeek).add('days', 6) };
      this.updatedWeek = this.selectedDateRange.end['_d'];
    } else if (this.selectedOption == 'Day') {
      this.selectedDateRange = { 'start': moment(this.updatedDAy).add('days', 1), 'end': moment(this.updatedDAy).add('days', 1) };
      this.updatedDAy = this.selectedDateRange.end['_d'];
    } else {
      (document.querySelector('.md-drppicker') as HTMLElement).style.display = 'block';
    }
    this.dateRangeSelected.emit(this.selectedDateRange);
  }
  datesRangeUpdated(event) {
    $( ".md-drppicker" ).removeClass( "shown" ).addClass( "hidden" );
    console.log("datesRangeUpdated", event, this.selectedDateRange);
    this.dateRangeSelected.emit(this.selectedDateRange);

  }
}
