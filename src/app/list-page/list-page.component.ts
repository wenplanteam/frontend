import { Component, OnInit, Input, Output, OnDestroy } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';
import { DatePipe, WeekDay } from '@angular/common';
import { CurrencyPipe } from '@angular/common'

declare let $: any;
@Component({
  selector: 'app-list-page',
  templateUrl: './list-page.component.html',
  styleUrls: ['./list-page.component.scss'],
  providers: [CurrencyPipe]
})
export class ListPageComponent implements OnInit {

  public routingId: any;
  public result: any;
  public resultData: any = [];
  public pageData: any;
  public tableData: any;
  public subscription: any;
  public advancedSearch: any;
  public filterData: any;
  public buttonData: any;
  public header: any;
  public filter: any;
  public updateCustom: any;
  public customList: any;
  public deleteData: string[] = [];
  public privileges: any;
  public resultOrg: any;
  public pagination: any;
  public sorting: any = {
    column: 'contactName', //to match the variable of one of the columns
    descending: false
  };
  public filteredData: any;

  /*pagination*/
  public pageSizes: any;
  public currentPageNo: any;
  public lastPage: any;
  public totalPage: any;
  public recordCountPerPage = 10;
  public totalRecords: any;
  public basicSearchItem: any;
  public searchItem: any = {};
  public page: any;
  public responseSampleData: any;
  public fetchRestServiceUrl: any;
  public uiBefor: any;
  public uiAfter: any;
  public getJsonData: any;
  public serviceBefore: any;
  public serviceAfter: any;
  public pageName: any;
  public msgSuc: boolean = true;
  public msg: any = '';
  public details: any;
  public tools: any;
  public licenseCount: any;
  public createdUserId: any;
  public superAdminLogin: boolean = false;
  pager: any;

  constructor(public restservice: RestserviceService, private datePipe: DatePipe, private currencyPipe: CurrencyPipe,
    public router: Router, public route: ActivatedRoute, public emitter: EmitterService) { }

  ngOnInit() {
    if (this.router.url.match("list") && JSON.parse(sessionStorage.getItem('loginDetails'))['roleName'] == 'SUPERADMIN') {
      this.superAdminLogin = true;
      this.emitter.roleDetail(true);
    } else {
      this.emitter.roleDetail(false);
    }
    this.emitter.routerUrlSource.subscribe(item => {
      this.emitter.getDataSource(this.deleteData);
    });
    if (this.router.url.match("list")) {
      this.page = "list";
    }
    let id;
    this.emitter.getRouterUrl();

    this.route.params.subscribe(params => {
      id = params['id'];
      this.routingId = id;
    })
    let localCompany = sessionStorage.getItem('gcCompany');
    if (sessionStorage.getItem('popupGcName') == null || sessionStorage.getItem('popupGcName') == undefined)
      this.emitter.sendGcCompanyFromList(localCompany);
    this.pager = this.emitter.selectedHeaderCompany.subscribe(item => {
      this.createdUserId = item;
      this.getListJson(this.page, id);
    });
    this.emitter.routerUrlSource.subscribe(item => {
      this.emitter.getDataSource(this.deleteData);
    });
    this.subscription = this.emitter.currentMessage.subscribe(item => {
      if (item) {
        this.deleteData = [];
        let pageName = {};
        if (this.routingId == 'project' || this.routingId == 'user-permission') {
          pageName = {
            pageNo: 1,
            pageSize: 10,
            created: this.createdUserId
          } 
        } else {
          pageName = {
            pageNo: 1,
            pageSize: 10
            
          }
          this.searchItem['filters'] = item;
        }
        this.getResponseData(this.fetchRestServiceUrl, pageName, this.searchItem);
      }
    });
    
    this.getJsonData = this.emitter.getResponseData.subscribe(pageItem => {
      
      if (pageItem != ""){
      //   console.log('page item',pageItem);
      // if (this.routingId == 'project' || this.routingId == 'user-permission') {
         this.getResponseData(this.fetchRestServiceUrl, pageItem, {});
      }
    });
  }

  /* Go View Page */
  goView(item) {
    let id = {};
    id['field'] = this.buttonData['checkbox'];
    id['value'] = item[this.buttonData['checkbox']];
    item[this.buttonData['checkbox']];
    sessionStorage.setItem("listId", JSON.stringify(id));
    this.router.navigate(["/authenticate/view/" + this.routingId])
  }
  /* Go Edit Page */

  goEdit(item) {
    let id = {};
    id['field'] = this.buttonData['checkbox'];
    id['value'] = item[this.buttonData['checkbox']];
    item[this.buttonData['checkbox']];
    sessionStorage.setItem("listId", JSON.stringify(id));
    this.router.navigate(["/authenticate/edit/" + this.routingId])
  }

  getListJson(pageName?, id?) {
    this.restservice.getRenderingJson(pageName, id).subscribe(success => {
      this.tools = success['tools'];
      let result = success;
      this.fetchRestServiceUrl = result['fetchRestServiceUrl'];
      this.filterData = result['filter'];
      this.buttonData = result;
      result['panels'].forEach(item => {
        this.header = item.items;
        this.result = item;
      })
      this.pageData = result;
      if (success) {
        let pageName = {};
        if (this.routingId == 'project' || this.routingId == 'user-permission') {
          pageName = {
            pageNo: 1,
            pageSize: 10,
            created: this.createdUserId
          }
        } else {
          pageName = {
            pageNo: 1,
            pageSize: 10
          }
        }
        this.getResponseData(this.fetchRestServiceUrl, pageName, {});
      }
    }, err => { });
  }

  getLicenseCount() {
    this.restservice.getLicenseCount().subscribe(success => {
      this.licenseCount = success['payload'];
    }, (err) => {
      console.log('err', err);
    });
  }

  getResponseData(fetchRestServiceUrl, pageName, searchItem) {
    //this.routingId = this.routingId.split('-')[0];
    // get href
    var url= window.location.href.split('/');
    var urlName = url[url.length-1];
    urlName = urlName.split('-')[0];



    if( (fetchRestServiceUrl != undefined) && (fetchRestServiceUrl.includes(urlName)) ){
      console.log('working URL' ,fetchRestServiceUrl)
       this.restservice.getResponseData(fetchRestServiceUrl, pageName, searchItem).subscribe(success => {
        this.filteredData = success['payload'];
        console.log('1');
        if (this.filteredData) {
          this.filteredData.forEach(item => {
            if (this.routingId == 'user-permission') {
              item['fullName'] = item.firstName + ' ' + item.lastName;
            }
            if (this.routingId == 'plan') {
              if (item.amount) {
                item['monthly'] = this.currencyPipe.transform(item.amount.monthly, 'USD');
                item['annually'] = this.currencyPipe.transform(item.amount.annually, 'USD');
              }
            }
            if (item.expiryDate) {
              item.expiryDate = this.datePipe.transform(item.expiryDate, 'dd-MMM-yyyy');
            }
            if (item.givenTo) {
              if (item.givenTo == JSON.parse(sessionStorage.getItem('loginDetails')).email) {
                item.givenTo = 'Self';
              }
            } else {
              item.givenTo = 'Nobody';
            }
            if (item.gotFrom == JSON.parse(sessionStorage.getItem('loginDetails')).email) {
              item.gotFrom = 'Owner';
            }
          });
        }
        this.pagination = success['pageable'];
        if (success['pageable']) {
          this.emitter.getPagination(success['pageable']);
        }
        let store = [];
        if (success['message'] == 'Record Found' || (success['payload'] && success['payload'].length > 0)) {
          this.msg = success['message'];
          this.msgSuc = true;
        }
        if (success['payload']) {
          this.resultOrg = success['payload'];
          this.result['responseObj'] = success['payload'];
          if (this.routingId == 'license') {
            this.getLicenseCount();
          }
        }
        if (this.routingId == 'license') {
          this.details = success['licenses'];
        } else {
          this.details = '';
        }
      }, err => {
        if (err['error']['message'] == 'Record Not Found') {
          this.msgSuc = false;
          this.msg = err['error']['message'];
        }
        if (err['error']['message'] == 'No Records') {
          this.msgSuc = false;
          this.msg = err['error']['message'];
        }
      });
    }
  }

  getArrangeData(result) {
    let store = [];
    result.forEach(itemData => {
      let detailsArr = [];
      this.result.items.forEach((item, key) => {
        let obj = {};
        obj['field'] = item.field;
        if (item.visible)
          obj['visible'] = item.visible;
        if (item.inlineEdit)
          obj['inlineEdit'] = item.inlineEdit;
        obj["value"] = itemData[item.field];
        detailsArr.push(obj);
      });
      store.push(detailsArr);
      return store;
    })
  }

  getContactData(value) {
    let index = this.deleteData.indexOf(value);
    if (index == -1) {
      this.deleteData.push(value);
    } else {
      this.deleteData.splice(index, 1);
    }
    this.emitter.getDataSource(this.deleteData);
  }
  selectedClass(columnName): string {
    let asd = columnName == this.sorting.column ? 'sorting-' + (this.sorting.descending == true ? 'desc' : 'asc') : 'sorting';
    return asd;
  }
  checkFilter(e) {
    if (e.target.checked) {
    }
  }

  getPaginationData(event) {
    
    let search = {};
    if (this.basicSearchItem) {
      search = this.basicSearchItem;
    } else {
      search = this.searchItem;
    }
    this.getResponseData(this.fetchRestServiceUrl, event, this.searchItem);
    console.log("event om list", this.fetchRestServiceUrl);
    console.log("event on list", event);
  }
  getFilter(event) {
    let pageName;
    if (event.project) {
      pageName = {
        pageNo: 1,
        pageSize: 10,
        projectStatus: event.project.active ? true : false,
        created: this.createdUserId
      };
    } else {
      pageName = {
        pageNo: 1,
        pageSize: 10,
        status: event[this.routingId] ? Object.keys(event[this.routingId])[0] : 'valid',
        created: this.createdUserId
      };
    }
    this.searchItem['filters'] = event;
    this.getResponseData(this.fetchRestServiceUrl, pageName, this.searchItem);
  }
  ngOnDestroy() {
    this.getJsonData.unsubscribe();
    this.pager.unsubscribe();

  }

}
