import { Component, OnInit } from '@angular/core';
import { EmitterService } from '../emitter.service';

@Component({
  selector: 'app-project-journey',
  templateUrl: './project-journey.component.html',
  styleUrls: ['./project-journey.component.scss']
})
export class ProjectJourneyComponent implements OnInit {

  constructor(public emitter: EmitterService) { }

  ngOnInit() {
    this.emitter.currentOperation.subscribe(item=>{
      //console.log("items currentOperation",item);
    })
  }

}
