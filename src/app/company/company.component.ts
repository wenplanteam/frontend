import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {FormValidationService} from '../form-validation.service';

declare let $: any;

@Component({
  selector: 'app-company',
  templateUrl: './company.component.html',
  styleUrls: ['./company.component.scss',
  '../user/user.component.scss'
]
})
export class CompanyComponent implements OnInit {

  public form: any;
  public routepage: any;
  public page: any;
  public operation: any;
  public panels: any;
  public panelData: any;
  public formHandler: any;
  public availableCompany: any;
  public addedCompany = {
    'deleteCompanies': [],
    'projectCompanies': [],
    'projectId': 0
  };
  public searchData: any;
  public newCompany: any;
  public projectDetails: any;
  public constructors: any;
  public companyAssociate: any;
  public companyChecked = false;
  public operationBtn: any;


  constructor(public restservice: RestserviceService, private router: Router,
    public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    const ss = this.router.url.split('/');
    this.routepage = ss[ss.length - 1];
    this.page = ss[2];
    this.routepage = ss[4];
    console.log('this.page',this.routepage)
    this.getJsonDataForProject(this.page, 'add-company');
    this.restservice.getMasterData().subscribe(success => {
      this.constructors = success['payload']['company_role'];
    }, (err) => {
      console.log('err', err);
    });
    if (this.projectDetails) {
      this.getCompanyById(this.projectDetails.projectId);
    } else {
      const id = JSON.parse(sessionStorage.getItem('listId'));
      this.getCompanyById(id.value);
    }
    this.getAllCompany();
  }

  editCompany(id) {
    const param = {'field': 'id', 'value': id };
    //console.log('param', param);
    sessionStorage.setItem('listId', JSON.stringify(param));
    this.router.navigate(['/authenticate/edit/project-company']);
  }

  getCompanyById(projectId) {
    this.addedCompany.projectId = projectId;
    const payload = {
      projectId: projectId
    };
    this.restservice.company(payload).subscribe(success => {
      //console.log('success in id', success);
      if (success) {
        success['payload']['projectCompanies'].forEach(item => {
          if (item.name == JSON.parse(sessionStorage.getItem('loginDetails')).companyName) {
            item['isProjectCompany'] = true;
          }
        });
        if(sessionStorage.getItem('addedCompany') == undefined) {
        this.addedCompany['projectCompanies'] = success['payload']['projectCompanies'];
        } else {
          this.companyAssociate = JSON.parse(sessionStorage.getItem('addedCompany'));
          this.addedCompany['projectCompanies'] = this.companyAssociate['projectCompanies'];
        }
        sessionStorage.setItem('projectCompany', JSON.stringify(this.addedCompany));
        this.checkAddedCompany();
      }
    }, (err) => {
      console.log('err', err);
    });
  }

  constractorChange(item, index) {
    this.addedCompany.projectCompanies[index] = item;
    sessionStorage.setItem('projectCompany', JSON.stringify(this.addedCompany));
  }
  getAllCompany() {
    const data = {
      'createdBy': true
    };
    this.restservice.company(data).subscribe(success => {
      //console.log('success all company', success);
      if (success) {
        this.availableCompany = success['payload'];
        this.checkAddedCompany();
      }
    }, (err) => {
      console.log('err', err);
    });
  }

  searchCompany(companyName) {
    const tableRowsClass = $('.available-comapany-row td .search-value');
    const ss = $('.available-comapany-row');
    const tableBody = $('.no-result-found');
    tableRowsClass.each( function(i, val) {
      var rowText = $(val).text().toString().toLowerCase();
      var inputText = companyName.toString().toLowerCase();
      if ( rowText.indexOf( inputText ) == -1 ) {
        ss.eq(i).hide();
      } else {
        ss.eq(i).show();
      }
    });
    if (tableRowsClass.children(':visible').length == 0) {
      //console.log('companyName', companyName);
      sessionStorage.setItem('newCompany', companyName);
        $('#companyNotFound').modal('show');
      //tableBody.append('<div class="no-entries">No entries found.</div>');
    } else {
      $('#companyNotFound').modal('hide');
      $('.no-entries').remove();
    }
  }

  clearCompany() {
    this.newCompany = '';
    this.searchCompany(this.newCompany);
  }

  getCreateCompanyJson() {
    $('#companyNotFound').modal('hide');
    this.restservice.getRenderingJson('create', 'company').subscribe(success => {
      //console.log('success create Company', success);
      this.panelData = success;
      this.panels = success['panels'];
      this.operationBtn = success['tools'];
      //console.log('this.panels', this.panels);
      this.panels.forEach(item => {
        //console.log('item', item);
        item['items'].forEach(element => {
          //console.log('element', element);
          if (element.bindingDbFieldName == 'name') {
            element.value = this.newCompany;
          }
        });
      });
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
    $('#createCompanyPopup').modal('show');
  }

  getJsonDataForProject(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      this.operation = success;
      this.panelData = success;

      const x = this.router.url.split('/');
      if (x.length === 5) {
        const projectRole = JSON.parse(sessionStorage.getItem('projectDetails')).projectRole;
        //console.log('projectRole', projectRole);
        if (projectRole != 'GC-Project Admin' && projectRole != 'GC-Project Manager') {
          success['tools']['ComponentItem'] = [];
        }
    }
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }

  addCompany(item) {
    item['projectRole'] = 'Sub Contractor';
    this.addedCompany['projectCompanies'].push(item);
    sessionStorage.setItem('addedCompany', JSON.stringify(this.addedCompany));
    sessionStorage.setItem('routing','true');
    sessionStorage.setItem('projectCompany', JSON.stringify(this.addedCompany));
    this.checkAddedCompany();
  }

  removeCompany(index) {
    const id = this.addedCompany['projectCompanies'][index].id;
    this.addedCompany['projectCompanies'].splice(index, 1);
    this.addedCompany['deleteCompanies'].push(id);
    sessionStorage.setItem('addedCompany', JSON.stringify(this.addedCompany));
    sessionStorage.setItem('projectCompany', JSON.stringify(this.addedCompany));
    this.checkAddedCompany(id);
  }

  checkAddedCompany(id?) {
    if (id) {
      this.availableCompany.forEach(item => {
        if (item.id == id) {
          item['isAvailable'] = false;
        }
      });
    } else {
      //console.log('this.addedCompany["projectCompanies"]', this.addedCompany['projectCompanies']);
      //console.log('this.availableCompany', this.availableCompany);
      if (this.availableCompany) {
        this.addedCompany['projectCompanies'].forEach(item => {
          this.availableCompany.forEach(availableItem => {
            //console.log('availableItem', availableItem);
            if (item.id == availableItem.id) {
              availableItem['isAvailable'] = true;
            }
          });
        });
        this.companyChecked = true;
      }
    }
    //console.log('this.companyChecked', this.companyChecked);
  }
  ngOnDestroy() {
    // sessionStorage.removeItem('associatedUser');
    // console.log("this.router.url ng destroy",this.router.url);
    if(this.router.url != '/authenticate/edit/project/company' && this.router.url != '/home')  {
      // console.log('i am not in this url');
      sessionStorage.removeItem('addedCompany');
    }
    // return false;
  }
}
