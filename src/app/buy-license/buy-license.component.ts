import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { RestserviceService } from '../restservice.service';
import { environment } from '../../environments/environment';
import { EmitterService } from '../emitter.service';


//declare let google: any;

declare var StripeCheckout: any;
declare let $: any;

@Component({
  selector: 'app-buy-license',
  templateUrl: './buy-license.component.html',
  styleUrls: ['./buy-license.component.scss']
})
export class BuyLicenseComponent implements OnInit {

  public planTypes: any;
  public year: any;
  public billingCycle: any;
  public noLicense: any;
  public total: any;
  public expiryDate: any;
  public that: any;
  public totalAmount: any;
  public stripeKey: any;
  public handler: any;
  public noOfLicense: any;
  public txn_id: any;
  public transactionId: any;
  public licenseCount: any;

  constructor(private datePipe: DatePipe, public restservice: RestserviceService,
    private router: Router, public emitter: EmitterService) {
    this.that = this;
  }

  ngOnInit() {
    const data = {
      'paid': true
    };
    this.restservice.buyPlan(data).subscribe(success => {
      console.log('success buyPlan', success);
      if (success) {
        success['payload'].forEach(item => {
          console.log('item oninit', item);
          item.noOfLicense = 0;
          if (item.periodicName === 'month') {
            item['billingCycle'] = 'Monthly';
          } else {
            item['billingCycle'] = 'Annually';
          }
        });
        this.planTypes = success['payload'];
        console.log('this.planTypes', this.planTypes);
        this.singalLicenseTotal();
      }
    }, (err) => {
      console.log('err', err);
    });

    this.noLicense = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25];
    this.year = [1];
    this.billingCycle = ['Monthly', 'Annually'];
  }



  calExpiryDate(item) {
    console.log('item calExpiryDate', item);
    // const date = new Date();
    // let year;
    // let month;
    // if (item.billingCycle === 'Annually') {
    //   year = date.getFullYear() + 1;
    //   month = date.getMonth();
    // } else {
    //   year = date.getFullYear();
    //   month = date.getMonth() + 1;
    // }
    // let finaldate = date.setDate(date.getDate() - 1);
    // const fulldate = month + '-' + date.getDate() + '-' + year;
    let payload = {
      "billingCycle":item.billingCycle
    }
    this.restservice.getExpiryDate(payload).subscribe(expSucc => {
      console.log('expSucc', expSucc);
      item.expiryDate = this.datePipe.transform(expSucc['payload']['expiryDate'], 'dd-MMM-yyyy');
    }, (expErr) => {
      console.log('expErr', expErr);
    })
  }


  singalLicenseTotal(data?) {
    //console.log('data', data);
    if (data) {
      if (data.billingCycle === 'Annually') {
        data['totalAmount'] = data.amount.annually * (12 * parseInt(data.noOfLicense));
        this.calExpiryDate(data);
      } else {
        data['totalAmount'] = data.amount.monthly * (1 * parseInt(data.noOfLicense));
        this.calExpiryDate(data);
      }
    } else {
      //console.log('this.planTypes', this.planTypes);
      if (this.planTypes) {
        this.planTypes.forEach(item => {
          //console.log('item', item);
          if (item.billingCycle === 'Annually') {
            item['totalAmount'] = item.amount.annually * (12 * item.noOfLicense);
            this.calExpiryDate(item);
          } else {
            item['totalAmount'] = item.amount.monthly * (1 * item.noOfLicense);
            this.calExpiryDate(item);
          }
        });
      }
    }

    let total;
    //console.log('this.planTypes', this.planTypes);
    this.planTypes.forEach(item => {
      if (total) {
        total = total + item.totalAmount;
      } else {
        total = item.totalAmount;
      }
    });
    this.total = total;
  }
  makeLicenseMandatory() {
    //console.log("no of license",this.noOfLicense);
  }
  goToPayment() {
    const reqPay = {
      'amount': this.total,
      'planDTOs': []
    };
    //console.log('reqPay', reqPay);
    let plans;
    this.planTypes.forEach(item => {
      plans = {
        'price': item.totalAmount,
        'noOfLicense': parseInt(item.noOfLicense),
        'billingCycle': item.billingCycle,
        'id': item.id
      };
      reqPay.planDTOs.push(plans);
    });

    this.restservice.goToPayment(reqPay).subscribe(success => {
      if (success) {
        console.log('success goToPayment', success);
        /*console.log('success goToPayment', success);
        const transaction = {
          'transactionId': success['payload'].transactionId
        };
        this.restservice.transaction(transaction).subscribe( succ => {
          console.log('success transaction', success);
          this.router.navigate(['/authenticate/list/license']);
        }, (err) => {
          console.log('err', err);
        });*/
        console.log('success', success);
        this.stripeKey = success['payload']['stripePublicKey'];
        this.transactionId = success['payload']['transactionId'];
        console.log('this.transaction Id', this.transactionId);
        this.handler = StripeCheckout.configure({
          key: this.stripeKey,
          image: 'assets/images/fav_icon_new.png',
          locale: 'auto',
          token: token => {
            this.processPayment(token, this.total)
          }
        });
        this.handlePayment();
      }
    }, (err) => {
      console.log('err', err);
    });
  }
  handlePayment() {
    this.handler.open({
      name: 'app.wenplan.com',
      excerpt: 'Deposit Funds to Account',
      amount: (this.total * 100)
    });
    console.log("this.total", this.total);
  }
  processPayment(token, total) {
    console.log("token", token, this.total);
    const paymentPayload = {
      'amount': this.total,
      'currency': 'USD',
      'stripeToken': token.id,
      'transactionId': this.transactionId
    };
    this.restservice.paymentProcess(paymentPayload).subscribe(success => {
      if (success) {
        console.log("sucess 654", success);
        if (success['success']) {
          this.txn_id = success['payload']['transactionId'];
          this.licenseCount = success['payload']['noOfLicenses'];
          $('#Success').modal('show');
        } else {
          this.emitter.getMessage(success['message']);
        }
      }
    }, (err) => {
      console.log('err', err);
    });
  }
}