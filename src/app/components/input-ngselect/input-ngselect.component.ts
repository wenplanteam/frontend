//import { Component, OnInit } from '@angular/core';
import { Component, OnInit, Input,EventEmitter,Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';

declare let $: any;
@Component({
  selector: 'app-input-ngselect',
  templateUrl: './input-ngselect.component.html',
  styleUrls: ['./input-ngselect.component.scss']
})
export class InputNgselectComponent implements OnInit {

  @Input() inputData: any;
  @Input() pageData: any;
  @Input() form: FormGroup;
  public formHandler: any;
  public companyList: any;
  public validation: any;

  constructor(public fb: FormBuilder, public restservice: RestserviceService) { 
    this.formHandler = fb;
  }

  ngOnInit() {
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    this.getCompany();
  }

  getCompany(){
    this.restservice.getAutocompleteData(this.inputData.restServiceUrl).subscribe( success =>{
      console.log("success getData combobox", success);
      this.companyList =success['payload'];
  });
  }

}
