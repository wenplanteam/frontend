import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../../emitter.service';

@Component({
  selector: 'app-grid-view',
  templateUrl: './grid-view.component.html',
  styleUrls: ['./grid-view.component.scss']
})
export class GridViewComponent implements OnInit {

  @Input() gridViewPanelData: any;
  @Input() form: FormGroup;
  formHandler: any;
  public noResult: any;
  public viewData = [];
  public routingId: any;
  public page: any;
  public deleteData: any;

  constructor(public restservice:RestserviceService, public router: Router, public route:ActivatedRoute, public emitter: EmitterService) { }

  ngOnInit() {
    this.noResult=true;

    console.log("gridViewPanelData dhdgf", this.gridViewPanelData);
    this.noResult=true;
    console.log("this.deleteData", this.deleteData);
    this.emitter.routerUrlSource.subscribe(item=>{
      console.log("item",item);
      this.emitter.getDataSource(this.deleteData);
    });
    //console.log("this.router", this.router.url);
    if(this.router.url.match("list")){
      this.page = "list";
    }
    let id;
    this.emitter.getRouterUrl();
    this.route.params.subscribe(params=>{
      id = params['id'];
      console.log("id",id);
      this.routingId=id;
    })
  }

}
