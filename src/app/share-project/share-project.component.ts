import { Component, OnInit, Input ,EventEmitter,Output,OnDestroy } from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';
import {FormValidationService} from '../form-validation.service';

declare let $:any;
@Component({
  selector: 'app-share-project',
  templateUrl: './share-project.component.html',
  styleUrls: ['./share-project.component.scss'],
  providers: [FormBuilder]
})
export class ShareProjectComponent implements OnInit {
  form: FormGroup;
  public panelData: any;
  public formHandler: any;
  public panels: any;
  public page: any;
  public routepage: any;
  public panelItems: any;
  public operation:any;
  public formObj;
  public validation: any;
  public shareMember:any = [];
  public id: any;
  public userDetails: any;
  public deleteMember: any= [];
  public countShareMember:any;
  public shareToPeopleData:any;
  //public emptyprojecttext:any;
  //public item:any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router , private route: ActivatedRoute,public emitter: EmitterService,public fb:FormBuilder,public formValidation:FormValidationService) {
    this.formHandler =fb;
   }

  ngOnInit() {
    
  //  console.log("this.id",this.id);
  //  console.log("panelData",this.panelData);
    
    this.shareToPeopleData =this.emitter.shareToPeopleData().subscribe((share)=>{
      console.log("share", share);
      if(share !=''){
        let existEmail=this.shareMember.filter(data=>{
          return data.email==share['email'];
        })
        console.log("existEmail",existEmail);
        if(existEmail.length==0){
          if(share['lookaheadAccess'].indexOf('View')==-1){
            share['lookaheadAccess'].push('View');
          }
          this.shareMember.push(share);
          this.emitter.shareToPeople('');
        //  console.log("shareMember",this.shareMember);
          this.panelData['panels'][0]['items'].forEach(item =>{
            //console.log("item", item);
            setTimeout(()=>{
            if(item.bindingDbFieldName == 'lookaheadAccess'){
              item.value = [{"itemName":"View"}];
            }
            });
          })
        } else{
          alert('Email Exist');
          return false;
        }
      }
      // this.panelData.forEach(item =>{
      //   console.log("item", item);
      // })
    });

    // this.emitter.sharedToPanel.subscribe((panelData)=>{
    //   console.log("panelData", panelData);
    //   if(panelData){
    //     //panelData['value'] = '';
        
    //   }
    // });

    this.emitter.userData.subscribe((item) =>{
    //  console.log("item",item);
      this.countShareMember =item.length;
      this.shareMember = item;
    //  console.log("userDetails",this.shareMember);
    })

    this.emitter.getRouterUrl();
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    this.route.params.subscribe(params => {
      this.routepage = params.id;
    });

    if(this.router.url.match("create")){
      this.page = "create";
    }
      this.getJsonDataForshareProject(this.page, 'shareProject');
  }
   
  onhandleUserUpdated(e) {
    console.log(e)
    this.notifyPanel.emit(e);
  }

/* Remove visible false from rendering json*/

removeData(item, index){
//  console.log("this.shareMember[index]",this.shareMember[index],this.shareMember,index);
  if(this.shareMember[index].id)
    this.deleteMember.push(this.shareMember[index].id);
    this.shareMember.splice(index, 1);
    this.shareMember = this.shareMember;
}

removeButton(shareMemberIndex, itemIndex){
  this.shareMember[shareMemberIndex].lookaheadAccess.splice(itemIndex, 1); 
}
// contentHide(){
// $(".emptyprojecttext").hide();
// }

removeByAttr(arr, attr, value){
  var i = arr.length;
  while(i--){
     if( arr[i] 
         && arr[i].hasOwnProperty(attr) 
         && (arguments.length > 2 && arr[i][attr] === value ) ){
         arr.splice(i,1);
     }
  }
  return arr;
}

/* Get UI Rendering JSON for create from Server */

getJsonDataForshareProject(operation, pageName){
  this.restservice.getRenderingJsonForShare(operation, pageName).subscribe(success =>{
    const buttonData = success;
    //const buttonData = JSON.parse(success['payload']);
    this.operation = buttonData;
  //  console.log("this.operation",this.operation);
    this.panelData =buttonData;
  //console.log("this.panelData", this.panelData);
    this.panelData.panels.forEach(item =>{
      this.removeByAttr(item.items, 'visible', '');
    })
      this.panels=this.panelData['panels'];
 //  console.log("this.panels", this.panels);
    this.form =this.formHandler.group(this.formValidation.toFormGroup(this.panels));
  }, (err) =>{
    console.log("err", err);
  });
}

send(panelData){   //passing the json payload for proj create 
  let sharePayload = {};
  this.id = JSON.parse(sessionStorage.getItem("project"));
  //sharePayload['deleteUserDTO'] = [];
  sharePayload['deleteUsers'] = this.deleteMember;
  sharePayload['projectId'] = this.id;
 // console.log("this.id",this.id);
  let projectUsers=[];
 // console.log("shareMember",this.shareMember);
  this.shareMember.forEach(item=>{
  //  console.log("item", item);
    let projectUsersObj={};
    projectUsersObj['firstName'] = item.firstName;
    projectUsersObj['lastName'] = item.lastName;
    projectUsersObj['email'] = item.email;
    let lookaheadAccess = [];
    item.lookaheadAccess.forEach(lookAheadItem=>{
  //    console.log("lookAheadItem", lookAheadItem);
      lookaheadAccess.push(lookAheadItem);
    });
    projectUsersObj['lookaheadAccess'] = lookaheadAccess;
    projectUsers.push(projectUsersObj);
    //sharePayload['projectUsers']['lookaheadAccess'] = lookaheadAccess;
  });
  sharePayload['projectUsers']=projectUsers;
//  console.log("sharePayload", sharePayload);
  // sharePayload['projectUsers']=this.shareMember;  
//  console.log("panelData", panelData);
    //  console.log("sharePayload", sharePayload);
      this.restservice.shareProject(sharePayload, panelData.restServiceUrl).subscribe(success =>{
        console.log("success share",success);
        if(success){
          this.emitter.getMessage(success['message']);
         // this.emitter.getActivatedRoute();
         this.deleteMember = [];
         this.router.navigate(['/authenticate/list/project']);
        }
      }, (err)=>{
        console.log("err", err);
      })
}

/* Add custom Field */
add(event){
  this.panels.panels.push(this.panels.panels[0]);
}
reset(){
  //this.form.get("optionsModel").item([]);
}

clickMethod() {
  if(this.countShareMember!=this.shareMember.length){
    if(confirm("Would you like to save the changes?")) {
      this.send(this.panelData);
      //alert("Email Send Successfully");
    }
  }
}
ngOnDestroy() {
  this.shareToPeopleData.unsubscribe();
}
}
