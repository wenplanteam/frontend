import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { HeaderComponent } from './header/header.component';
import { HttpClientModule } from '@angular/common/http';
import { NgZone } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgbPaginationModule, NgbAlertModule} from '@ng-bootstrap/ng-bootstrap';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal'
//import { NgxDateRangeModule } from 'ngx-daterange';
import {NgxDaterangepickerMd} from 'ngx-daterangepicker-material'
import { OperationComponent } from './operations/operation.component'
import { ComponentsComponent } from './components/components.component';
import { PanelsComponent } from './components/panels/panels.component';
import { DatepickerComponent } from './components/datepicker/datepicker.component';
import { InputTextboxComponent } from './components/input-textbox/input-textbox.component';
import { InputTextareaComponent } from './components/input-textarea/input-textarea.component';
import { InputSelectboxComponent } from './components/input-selectbox/input-selectbox.component';
import { SimplePanelComponent } from './components/simple-panel/simple-panel.component';
import { SimpleListPanelComponent } from './components/simple-list-panel/simple-list-panel.component';
import { ListPageComponent } from './list-page/list-page.component';
import { ButtonComponent } from './components/button/button.component';
import { FormBuilder } from '@angular/forms';
import { InputCheckboxComponent } from './components/input-checkbox/input-checkbox.component';
import { LoginComponent } from './login/login.component';
import { AuthenticateComponent } from './authenticate/authenticate.component';
import { EmitterService } from './emitter.service';
import { LandingComponent } from './landing/landing.component';
import { InputComboboxComponent } from './components/input-combobox/input-combobox.component';
import { CreatePageComponent } from './create-page/create-page.component';
import { EditPageComponent } from './edit-page/edit-page.component';
import { LoaderComponent } from './loader/loader.component';
import { PaginationComponent } from './components/pagination/pagination.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AuthService } from './auth.service';
import { FormValidationService } from './form-validation.service';
import { RedirectComponent } from './redirect/redirect.component';
import { FilterComponent } from './components/filter/filter.component';
import { OrderByPipe } from './order-by.pipe';
import { RegistrationComponent } from './registration/registration.component';
import { GridListComponent } from './components/grid-list/grid-list.component';
import { GanttChartComponent } from './components/ganttchart/ganttchart.component';
import { GanttServices } from './services/gantt.services';
import { DatePipe } from '@angular/common';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ShareProjectComponent } from './share-project/share-project.component';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown';
import { InputMultiselectComponent } from './components/input-multiselect/input-multiselect.component';
import { MyDatePickerModule } from 'mydatepicker';
import { GridViewComponent } from './components/grid-view/grid-view.component';
import { ViewPageComponent } from './view-page/view-page.component';
import { NgSelectModule } from '@ng-select/ng-select';
import { MasterService, OnlyLoggedInUsersGuard, ProjectCompanyService } from './master.service';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { ChangePasswordComponent } from './change-password/change-password.component';
import { SidenavbarComponent } from './sidenavbar/sidenavbar.component';
import { ProjectJourneyComponent } from './project-journey/project-journey.component';
import { PlanConfigurationComponent } from './plan-configuration/plan-configuration.component';
import { CompanyComponent } from './company/company.component';
import { InputNgselectComponent } from './components/input-ngselect/input-ngselect.component';
import { EditProfileComponent } from './edit-profile/edit-profile.component';
import { HomeComponent } from './home/home.component';
import { BuyLicenseComponent } from './buy-license/buy-license.component';
import { PaymentHistoryComponent } from './payment-history/payment-history.component';
import { UserComponent } from './user/user.component';
import { TradeComponent } from './trade/trade.component';
import { InputNginputComponent } from './components/input-nginput/input-nginput.component';
import { GcCompanyListComponent } from './gc-company-list/gc-company-list.component';
import { CaptchaComponent } from './components/captcha/captcha.component';
import { ExcelExportDirective } from './directives/excel-export/excel-export.directive';
import { AscDescPipe } from './pipes/asc-desc/asc-desc.pipe';
import { NgxSpinnerModule } from 'ngx-spinner';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { ExpiredLicenseComponent } from './expired-license/expired-license.component';
import { InputToggleSwitchComponent } from './components/input-toggle-switch/input-toggle-switch.component';
import { InputRadioButtonComponent } from './components/input-radio-button/input-radio-button.component';
import { SpecDivisionComponent } from './spec-division/spec-division.component';
import * as bootstrap from "bootstrap";
import { PercentageCompletionComponent } from './components/percentage-completion/percentage-completion.component';
import {DateRangePickerComponent} from './components/date-range-picker/date-range-picker.component';
import { ImportFileComponent } from './import-file/import-file.component';



const appRoutes: Routes = [
  { path: '', redirectTo: 'landing', pathMatch: 'full' },
  {
    path: 'authenticate', component: AuthenticateComponent, canActivateChild: [OnlyLoggedInUsersGuard], children: [
      { path: 'list/:id', component: ListPageComponent },
      { path: 'view/:id', component: ViewPageComponent },
      { path: 'create/plan', component: CreatePageComponent },
      { path: 'create/user-permission', component: CreatePageComponent },
      { path: 'share', component: ShareProjectComponent },
      {
        path: 'create/project', component: ProjectJourneyComponent, children: [
          { path: '', redirectTo: 'details', pathMatch: 'full' },
          { path: 'details', component: CreatePageComponent },
          { path: 'configuration', component: PlanConfigurationComponent },
          { path: 'company', component: CompanyComponent },
          { path: 'user', component: UserComponent },
          { path: 'new-company', component: CreatePageComponent }
        ]
      },
      { path: 'edit/plan', component: EditPageComponent },
      { path: 'edit/user-permission', component: EditPageComponent },
      { path: 'edit/project-user', component: EditPageComponent },
      { path: 'edit/company', component: EditPageComponent },
      { path: 'edit/project-company', component: EditPageComponent },
      {
        path: 'edit/project', component: ProjectJourneyComponent, children: [
          { path: '', redirectTo: 'details', pathMatch: 'full' },
          { path: 'details', component: EditPageComponent },
          { path: 'configuration', component: PlanConfigurationComponent },
          { path: 'company', component: CompanyComponent },
          { path: 'new-company', component: EditPageComponent },
          { path: 'new-company/:id', component: EditPageComponent },
          { path: 'user', component: UserComponent },
          { path: 'trade', component: TradeComponent },
          { path: 'specDivision', component: SpecDivisionComponent },
          { path: 'import-data', component: ImportFileComponent}
        ]
      },
      {
        path: 'lookaheadplan', component: GanttChartComponent, resolve: {
          "masterData": MasterService,
          "projectCompanyDetails": ProjectCompanyService
        }
      },
      { path: 'edit-profile', component: EditProfileComponent },
      { path: 'changePassword', component: ChangePasswordComponent },
      { path: 'payment-history', component: PaymentHistoryComponent },
      { path: 'buyLicense', component: BuyLicenseComponent },
      { path: 'effortCompletion',component:PercentageCompletionComponent, resolve: {
        "masterData": MasterService
        
      }
     }
    ]
  },
  { path: 'home', component: HomeComponent },
  { path: 'license-expired', component: ExpiredLicenseComponent },
  { path: 'landing', component: LandingComponent, canActivate: [OnlyLoggedInUsersGuard] },
  { path: 'confirmation', component: ConfirmationComponent },
  { path: 'login', component: LoginComponent, canActivate: [OnlyLoggedInUsersGuard] },
  { path: 'registration', component: RegistrationComponent, canActivate: [OnlyLoggedInUsersGuard] },
  { path: 'forgotPassword', component: ForgotPasswordComponent },
  { path: 'selectGc', component: GcCompanyListComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    OperationComponent,
    ComponentsComponent,
    PanelsComponent,
    DatepickerComponent,
    InputTextboxComponent,
    InputTextareaComponent,
    InputSelectboxComponent,
    SimplePanelComponent,
    ListPageComponent,
    SimpleListPanelComponent,
    ButtonComponent,
    InputCheckboxComponent,
    LoginComponent,
    AuthenticateComponent,
    InputCheckboxComponent,
    LandingComponent,
    InputComboboxComponent,
    CreatePageComponent,
    EditPageComponent,
    LoaderComponent,
    PaginationComponent,
    RedirectComponent,
    FilterComponent,
    OrderByPipe,
    RegistrationComponent,
    GridListComponent,
    GanttChartComponent,
    ForgotPasswordComponent,
    ShareProjectComponent,
    InputMultiselectComponent,
    GridViewComponent,
    ViewPageComponent,
    ConfirmationComponent,
    ChangePasswordComponent,
    SidenavbarComponent,
    ProjectJourneyComponent,
    PlanConfigurationComponent,
    CompanyComponent,
    InputNgselectComponent,
    EditProfileComponent,
    HomeComponent,
    BuyLicenseComponent,
    PaymentHistoryComponent,
    UserComponent,
    TradeComponent,
    InputNginputComponent,
    GcCompanyListComponent,
    CaptchaComponent,
    ExcelExportDirective,
    AscDescPipe,
    ExpiredLicenseComponent,
    InputToggleSwitchComponent,
    InputRadioButtonComponent,
    SpecDivisionComponent,
    PercentageCompletionComponent,
    DateRangePickerComponent,
    ImportFileComponent
  ],

  imports: [
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(appRoutes, { onSameUrlNavigation: 'reload' }),
    HttpClientModule,
    ReactiveFormsModule,
    MyDatePickerModule,
    AngularMultiSelectModule,
    NgSelectModule,
    NgxSpinnerModule,
    Ng2SearchPipeModule,
    ModalModule.forRoot(),
    NgbModule,
    NgbPaginationModule, NgbAlertModule,
    NgxDaterangepickerMd.forRoot({
      format: 'DD-MMM-YYYY',
  }),
  ],
  exports: [RouterModule],
  providers: [FormBuilder, EmitterService, AscDescPipe,
    BsModalRef,
    FormValidationService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthService,
      multi: true
    }, GanttServices, DatePipe, MasterService, OnlyLoggedInUsersGuard, ProjectCompanyService],
  schemas: [
    CUSTOM_ELEMENTS_SCHEMA
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

