import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';

declare let $: any;
@Component({
  selector: 'app-input-nginput',
  templateUrl: './input-nginput.component.html',
  styleUrls: ['./input-nginput.component.scss']
})
export class InputNginputComponent implements OnInit {

  @Input() inputData: any;
  @Input() pageData: any;
  @Input() form: FormGroup;
  public formHandler: any;
  public companyList: any;
  public validation: any;

  constructor(public fb: FormBuilder, public restservice: RestserviceService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    //ng-tag-label
    console.log('form', this.form);
    console.log('inputData nginput', this.inputData);
    console.log('$(selector).html()', $('span .ng-tag-label').html());
    $('span .ng-tag-label').text('Add ' + this.inputData.label);
    this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
    console.log('this.validation', this.validation);
    this.getCompany();
  }

  getCompany() {
    this.restservice.getAutocompleteData(this.inputData.restServiceUrl).subscribe(success => {
      console.log('success getData combobox', success);
      this.companyList = success['payload'];
      console.log('this.companyList', this.companyList);
    }, (err) => {
      console.log('err', err);
    });
  }

  changeCompany(inputData, changedVal){
    console.log('inputData', inputData);
    console.log('changedVal', changedVal);
    console.log('pageData', this.pageData);
    this.pageData.forEach(element => {
      console.log('element', element);
      element.items.forEach(item => {
        if(item.bindingDbFieldName == 'companyId'){
          item.value = changedVal.id
        }
      });
    });
  }

}
