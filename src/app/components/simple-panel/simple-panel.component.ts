import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder,FormControl,Validator }  from '@angular/forms';
import { RestserviceService } from '../../restservice.service';

@Component({
  selector: 'app-simple-panel',
  templateUrl: './simple-panel.component.html',
  styleUrls: ['./simple-panel.component.scss']
})
export class SimplePanelComponent implements OnInit {

  @Input() simplepanelData: any;
  @Input() panelData: any;
  @Input() form: FormGroup;

  constructor(public fb:FormBuilder,public restservice: RestserviceService) {
   }

  ngOnInit() {
    
    setTimeout(()=>{
      this.simplepanelData.items.forEach(item =>{
        if(this.simplepanelData['module']){
          for (let response in this.simplepanelData['module']){
               if(item.bindingDbFieldName == response){
                 item.value = this.simplepanelData['module'][response];
               }
           }
        } 
        if(this.simplepanelData['customFields']){
          for (let response in this.simplepanelData['customFields']){
               if(item.bindingDbFieldName == response){
                 item.value = this.simplepanelData['customFields'][response];
               }
           }
        }
        if(this.simplepanelData[this.simplepanelData['reponseObj']] instanceof Array){
          if(this.simplepanelData[this.simplepanelData['reponseObj']].length>1){
            this.simplepanelData[this.simplepanelData['reponseObj']].forEach(arrayItem =>{
              for (let response in arrayItem){
                if(item.bindingDbFieldName == response){
                  if(arrayItem[item.bindingDbFieldName]!="" && arrayItem[item.bindingDbFieldName]!=null){
                    item.value = arrayItem[item.bindingDbFieldName];
                  }
                }
              }
            })
          }else{
            for (let response in this.simplepanelData[this.simplepanelData['reponseObj']][0]){
              if(item.bindingDbFieldName == response){
                item.value = this.simplepanelData[this.simplepanelData['reponseObj']][0][response];
              }
            } 
          }
        }else{
          for (let response in this.simplepanelData[this.simplepanelData['reponseObj']]){
            if(item.bindingDbFieldName == response){
              item.value = this.simplepanelData[this.simplepanelData['reponseObj']][response];
            }
          }
        }
        
      })
    }, 1500);
  }
}
