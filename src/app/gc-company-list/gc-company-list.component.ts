import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';

declare const $:any;

@Component({
  selector: 'app-gc-company-list',
  templateUrl: './gc-company-list.component.html',
  styleUrls: ['./gc-company-list.component.scss']
})
export class GcCompanyListComponent implements OnInit {

  public gcCompany: any;
  public gcCompanyName: any;
  public licenseExpired: any;
  constructor(public restservice: RestserviceService, public emitter: EmitterService, public router: Router) { }

  ngOnInit() {
    $('#selectGc').modal('show');
    this.gcCompany =JSON.parse(sessionStorage.getItem('gcCompany'));
  }
  modalHide() {
    sessionStorage.setItem('popupGcName', this.gcCompanyName);
    this.emitter.gcCompanyItem(this.gcCompanyName);
    $('#selectGc').modal('hide');
    this.licenseExpired = JSON.parse(sessionStorage.getItem('loginDetails'));
    console.log('this.licenseExpired',this.licenseExpired.licenseAvailable , this.licenseExpired.trial);
    if(this.licenseExpired.licenseAvailable || this.licenseExpired.trial) {
      this.router.navigate(['/authenticate/list/project']);
    } else {
      this.router.navigate(['/license-expired']);
    }
    
  }
}
