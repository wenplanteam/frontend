import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputToggleSwitchComponent } from './input-toggle-switch.component';

describe('InputToggleSwitchComponent', () => {
  let component: InputToggleSwitchComponent;
  let fixture: ComponentFixture<InputToggleSwitchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputToggleSwitchComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputToggleSwitchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
