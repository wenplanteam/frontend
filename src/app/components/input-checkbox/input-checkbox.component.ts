import { Component, OnInit, AfterViewInit, Input } from '@angular/core';
import {FormArray, FormGroup, FormBuilder} from '@angular/forms';
import { RestserviceService } from '../../restservice.service';

@Component({
  selector: 'app-input-checkbox',
  templateUrl: './input-checkbox.component.html',
  styleUrls: ['./input-checkbox.component.scss'],
  providers: [FormBuilder]
})

export class InputCheckboxComponent implements OnInit {

  @Input() checkBoxData: any;
  @Input() form: FormGroup;
  @Input() pageData: any;
  public formHandler: any;
  public validationMgs: any;

  constructor(public restservice: RestserviceService, public formBuilder: FormBuilder) {
    this.formHandler = formBuilder;
   }

  ngOnInit() {
    console.log('form checkbox', this.form);
    this.validationMgs = sessionStorage.getItem('validationMessages');
    console.log('checkBox', this.checkBoxData);
  }


}
