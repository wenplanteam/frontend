import { Component, OnInit,Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder,Validators } from '@angular/forms';
import {RestserviceService} from '../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.scss']
})
export class ChangePasswordComponent implements OnInit {

  form: FormGroup;
  public formHandler: any;
  public operation: any;
  public panelData: any;
  public panels: any;
  public validation: any;
  public routepage: any;
  public page: any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router, private route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) { 
    this.formHandler = fb;
  }

  ngOnInit() {
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    // this.route.params.subscribe(params => {
    //   this.routepage = params.id;
    // });

    this.page = "create";

    //setTimeout(() => {
      this.getJsonData(this.page, 'changePassword');
    //}, 1000);
  }

  getJsonData(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      const buttonData =success;
      this.operation = buttonData;
      this.panelData = buttonData;
      this.panels = this.panelData['panels'];
      console.log(this.operation);
      
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log("err", err);
    });

  }

}
