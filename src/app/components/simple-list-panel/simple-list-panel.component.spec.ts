import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SimpleListPanelComponent } from './simple-list-panel.component';

describe('SimpleListPanelComponent', () => {
  let component: SimpleListPanelComponent;
  let fixture: ComponentFixture<SimpleListPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SimpleListPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SimpleListPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
