import { Component, OnInit, Input,OnDestroy } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { EmitterService } from '../emitter.service';
import { ActivatedRoute, Router } from '@angular/router';

declare const $: any;
@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.scss']
})
export class LandingComponent implements OnInit {
  @Input() panels: any;
  public dashBoard: any;
  public operation: any;
  public result: any;
  public routepage: any;
  public page: any;

  constructor(public restservice: RestserviceService, public emitter: EmitterService,
    private router: Router , private route: ActivatedRoute) { }

  ngOnInit() {
  this.getValidations();
  this.emitter.getRouterUrl();
  this.route.params.subscribe(params => {
    this.routepage = params.id;
  });
  this.page = 'landing';
    this.getJsonDataForLanding(this.page, 'landing');
  }

  getJsonDataForLanding(operation, pageName) {
   this.restservice.getRenderingJson(operation, pageName).subscribe(result => {
      this.result = result;
   }, (err) => {
     console.log(err);
   });
  }

  getValidations() {
    this.restservice.getValidations().subscribe(success => {
      sessionStorage.setItem('validationMessages', JSON.stringify(success));
    }, (err) => {
      console.log('err', err);
    });
  }
  ngOnDestroy(){
    //this.emitter.updateLogingDetails.unsubscribe();
  }
}
