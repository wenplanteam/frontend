import { Injectable } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormValidationService {

  constructor(public fb:FormBuilder) { }
  toFormGroup(panelData){
    let validation={};
    let validationFormArr ={};
    if(panelData){
      panelData.forEach((panels)=>{
        if(panels.type!="multiaddField" && panels.type!="simpleList" && panels.type!="multiadd"){
        panels.items.forEach((item)=>{
          if(item.validation){
            validation[item.name]= [null, this.validationInput(item.validation)];
          } else {
            validation[item.name] = [null,''];
          } 
        });
        } else if(panels.type=="multiaddField"){
          panels.items.forEach((items)=>{
              items.forEach((item)=>{
                validationFormArr[item[0].value]={};
                item.forEach(finalItem=>{
                  if(finalItem.htmlTag!='label'){
                  if(finalItem.validation){
                    validationFormArr[item[0].value][finalItem.name]= [null, this.validationInput(finalItem.validation)];
                  } else {
                    validationFormArr[item[0].value][finalItem.name] = [null,''];
                  }
                }
                })
              });  
            });
        }
      });
    }
    if(validationFormArr){
      for(let key in validationFormArr){
        validation[key]= this.fb.array([this.fb.group(validationFormArr[key])]);  
      }
    }
   return validation;
  }
 validationInput(validation) {
        const validationFormat = [];
        if (typeof (validation.maxLength) === 'number') {
            validationFormat.push(Validators.maxLength(validation.maxLength));
        }
        if (validation.required) {
            validationFormat.push(Validators.required);
        }        
        if (typeof (validation.minLength) === 'number') {
            validationFormat.push(Validators.minLength(validation.minLength));
        }
        if (validation.pattern) {
          validationFormat.push(Validators.pattern(validation.pattern))
        }
        return validationFormat;
 } 
}
