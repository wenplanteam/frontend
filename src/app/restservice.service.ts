import { Injectable, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams, HttpResponse } from '@angular/common/http';
import { ResponseContentType, ResponseType } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import 'rxjs/Observable';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { UUID } from 'angular2-uuid'
import { properties } from './properties.service';
import { environment } from '../environments/environment';

// import { request } from 'http';


const operations = {
  'list': 'read',
  'view': 'view',
  'edit': 'update',
  'create': 'create'
};
let token;

@Injectable({
  providedIn: 'root'
})

export class RestserviceService {

  public baseUrl = environment.baseUrl;
  public locale: any;

  ngOnInit() {
  }

  service: ServiceVar = {
    header: {},
    options: {},
    sessionStore: '',
    key: ''
  };

  constructor(public http: HttpClient) {
    this.service.header = new HttpHeaders();
  }

  public upperCaseFirst(pageName) {
    return pageName.charAt(0).toUpperCase() + pageName.slice(1);
  }

  /* template rendering */

  public getRenderingJson(operation, pageName) {
    let jsonUrl = './assets/jsonData/' + operation + '/' + pageName + ".json";
    return this.http.get(jsonUrl);
  }

  public getRenderingJsonForShare(operation, pageName) {
    let jsonUrl = './assets/jsonData/create' + '/' + pageName + ".json";
    return this.http.get(jsonUrl);
  }

  public getRenderingJsonView(operation, pageName) {
    let jsonUrl = './assets/jsonData/view' + '/' + pageName + ".json";
    return this.http.get(jsonUrl)
  }


  // Decode Login

  public decodeLogin(data) {
    let url;
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = data;
    url = this.baseUrl + "auth/decodeCode";
    return this.http.get(url, requestOption)
  }
  public getImportJson() {
    let jsonUrl = "./assets/jsonData/data/importData.json";
    return this.http.get(jsonUrl);
  }





  importExcel(pro_company_id, project_id, file) {

    const formData = new FormData();

    formData.append('fileName', file);

    console.log(pro_company_id, project_id, file);

    const url = this.baseUrl + 'pmc/importExcel?projectCompanyId=' + pro_company_id + '&projectId=' + project_id;

    return this.http.post(url, formData, { responseType: 'arraybuffer', observe: "response" })
  }






  /* Delete List Records */

  public deleteData(deleteData, url) {
    let headerOption = {};
    headerOption['body'] = deleteData;
    let jsonUrl = this.baseUrl + url;
    return this.http.delete(jsonUrl, headerOption);
  }

  /* Service For Create project */
  public create(data, serviceEndUrl) {
    let url;
    url = this.baseUrl + serviceEndUrl;
    return this.http.post(url, data);
  }

  /* Service For Update project */
  public edit(data, serviceEndUrl) {
    let url;
    url = this.baseUrl + serviceEndUrl;
    return this.http.put(url, data);
  }

  public _getEffortcompletion(payload) {
    let url;
    url = this.baseUrl + 'pmc/progressCompletionByProjectId';
    return this.http.post(url, payload);

  }

  public _pdfDownload(payload) {
    const url = this.baseUrl + 'pmc/downloadPercentageCompletePDF';
    return this.http.post(url, payload, { responseType: 'arraybuffer' });
  }
  public _sendEmail(payload) {
    const url = this.baseUrl + 'pmc/downloadPercentageCompletePDF';
    return this.http.post(url, payload);
  }
  /* Get Project list response from server */
  getResponseData(fetchRestServiceUrl, param, searchItem) {
    if (fetchRestServiceUrl == "userLogData") {
      let jsonUrl = './assets/jsonData/data/' + fetchRestServiceUrl + ".json";
      return this.http.get(jsonUrl);
    } else {
      let url;
      let requestOption = {
        'params': new HttpParams()
      };
      requestOption.params = param;
      url = this.baseUrl + fetchRestServiceUrl;
      return this.http.get(url, requestOption);
    }
  }

  // Get License Count

  getLicenseCount() {
    let url = this.baseUrl + 'ulc/licenseCount';
    return this.http.get(url);
  }

  shareProject(data, serviceEndUrl) {
    let url;
    url = this.baseUrl + 'pmc/shareProject';
    return this.http.post(url, data);
  }

  getSharedUsers(param) {
    let url;
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = param;
    url = this.baseUrl + 'ulc/projectUsers';
    return this.http.get(url, requestOption);
  }

  sendEmail(param) {
    let url;

    url = this.baseUrl + 'pmc/lookaheadPDFReport';
    return this.http.post(url, param);
  }

  /* service for dropdown */
  getOptions(param) {
    let url;
    url = this.baseUrl + param;
    return this.http.get(url);
  }
  /* service for autocomplete */
  getAutocompleteData(serviceName) {
    let url = this.baseUrl + serviceName;
    return this.http.get(url);
  }

  public getlanguage() {
    const jsonUrl = './assets/jsonData/select-language.json';
    return this.http.get(jsonUrl);
  }

  getValidations() {
    const jsonUrl = './assets/jsonData/validation/validation.json';
    return this.http.get(jsonUrl);
  }

  projectCompany(param) {
    let url;
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = param;
    url = this.baseUrl + 'pmc/projectCompany';
    return this.http.get(url, requestOption);
  }

  company(param?) {
    let url;
    if (!param.createdBy) {
      url = this.baseUrl + 'pmc/projectCompany';
      let requestOption = {
        'params': new HttpParams()
      };
      requestOption.params = param;
      return this.http.get(url, requestOption);
    } else {
      url = this.baseUrl + 'ulc/company';
      let requestOption = {
        'params': new HttpParams()
      };
      requestOption.params = param;
      return this.http.get(url, requestOption);
    }
  }


  users(param?) {
    let url;
    if (!param.createdBy) {
      url = this.baseUrl + 'ulc/projectUsers';
      let requestOption = {
        'params': new HttpParams()
      };
      requestOption.params = param;
      return this.http.get(url, requestOption);
    } else {
      url = this.baseUrl + 'ulc/user';
      let requestOption = {
        'params': new HttpParams()
      };
      requestOption.params = param;
      return this.http.get(url, requestOption);
    }
  }

  trade(param?) {
    let url;
    //if(param){
    url = this.baseUrl + 'pmc/trade';
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = param;
    return this.http.get(url, requestOption);
    // }
  }

  specDivision(param?) {
    let url;
    //if(param){
    url = this.baseUrl + 'pmc/specDivision';
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = param;
    return this.http.get(url, requestOption);
    // }
  }

  getMasterData() {
    let url;
    let requestOption = {
      'params': new HttpParams()
    };
    url = this.baseUrl + 'util/masters';
    return this.http.get(url, requestOption);
  }

  // buy plan

  buyPlan(data) {
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = data;
    let url = this.baseUrl + 'ulc/plan';
    return this.http.get(url, requestOption);
  }

  getExpiryDate(data) {
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = data;
    let url = this.baseUrl + 'ulc/expirydate';
    return this.http.get(url, requestOption);
  }

  goToPayment(data) {
    // let url = 'http://192.168.1.124:9903/api/userLicense/buyLicense';
    let url = this.baseUrl + 'ulc/buyLicense';
    return this.http.post(url, data);
  }
  paymentProcess(data) {
    console.log("data 435", data);
    //let url = 'http://192.168.1.124:9903/api/userLicense/charge';
    let url = this.baseUrl + 'ulc/charge';
    return this.http.post(url, data);
  }
  transaction(data) {
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = data;
    let url = this.baseUrl + 'ulc/licenseResponse';
    return this.http.get(url, requestOption);
  }

  getItback(data) {
    let url = this.baseUrl + 'ulc/revokeLicense';
    return this.http.post(url, data);
  }
  gcByUser() {
    let url;
    let requestOption = {
      'params': new HttpParams()
    };
    url = this.baseUrl + 'pmc/gcByUser';
    return this.http.get(url, requestOption);
    // return this.http.get(this.baseUrl+"pmc/gcByUser");
  }
  saveProfile(data) {
    console.log('data', data);
    const url = this.baseUrl + 'ulc/user';
    return this.http.put(url, data);
  }

  logout() {
    // let url;
    // let requestOption = {
    //   'params' : new HttpParams()
    // };
    // console.log('this.baseUrl', this.baseUrl);
    // url = this.baseUrl + 'logout';
    // console.log('url', url);
    return this.http.get('http://192.168.1.124:9900/api/emzee/auth/logout');
  }

  timeZone() {
    const url = this.baseUrl + 'util/timeZones';
    //const jsonUrl = '../assets/jsonData/data/timezone.json'
    return this.http.get(url);
  }


  downloadPdf(data) {
    console.log('data', data);
    // let requestOption = {
    //   'params' : new HttpParams()
    // };
    // requestOption.params = data;
    const url = this.baseUrl + 'pmc/downloadPDF';
    return this.http.post(url, data, { responseType: 'arraybuffer' });
  }

  importHistoryJson(operation, pageName) {
    let jsonUrl = './assets/jsonData/' + operation + '/' + pageName + ".json";
    return this.http.get(jsonUrl);
  }
  //** getResponse import hisroy */
  getResponseimportHistory(param) {
    let url = this.baseUrl + 'pmc/importHistory';
    let requestOption = {
      'params': new HttpParams()
    };
    requestOption.params = param;
    return this.http.get(url, requestOption);
  }

}
export class ServiceVar {
  header: any;
  sessionStore: any;
  options: any;
  key: any;
}

