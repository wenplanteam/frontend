import { Component, OnInit,ViewChild,Input } from '@angular/core';
import {FormArray, FormBuilder, FormGroup} from "@angular/forms";
import {RestserviceService} from "../../restservice.service";

export enum KEY_CODE {
UP_ARROW = 38,
DOWN_ARROW = 40,
TAB_KEY = 9,
ENTER_KEY =13
}

@Component({
  selector: 'app-input-combobox',
  templateUrl: './input-combobox.component.html',
  styleUrls: ['./input-combobox.component.scss'],
  providers:[FormBuilder]
})
export class InputComboboxComponent implements OnInit {

@Input() inputData: any;
@Input() panelData: any;
@Input() form: FormGroup;
formHandler : any;

showDropDown: boolean;
counter:number;
dataList :any;

constructor(private restservice:RestserviceService,public fb:FormBuilder  ) {
    this.formHandler=fb;
    this.reset();
}
onFocusEventAction():void {
    this.counter = -1;
}
onBlurEventAction(): void{
    this.showDropDown = false;
}
onKeyDownAction(event: KeyboardEvent):void {
    console.log("event", event, event.keyCode);
    this.showDropDown = true;
    // this.findInArray(this.dataList, event.key);

//     for (var i = 0,len = this.dataList.length; i < len; i++) {
//         //console.log("this.dataList[i].name", this.dataList[i].name);
// console.log("this.dataList[i].name.toString().toLowerCase().search(event.key)", this.dataList[i].name.toString().toLowerCase().search(event.key));
//         if ( this.dataList[i].name.toString().toLowerCase().search(event.key)> -1) {
//             //console.log("dataList[i]", this.dataList[i]);
//         }else{
//             //console.log("this.dataList", this.dataList[i]);
//             this.dataList.splice(i, 1);
//             //console.log("this.dataList", this.dataList);
//         }
//     }

    // if(event.keyCode===KEY_CODE.ENTER_KEY){
    //     this.counter = this.counter==-1?0:this.counter;
    //     //console.log("this.counter",this.counter);
    //     this.checkHighlight(this.counter);
    //     this.updateTextBox(this.dataList[this.counter]);
    //     this.showDropDown=false;
    //     this.counter=-1;
    // } else {
        
    //     if (event.keyCode === KEY_CODE.UP_ARROW) {
    //         console.log("key up",this.counter);
    //         this.counter = (this.counter === 0)?this.counter:--this.counter;
    //         this.checkHighlight(this.counter);
    //        // this.updateTextBox(this.dataList[this.counter][this.inputData.fieldName],this.counter,'keyEvent');
    //     }
    //     if (event.keyCode === KEY_CODE.DOWN_ARROW) {
    //         console.log("key down",this.counter);
    //         this.counter = (this.counter === this.dataList.length-1)?this.counter:++this.counter;
    //         this.checkHighlight(this.counter);
    //         //this.updateTextBox(this.dataList[this.counter][this.inputData.fieldName],this.counter,'keyEvent');
    //     }
    // }

    let dataList = this.dataList;
    

}

findInArray(dataList, key) {
    for (var i = 0,len = dataList.length; i < len; i++) {
        console.log("i", i, dataList[i]);
        if ( dataList[i].name.search(key)) {
            console.log("dataList[i]", dataList[i]);
        }else{
            this.dataList.splice(i, 1);
        }
    }
    return -1;
}


checkHighlight(currentItem):boolean{
    if(this.counter === currentItem) return true;
    else return false;
}

getData(){
    if(this.inputData){
        this.restservice.getAutocompleteData(this.inputData.restServiceUrl).subscribe( success =>{
            console.log("success getData combobox", success);
            this.dataList =success['payload'];
        });
    }
}
ngOnInit(){
    //this.reset();
    this.getData();
}
toogleDropDown():void {
    this.showDropDown = !this.showDropDown;
}
reset(): void{
    this.showDropDown = false;
    //this.getData('');
}
textChange(event: KeyboardEvent,value){
   console.log("event.keyCode",event.keyCode);
   if(event.keyCode!=KEY_CODE.ENTER_KEY && event.keyCode!=KEY_CODE.UP_ARROW && event.keyCode!=KEY_CODE.DOWN_ARROW){ 
        console.log("value",value);
        this.dataList = [];
        if(value.length >0){
            this.getData();
            setTimeout(()=>{
                this.showDropDown =true;
            });
        }
   }
}
updateTextBox(valueSelected){
    this.inputData.value = valueSelected;
    //  let childName =this.panelData.filter(item=>{
    //      return item.name ==this.inputData.childName;
    //  });
    //  childName[0].value=this.dataList[index][this.inputData.childFieldName];
    //  if(evnt=="click")
        this.showDropDown = false;
}

}
