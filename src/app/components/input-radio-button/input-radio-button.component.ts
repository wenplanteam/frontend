import { Component, OnInit,  AfterViewInit, Input } from '@angular/core';
import {FormArray, FormGroup, FormBuilder} from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
@Component({
  selector: 'app-input-radio-button',
  templateUrl: './input-radio-button.component.html',
  styleUrls: ['./input-radio-button.component.scss'],
  providers: [FormBuilder]
})
export class InputRadioButtonComponent implements OnInit {
  @Input() radioButtonData: any;
  @Input() form: FormGroup;
  @Input() pageData: any;
  public formHandler: any;
  public validationMgs: any;
  public myDropDown : any;
  @Input() myselectedVal:any;
  notification: string;

  constructor(public restservice: RestserviceService, public formBuilder: FormBuilder) { 
    this.formHandler = formBuilder;
  }

  /* Function for Init Component */
  ngOnInit() {
    console.log('form radioButtonData', this.form);
    this.validationMgs = sessionStorage.getItem('validationMessages');

    console.log('radioButtonData', this.radioButtonData);
      //  this.myselectedVal = this.radioButtonData.value;

    this.myDropDown = localStorage.getItem('frequencyDay');
  }

  selectedRadioValue(formVal , selectedVal){
    console.log("formVal", formVal);
    console.log("selectedVal", this.myselectedVal);
    if(formVal.name == 'frequency'){
      // formVal.selected = true;
      formVal.value = selectedVal;
    }   
  }

  onChangeofOptions(val){
    console.log(val)
    localStorage.setItem("frequencyDay", val)
  }

}
