import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { $ } from 'protractor';

@Component({
  selector: 'app-input-selectbox',
  templateUrl: './input-selectbox.component.html',
  styleUrls: ['./input-selectbox.component.scss']
})
export class InputSelectboxComponent implements OnInit {

  @Input() selectData: any;
  @Input() form: FormGroup;
  @Input() pageData: any;
  @Input() panelItem: any;
  public routepage: any;
  public formHandler: any;
  selectOption = FormControl;
  public restUrl: any;
  public module: any;
  public validation: any;
  public selectOptions: any;

  public checkOptionObj: boolean = false;
  lookAheadInfo: any;
  userRole: boolean = false;
  notification: any;
  frequency: string;

  constructor(public restservice: RestserviceService, private route: ActivatedRoute, public formBuilder: FormBuilder) {
    this.formHandler = formBuilder;
  }
  ngOnInit() {
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    this.lookAheadInfo =  JSON.parse(sessionStorage.getItem('lookaheadInfo'));
    this.frequency = sessionStorage.getItem('frequency');
  this.notification =   sessionStorage.getItem('notification');
    if(this.lookAheadInfo.projectRole == 'SC-Manager'){   
      this.userRole = true;
    }else if(this.lookAheadInfo.projectRole == 'GC-Observer'){    
      this.userRole = true;
    }else if(this.lookAheadInfo.projectRole == 'Owner'){     
      this.userRole = true;
    }else if(this.lookAheadInfo.projectRole == 'SC-Observer'){    
      this.userRole = true;
    } else{
     this.userRole = false;
    }
    this.route.params.subscribe(params => {
      this.routepage = params.id;
    });
    this.restUrl = this.selectData.restServiceUrl;
    if (this.selectData.options && this.selectData.options.length == "0") {
      this.restservice.getOptions(this.restUrl).subscribe(success => {
     
        this.selectData.options = success['payload'] ? success['payload'][this.selectData.responseKey] : success;
      }, error => {
        console.log("error", error);
      })
    }else{
      this.checkOptionObj = true;
    }
  }

  ngAfterViewInit() {
    const getId = <HTMLButtonElement>document.getElementById(this.selectData.name);
    const obj = new InputSelectboxComponent(this.restservice, this.route, this.formHandler);
    if (this.selectData.events) {
      for (const action of this.selectData.events) {
        if (action.type) {
          getId[action.type] = () => {
            for (const fn of action.functions) {
              if (typeof obj[fn.name] === 'undefined') {
              } else {
                obj[fn.name](this.pageData, this.selectData);
              }
            }
          }
        }
      }
    }
  }

  getField(pageData, fieldName, count = 0) {
   
    let panel = [];
    if (pageData.panels) {
      panel = pageData.panels.filter(panel => {
        let panelItem = [];
        if (panel.type !== 'dynamic') {
          panelItem = panel.items.filter(item => {
            return item.name == fieldName;
          });
        }
        return panelItem.length > 0;
      });
    }
    let field;
    if (panel.length > 0 || count == 1) {
      if (panel[0]) {
        field = panel[0].items.filter(item => {
          return item.name == fieldName;
        });
      } else {
        return panel;
      }
    } else {
      let dynamicPanel = pageData.panels.filter(panel => {
        return panel.type === 'dynamic';
      });
      count++;
      return this.getField(dynamicPanel[0], fieldName, count);
    }
    return field[0];
  }

  changeGroup(pageData, selectData) {
    let primaryGroup = this.getField(pageData, selectData.parent);
    let group = this.getField(pageData, 'userGroupId');
    primaryGroup.value = group.value;
  }
/* Function for Select value in selectBox */
  public onNameChanged(cascade, selectValue, optionValue) {
    // console.log("cascade", this.pageData)
    // console.log("selected Valuie",selectValue)
  /*   this.pageData.panels.forEach(items => {
  
    // console.log('items',items);
      if(items.panelName == 'Notification'){
        // console.log('items.panelnb',items);
        items.items.forEach(panelItem => {
          // console.log('panel item',panelItem);
          if(panelItem.label == 'Day of the week') {
            if(selectValue != 'Daily') {
            // console.log('panel item',panelItem);
            panelItem.visible = false;
            // alert("vidible false")
            }
            else {
              panelItem.visible = true;
              // alert("vidible true")
            }
          }
        })
      }
    });
     */

    if (cascade && cascade.length) {
      cascade.forEach(cascadeData => {
        let filterData = this.panelItem.items.filter(data => {
          return (data.name === cascadeData.name)
        });
        const id = selectValue ? JSON.parse(selectValue) : undefined;
        this.restservice.getOptions(filterData[0].restServiceUrl).subscribe(success => {
          this.selectOption[filterData[0].name] = success['payload'];
        }, error => {
          console.log('error', error);
        });
      });
    }
    if (this.selectData.events) {
      const event = this.selectData.events;
      event.forEach(eventData => {

      });
    }
  }

  //customField control type 
  disbleDropdowntype(pageData, selectData) {
    if (selectData.value != 2) {
      this.getField(pageData, 'dropdownValues').readOnly = true;
      this.getField(pageData, 'Length').readOnly = false;
      if (selectData.value == 3) {
        this.getField(pageData, 'Length').readOnly = true;
      }
      this.getField(pageData, 'dropdownValues').value = "";
      this.getField(pageData, 'Length').value = "";
    } else {
      this.getField(pageData, 'dropdownValues').readOnly = false;
      this.getField(pageData, 'Length').readOnly = true;
      this.getField(pageData, 'dropdownValues').value = "";
      this.getField(pageData, 'Length').value = "";
    }
  }
}
