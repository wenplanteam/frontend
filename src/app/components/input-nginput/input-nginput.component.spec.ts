import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputNginputComponent } from './input-nginput.component';

describe('InputNginputComponent', () => {
  let component: InputNginputComponent;
  let fixture: ComponentFixture<InputNginputComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputNginputComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNginputComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
