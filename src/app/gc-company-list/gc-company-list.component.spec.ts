import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GcCompanyListComponent } from './gc-company-list.component';

describe('GcCompanyListComponent', () => {
  let component: GcCompanyListComponent;
  let fixture: ComponentFixture<GcCompanyListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GcCompanyListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GcCompanyListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
