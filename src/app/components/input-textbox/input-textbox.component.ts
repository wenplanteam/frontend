import { Component, OnInit, Input, EventEmitter, Output, AfterViewInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import { EmitterService } from '../../emitter.service';
import { filter } from 'rxjs/operators';

declare let $: any;

@Component({
  selector: 'app-input-textbox',
  templateUrl: './input-textbox.component.html',
  styleUrls: ['./input-textbox.component.scss'],
  providers: [FormBuilder]
})
export class InputTextboxComponent implements OnInit, AfterViewInit {

  @Input() inputData: any;
  @Input() pageData: any;
  @Input() form: FormGroup;
  formHandler: any;
  public validation: any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();
  userRole: any;
  lookAheadInfo: any;

  constructor(public fb: FormBuilder, public restservice: RestserviceService, public emitter: EmitterService) {
    this.formHandler = fb;
  }
  ngOnInit() {
    const validation = {};
    this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
    console.log('this.validation', this.validation);

    if (sessionStorage.getItem('projectDetails')) {
      this.lookAheadInfo = JSON.parse(sessionStorage.getItem('projectDetails'));

      this.userRole = this.lookAheadInfo.projectRole;

      console.log("this.user role in button", this.userRole)
    }

    console.log("this.userRoles", this.userRole);
    this.emitter.loginDetailData.subscribe(item => { });
    this.getUsername(this.inputData);
    $(document).ready(function () {
      $(this).find('#newCompanyName').focus();
      $(this).find('#lastName').focus();
      $(this).find('#projectName').focus();
    });
    //   var x= document.getElementById("businessPhoneNumber");
    //   $(document).keydown(function(e) {
    //     if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
    //       e.preventDefault();
    //   }
    //   });
    // }

    // var x= document.getElementById("businessPhoneNumber");
    // $("#businessPhoneNumber").keypress(function (event) {
    // const pattern= /^(([0-9\-]{1,3})*)$/;
    //   const inputChar = String.fromCharCode(event.key ? event.which : event.key);
    //   const value = (event.target.value) + inputChar;
    //   var charCode = (event.which) ? event.which : event.keyCode;
    //   // let inputChar = String.fromCharCode(event.charCode);
    //   console.log("charCode", charCode);
    //   console.log("inputChar", inputChar);
    //   console.log("value", value);
    //   if (!pattern.test(value) ) {
    //     event.preventDefault();
    //     //return false;
    //   }
    //   });
  }

  getUsername(inputData) {
    if (sessionStorage.getItem('loginDetails') != undefined && sessionStorage.getItem('loginDetails') != null) {
      const firstName = JSON.parse(sessionStorage.getItem('loginDetails')).firstName;
      const lastName = JSON.parse(sessionStorage.getItem('loginDetails')).lastName;
      if (inputData.name == 'projectManager') {
        inputData.value = firstName + ' ' + lastName;
      }
    }
  }

  /*switch Toggle Function in notification */
  selectedValue(val) {
    console.log("pageData", this.pageData)
    console.log(val, "val")
    sessionStorage.setItem("notification", val)

    let filterData = this.pageData.panels.filter(panel => {
      return panel.panelName == 'Notification Type' || panel.panelName == 'Notification Information' || panel.panelName == 'frequency';

    });
    console.log("selected value item", filterData)

    filterData.forEach(data => {

      data.items.forEach(subData => {

        console.log(subData, "data in toggle notification")

        if (val && subData.name == 'frequency') {
          subData.value = "Daily"
    
        } else if (!val) {
          subData.value = ""
          // alert(val +"else");
        }

        subData.subItem.forEach(innerData => {
          console.log('innerData', innerData.value);
          if (val) {
            innerData.value = true;
            $("#ProjectTaskSummary").prop("disabled", false);
            $("#ThroughEmail").prop("disabled", false);
            $("#TasksPastsDue").prop("disabled", false);
            $("#TaskInAlert").prop("disabled", false);
            $("#NewlyAddedTask").prop("disabled", false);
            $("#ModifiedTask").prop("disabled", false);
            $("#FromApplication").prop("disabled", false);
            $("#Daily").attr("disabled", false);
            $("#Weekly").attr("disabled", false); 
            $("#dayOfWeek").prop("disabled", false);

           } else {
            innerData.value = false;
            $("#ProjectTaskSummary").prop("disabled", true);
            $("#ThroughEmail").prop("disabled", true);
            $("#TasksPastsDue").prop("disabled", true);
            $("#TaskInAlert").prop("disabled", true);
            $("#NewlyAddedTask").prop("disabled", true);
            $("#ModifiedTask").prop("disabled", true);
            $("#FromApplication").prop("disabled", true);
            $("#Daily").attr("disabled", true);
            $("#Weekly").attr("disabled", true);  
            $("#dayOfWeek").prop("disabled", true);
           


              // if( $("#dayOfWeek").prop("disabled", true).length >=1){
            
              //   $("#dayOfWeek").prop("disabled", false);
              // }else{
              //   $("#dayOfWeek").prop("disabled", true);
              // }

            
            
            
          }
        })
      })
    })

    // let filterItem = filterData.filter(filterItems =>{
    //   return filterItem;
    // })
    //  console.log(filterItem)
  }
  get isValid() {
    return this.form.controls[this.inputData.name].valid;
  }
  ngAfterViewInit() {
    const getId = <HTMLInputElement>document.getElementById(this.inputData.name);
    const obj = new InputTextboxComponent(this.formHandler, this.restservice, this.emitter);
    if (this.inputData.events) {
      for (const action of this.inputData.events) {
        getId[action.type] = () => {
          for (const fn of action.functions) {
            if (typeof obj[fn.name] === 'undefined') {
            } else {
              obj[fn.name](this.pageData, this.inputData);
            }
          }
        };
      }
    }
  }
  getField(pageData, fieldName, count = 0) {
    let panel = [];
    if (pageData.panels) {
      panel = pageData.panels.filter(panel => {
        let panelItem = [];
        if (panel.type !== 'dynamic') {
          panelItem = panel.items.filter(item => {
            return item.name == fieldName;
          });
        }
        return panelItem.length > 0;
      });
    }
    let field;
    if (panel.length > 0 || count == 1) {
      if (panel[0]) {
        field = panel[0].items.filter(item => {
          return item.name == fieldName;
        });
      } else {
        return panel;
      }
    } else {
      const dynamicPanel = pageData.panels.filter(panel => {
        return panel.type === 'dynamic';
      });
      count++;
      return this.getField(dynamicPanel[0], fieldName, count);
    }
    return field[0];
  }
}
