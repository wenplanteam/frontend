import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputNgselectComponent } from './input-ngselect.component';

describe('InputNgselectComponent', () => {
  let component: InputNgselectComponent;
  let fixture: ComponentFixture<InputNgselectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputNgselectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputNgselectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
