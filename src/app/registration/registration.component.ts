import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';

declare const $: any;

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss'],
  providers: [FormBuilder]
})

export class RegistrationComponent implements OnInit {
  form: FormGroup;

  public formHandler: any;
  public panels: any;
  public page: any;
  public routepage: any;
  public panelItems: any;
  public operation: any;
  public formObj;
  public panelData: any;
  public validation: any;
  
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router,
    private route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    setTimeout(() => {
      console.log('panels', this.panels);
      this.route.queryParams.subscribe((params) => {
        console.log('params', params);
        const email = params['email'];
        const companyName = params['companyName'];
        if (email) {
          console.log('this.panels', this.panels);
          this.panels.forEach(panelItem => {
            console.log('panelItem', panelItem);
            panelItem.items.forEach(item => {
              console.log('item', item);
              if (item.bindingDbFieldName == 'email') {
                item.value = email;
              }
              if (item.bindingDbFieldName == 'companyName') {
                console.log('inside if company name should be',companyName);
                item.value = companyName;
                console.log('assigned value',item.value);

                // item.value = item.value.replace("&amp;", "&");
              }

            });
          });
        }
      });
    }, 1000);

    

    this.emitter.getRouterUrl();
    this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
    this.route.params.subscribe(params => {
    });
    this.route.queryParams.subscribe((params) => {
      // console.log("params", params);
        if (params['email']) {
          this.routepage = 'registrationViaEmail';
        } else {
          this.routepage = 'registration';
        }
        this.page = 'create';
      console.log('this.routepage', this.routepage);
      this.getJsonDataForRegistration(this.page, this.routepage);
    });
  }

  onhandleUserUpdated(e) {
    this.notifyPanel.emit(e);
  }

  /* Remove visible false from rendering json*/

  removeByAttr(arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {
        arr.splice(i, 1);
      }
    }
    return arr;
  }

  /* Get UI Rendering JSON for create from Server */

  getJsonDataForRegistration(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      const buttonData = success;
      this.operation = buttonData;
      this.panelData = buttonData;
      this.panelData.panels.forEach(item => {
        this.removeByAttr(item.items, 'visible', '');
      });
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }
}