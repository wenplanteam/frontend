import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { EmitterService } from '../../emitter.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
    selector: 'app-pagination',
    templateUrl: './pagination.component.html',
    styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {


    @Output() goPagination: EventEmitter<any> = new EventEmitter();
    paginationData: any;

    public currentPageNo: any;
    /* public totalPage: any; */
    public totalRecords: any;
    public lastPage: any;
    public pageSizes: any;
    public recordCountPerPage = 10;
    public totalPage: any;
    routingId: any;
    createdUserId: string;

    constructor(public emitter: EmitterService, public router: Router, public route: ActivatedRoute) {

    }

    ngOnInit() {

    this.route.params.subscribe(params => {
       let id = params['id'];
        this.routingId = id;

      })
      setTimeout(()=>{
        this.emitter.paginationData.subscribe((paginationData)=>{
            console.log("paginationData", paginationData)
            this.paginationData =paginationData;
            this.totalRecords = this.paginationData.totalRecords;
            this.currentPageNo = this.paginationData.currentPageNumber+1;
            this.totalPage = Math.ceil(this.totalRecords / this.recordCountPerPage);
            this.lastPage =this.totalPage;
            this.pageSizes = [10, 15, 20, 25, 50, 100];
    });
    }, 500);
  }


    pagination(navigate) {
        console.log("naviate", navigate)

        let localCompany = sessionStorage.getItem('gcCompany');
        if (sessionStorage.getItem('popupGcName') == null || sessionStorage.getItem('popupGcName') == undefined)

            this.emitter.selectedHeaderCompany.subscribe(item => {
                this.createdUserId = item;

            });

        this.totalPage = Math.ceil(this.totalRecords / this.recordCountPerPage);
        this.lastPage = this.totalPage;
        let paginationValue = {};
         if (navigate === 'next') {
            
             this.currentPageNo++;
             paginationValue = {
                 'pageNo': this.currentPageNo,
                 'pageSize' : this.recordCountPerPage,
                  'created': this.createdUserId

             };
             this.goPagination.emit(paginationValue);

         }else if (navigate === 'before') {
             this.currentPageNo = this.currentPageNo - 1;
             paginationValue = {
                 'pageNo': this.currentPageNo,
                 'pageSize' : this.recordCountPerPage,
                 'created': this.createdUserId
                };
             this.goPagination.emit(paginationValue);
         }else if (navigate === 'last') {
             this.currentPageNo = Math.ceil(this.totalRecords / this.recordCountPerPage);
             paginationValue = {
                 'pageNo': this.currentPageNo,
                 'pageSize' : this.recordCountPerPage,
                 'created': this.createdUserId

             };
             this.goPagination.emit(paginationValue);
         }else {
             this.currentPageNo = 1;
             paginationValue = {
                 'pageNo': this.currentPageNo,
                 'pageSize' : this.recordCountPerPage,
                 'created': this.createdUserId


                };
             this.goPagination.emit(paginationValue);
         }
     }
}
