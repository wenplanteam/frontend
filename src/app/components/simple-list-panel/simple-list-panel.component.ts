import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder,FormControl,Validator }  from '@angular/forms';

@Component({
  selector: 'app-simple-list-panel',
  templateUrl: './simple-list-panel.component.html',
  styleUrls: ['./simple-list-panel.component.scss']
})
export class SimpleListPanelComponent implements OnInit {

  @Input() simpleListPanelData:any;
  @Input() form: FormGroup;
  public listData = [];
  public noResult :any;

  constructor() { }

  ngOnInit() {

    this.noResult=true;
    setTimeout(()=>{
    this.simpleListPanelData.items.forEach(item=>{
      this.listData.push(item);
    });

    this.listData.forEach(item =>{
      
      item.listItems.forEach(listItem =>{
       if(this.simpleListPanelData[this.simpleListPanelData["reponseObj"]] && this.simpleListPanelData[this.simpleListPanelData["reponseObj"]].length){
          this.simpleListPanelData[this.simpleListPanelData["reponseObj"]].forEach(responseDataValue =>{
            for (let responseData in responseDataValue){              
              if(listItem.bindingDbFieldName == responseData){
                listItem.value = responseDataValue[responseData];
              }
            }
          });
        } else {
          this.noResult=false;
        }
      });
    },1000);
})
  }
}
