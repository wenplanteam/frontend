import { Component, OnInit, Directive, Input, Output, EventEmitter, HostListener } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { RestserviceService } from '../restservice.service';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';

declare const $: any;
@Component({
  selector: 'app-operation',
  templateUrl: './operation.component.html',
  styleUrls: ['./operation.component.scss']
})
// @Directive({
//   selector: '[routerLinkActive]',
//   exportAs: 'routerLinkActive'
// })
export class OperationComponent implements OnInit {
  @Output() addDetails: EventEmitter<any> = new EventEmitter();
  @Input() pageData: any;
  @Input() pageRouting: any;
  @Input() panelData: any;
  @Input() form: FormGroup;
  public id: any;
  public listData: any;
  public deleteData: any;
  private loadComponent = false;
  public privileges;
  public projectName;
  public routeUrl: any;
  public panels: any;
  public formHandler: any;
  public operation: any;
  public pageDatapopup: any;
  public loginDetails: any;
  scrHeight: number;
  scrWidth: number;
  actionButton: boolean;
  @HostListener('window: load,resize', ['$event'])

  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    console.log(this.scrHeight, this.scrWidth, "->>->>>>>>>");
    if (this.scrWidth < 576) {
      this.actionButton = true;

    } else {
      this.actionButton = false;
    }
  }
  constructor(public restservice: RestserviceService,
    public router: Router, public route: ActivatedRoute,
    public emitter: EmitterService, public location: Location, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
    this.getScreenSize();
  }

  ngOnInit() {
    this.getScreenSize();
    this.loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    setTimeout(() => {
      if (this.pageData.pageType == 'Edit' && this.pageData.pageName == 'Projects') {
        this.projectName = JSON.parse(sessionStorage.getItem('projectDetails')).projectName;
      } else {
        this.emitter.projectName.subscribe(item => {
          this.projectName = item;
        });
      }
    }, 500);

    this.id = JSON.parse(sessionStorage.getItem('listId'));
    this.emitter.deleteDataSource.subscribe(item => {
      this.deleteData = item;
    });
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  goRoute() {
    const path = this.router.url.split('/');
    console.log('path', this.route.url);

    if (path.length === 5) {
      if (sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing') != undefined) {
        this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
        sessionStorage.setItem('url', 'project');
      } else {
        this.router.navigate(['/authenticate/list/project']);
      }
    } else {
      if (path[3] == 'project-user' || path[3] == 'project-company') {
        const redirectPath = path[3] == 'project-user' ? 'user' : 'company';
        this.router.navigate(['/authenticate/edit/project/' + redirectPath]);
      } else {
        this.router.navigate(['/authenticate/list/' + path[3]]);
      }
    }
    if (path[2] == 'changePassword') {
      this.router.navigate(['/authenticate/changePassword']);
    }
  }

  routeToPage(operation) {
    const ss = this.router.url.split('/');
    const routepage = ss[ss.length - 1];
    console.log('sssss', routepage);
    if (operation.popup) {
      this.restservice.getRenderingJson('create', routepage).subscribe(success => {
        this.operation = success['tools'];
        this.panels = success['panels'];
        this.pageDatapopup = success;
        this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
        $('#createModel').modal('show');
      }, (err) => {
        console.log('err', err);
      });
    } else {
      this.router.navigate([operation.redirectToPage]);
    }
  }

  addInputFields() {
    this.addDetails.emit('hi');
  }
  getOperationPrivileges(operation) {
    this.privileges = JSON.parse(sessionStorage.getItem('privileges'));
  }
  deleteListData() {
    const url = this.pageData.fetchRestServiceUrl.split('?');
    const item = this.deleteData;
    if (item) {
      this.restservice.deleteData(item, url[0]).subscribe(success => {
        this.emitter.getActivatedRoute();
        this.emitter.getMessage(success['message']);
        this.emitter.changeMessage({});
        this.emitter.getMessage('');
        this.emitter.getDataSource({});
      }, (err) => {

      });
    }
  }
  getFocus() {
    $(document).ready(function () {
      $('#createModel').on('shown.bs.modal', function () {
        $(this).find('.description-focus').focus();
      });
    });
  }
  goFocus() {
    $(document).ready(function () {
      $('.addProjectBtn').click(function () {
        var eee = document.getElementById('projectId').focus();
      });
    });
  }
  goBack() {
    this.location.back();
  }

  gotoRoute() {
    this.router.navigate(['/authenticate/edit/project/details']);
  }
}