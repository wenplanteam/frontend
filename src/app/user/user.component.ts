import { Component, OnInit,OnDestroy } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute,Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {FormValidationService} from '../form-validation.service';
import { EmitterService } from '../emitter.service';

declare let $: any;

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss',
  '../company/company.component.scss'
]
})
export class UserComponent implements OnInit {
  public form: any;
  public popupForm: any;
  public formHandler: any;
  public operation: any;
  public panels: any;
  public panelData: any;
  public projectDetails: any;
  public routepage: any;
  public page: any;
  public createdUserId: any;
  public availableUser: any;
  public routingId: any;
  public companyChecked = false;
  public user: any;
  public userAssociate: any;
  public operationBtn: any;
  public associatedUser = {
    'projectId': 0,
    'deleteUsers': [],
    'projectUsers': []
  };
  public userRole: any;


  constructor(public restservice: RestserviceService, public emitter: EmitterService,
    private router: Router, public route: ActivatedRoute, public fb: FormBuilder, public formValidation: FormValidationService) { 
    this.formHandler = fb;
  }

  ngOnInit() {
    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    // console.log('this.projectDetails user', this.projectDetails);

    const ss = this.router.url.split('/');
    this.routepage = ss[ss.length - 1];
    this.page = ss[2];
    this.getJsonDataForProject(this.page, this.routepage);
    this.emitter.selectedHeaderCompany.subscribe(item =>{
      this.createdUserId = item;
      // console.log("this.createdUserId item item",item);    
      this.getAllUsers();
    });
    this.restservice.getMasterData().subscribe(success => {
      // console.log('success sdd', success);
      this.userRole = success['payload']['project_permission'];
    }, (err) => {
      console.log('err', err);
    });

    if (this.projectDetails) {
      this.getUserById(this.projectDetails.projectId);
    } else {
      const id = JSON.parse(sessionStorage.getItem('listId'));
      this.getUserById(id.value);
    }
    this.getAllUsers();
    console.log('url',this.router.url);
  }

  getJsonDataForProject(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      this.operation = success;
      this.panelData = success;
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }

  /* Get All Users */

  getAllUsers() {
    const data = {
      'createdBy': true,
      'created' : this.createdUserId
    };
    this.restservice.users(data).subscribe(success => {
      // console.log('success getAllUsers', success);
      if (success) {
        this.availableUser = success['payload'];
        this.checkAddedUsers();
      }
    }, (err) => {
      console.log('err', err);
    });
  }

  editUser(item) {
    // console.log('item ddd', item);
    const param = {'field': 'id', 'value': item.id };
    // console.log('param', param);
    sessionStorage.setItem('projectUser', JSON.stringify(this.associatedUser));
    sessionStorage.setItem('listId', JSON.stringify(param));
    const name = item.firstName + ' ' + item.lastName;
    // console.log('name', name);
    this.emitter.getProjectName(name);
    this.router.navigate(['/authenticate/edit/project-user']);
  }

  getUserById(projectId) {
    this.associatedUser.projectId = projectId;
    const payload = {
      projectId: projectId
    };
    this.restservice.users(payload).subscribe(success => {
      // console.log('success getUserById', success);
      success['payload'].forEach(item => {
        // console.log('item', item);
          if (this.projectDetails.projectRole == 'SC-Manager') {
            if (item.projectRole == 'GC-Project Admin' || item.projectRole == 'GC-Project Manager' || item.projectRole == 'Owner' || item.email == JSON.parse(sessionStorage.getItem('loginDetails')).email) {
              item['isProjectUser'] = true;
            }
            if (item.projectRole == 'GC-Project Admin' || item.projectRole == 'GC-Project Manager' || item.projectRole == 'Owner') {
              item['isProjectUserEdit'] = true;
            }
          } else if (this.projectDetails.projectRole == 'SC-Observer') {
            item['isProjectUser'] = true;
          } else {
            if (this.projectDetails.projectRole == 'GC-Project Manager') {
              if (item.projectRole == 'GC-Project Admin' || item.projectRole == 'GC-Project Manager') {
                item['isProjectUser'] = true;
              }
            } else {
              if (item.projectRole == 'GC-Project Admin') {
                item['isProjectUser'] = true;
              }
            }
          }
      });
      // console.log('success["payload"]', success['payload']);
      if(sessionStorage.getItem('associatedUser') == undefined) {
      this.associatedUser['projectUsers'] = success['payload'];
      } else {
        this.userAssociate = JSON.parse(sessionStorage.getItem('associatedUser'));
        this.associatedUser['projectUsers'] = this.userAssociate['projectUsers'];
      }
      console.log('this.associatedUser sdfdf', this.associatedUser);
      sessionStorage.setItem('projectUsers', JSON.stringify(this.associatedUser));
      //this.checkAddedUsers();
    }, (err) => {
      console.log('err', err);
    });
  }
  getFocus() {
    $(document).ready(function(){
      $("#createUserPopup").on('shown.bs.modal', function(){
        $(this).find('.description-focus').focus();
      });
    });
  }
  roleChange(item, index) {
    this.associatedUser.projectUsers[index] = item;
    sessionStorage.setItem('projectUser', JSON.stringify(this.associatedUser));
  }

  searchCompany(companyName) {
    const tableRowsClass = $('.available-comapany-row td .search-value');
    const ss = $('.available-comapany-row');
    const tableBody = $('.no-result-found');
    tableRowsClass.each( function(i, val) {
      const rowText = $(val).text().toString().toLowerCase();
      const inputText = companyName.toString().toLowerCase();
      if (rowText.indexOf( inputText ) == -1 ) {
        ss.eq(i).hide();
      } else {
        ss.eq(i).show();
      }
    });
    if (tableRowsClass.children(':visible').length == 0) {
      // console.log('companyName', companyName);
      sessionStorage.setItem('newCompany', companyName);
        $('#userNotFound').modal('show');
      //tableBody.append('<div class="no-entries">No entries found.</div>');
    } else {
      $('#userNotFound').modal('hide');
      $('.no-entries').remove();
    }
  }
  
  getAddUser(){
    $('#createUserPopup').modal('show')
  }
  addUser(item) {
    console.log('addUser item', item);
    // console.log('addUser this.projectDetails', this.projectDetails);
    if (this.projectDetails.projectRole == 'SC-Manager') {
      item['projectRole'] = 'SC-Observer';
    } else {
      item['projectRole'] = 'SC-Manager';
    }
    //item['projectRole'] = 'SC-Manager';
    
    this.associatedUser['projectUsers'].push(item);
    sessionStorage.setItem('associatedUser', JSON.stringify(this.associatedUser));
    sessionStorage.setItem('routing','true');
    sessionStorage.setItem('projectUser', JSON.stringify(this.associatedUser));
    this.checkAddedUsers();
  }

  removeCompany(index) {
    console.log('index', index);
    const id = this.associatedUser['projectUsers'][index].id;
    this.associatedUser['projectUsers'].splice(index, 1);
    this.associatedUser['deleteUsers'].push(id);
    sessionStorage.setItem('projectUser', JSON.stringify(this.associatedUser));
    sessionStorage.setItem('associatedUser',JSON.stringify(this.associatedUser));
    this.checkAddedUsers(id);
  }

  checkAddedUsers(id?) {
    // console.log('id checkAddedUsers', id);
    if (id) {
      this.availableUser.forEach(item => {
        if (item.id == id) {
          item['isAvailable'] = false;
        }
      });
    } else {
      // console.log('this.associatedUser["projectUsers"]', this.associatedUser);
      // console.log('this.availableUser', this.availableUser);
      // if (this.availableUser) {
        this.associatedUser['projectUsers'].forEach(item => {
          this.availableUser.forEach(availableItem => {
            if (item.id == availableItem.id) {
              availableItem['isAvailable'] = true;
            }
          });
        });
        this.companyChecked = true;
      // }
    }
  }

  clearUser() {
    this.user = '';
    this.searchCompany(this.user);
  }

  getCreateUserJson() {
    $('#userNotFound').modal("hide");
    this.restservice.getRenderingJson('create', "user-permission").subscribe(success =>{
      // console.log("success create user", success);
      this.panelData = success;
      this.panels = success['panels'];
      this.operationBtn = success['tools'];
      // console.log("this.panels", this.panels);
      this.panels.forEach(item => {
        // console.log('item', item);
        item['items'].forEach(element => {
          // console.log('element', element);
          if (element.bindingDbFieldName == 'lastName') {
            element.value = this.user;
          }
        });
      });
      this.form =this.formHandler.group(this.formValidation.toFormGroup(this.panels));
      // console.log("this.form createNewUser", this.form);
    }, (err) =>{
      console.log("err", err);
    });
    $('#createUserPopup').modal("show");
  }
  ngOnDestroy() {
    // sessionStorage.removeItem('associatedUser');
    // console.log("this.router.url ng destroy",this.router.url);
    if(this.router.url != '/authenticate/edit/project/user' && this.router.url != '/home')  {
      sessionStorage.removeItem('associatedUser');
    }
    // return false;
  }
}
