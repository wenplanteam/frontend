import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BuyLicenseComponent } from './buy-license.component';

describe('BuyLicenseComponent', () => {
  let component: BuyLicenseComponent;
  let fixture: ComponentFixture<BuyLicenseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BuyLicenseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BuyLicenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
