
import { Component, OnInit, Input } from '@angular/core';
import { RestserviceService } from '../../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../../emitter.service';
import { DatePipe } from '@angular/common'
import { environment } from '../../../environments/environment';
import { NgxSpinnerService } from 'ngx-spinner';
import { AscDescPipe } from '../../pipes/asc-desc/asc-desc.pipe';
import { GanttServices } from "../../services/gantt.services";
import { isEmpty } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { reduce } from 'rxjs-compat/operator/reduce';
import * as moment from 'moment';
import * as FileSaver from 'file-saver';

declare let $: any;

@Component({
  selector: 'percentageCompletion',
  templateUrl: './percentage-completion.component.html',
  styleUrls: ['./percentage-completion.component.scss']
})

export class PercentageCompletionComponent implements OnInit {

  /*Declare variable Block*/
  public xmark: boolean;
  public projectName: any;
  public projectDetails: any;
  public listData: any;
  public subTask = [];
  public createdId: any;
  public fromDate: any;
  public toDate: any;
  public projectRole: any;
  @Input() public selectedDateRange;
  public lookaheadDate: any;
  value: DoubleRange;
  public options = [{
    name: "Task No A-Z",
    order: 'asc',
    field: 'taskId'
  },
  {
    name: "Task No Z-A",
    order: 'desc',
    field: 'taskId'
  },
  {
    name: "Task Start Date A-Z",
    order: 'asc',
    field: 'startDate'
  },
  {
    name: "Task Start Date Z-A",
    order: 'desc',
    field: 'startDate'
  }

  ];
  // public options = ["Task No A-Z", "Task No Z-A","Task Start Date A-Z", "Task Start Date Z-A"];
  public query: any = ""
  public selectedValue: any = "sort"
  public sortType: any;
  public columname: any;
  public taskids = [];
  public subtaskids = [];
  public documents: Array<any> = [];
  public selectedDocuments: Array<any> = [];
  public getAllTask: any;
  public baseUrl = environment.baseUrl;
  public projectId: any;
  public projectCompany: any;
  public projectCompanyList: any = [];
  public loginDetails: any;
  public Issearch: boolean = false;
  public Isfilter: boolean = false;
  public viewOnlyAccess: any;
  public filterData: boolean = false;
  public pageNo: any;
  public pageSize: any;
  public clickPagination: boolean;
  public checkedValue = [];
  public filterArray = [];
  public masterData: any;
  public percentageCompletion: any;
  //public getFilteredData = [{}];
  public timeEnd: number;
  public totaltime: number;
  public noRecords: any;
  public filteredItems: {};
  public fiteredITems = {};
  public isDateClicked: boolean = false;
  // public Task_Type: [];
  // public Trade_Type: [];


  public filterItem = {
    "Task Status": ["On Track", "Need Attention", "Behind Schedule"],
    "Responsibility": [],
    "Task_Type": [],
    "Trade_Type": []
  };
  responsibility: any;
  public emailUsers: Array<string[]> = [];
  public emailArray: any = [];
  lookaheadSend: boolean;
  noUsers: boolean;
  updatesUser: any;
  /* End of Variable Block */

  constructor(private ascDesc: AscDescPipe, private ganttService: GanttServices, public router: Router, public restserviceService: RestserviceService, public route: ActivatedRoute, public emitter: EmitterService, private spinner: NgxSpinnerService) { }
  /* initialize the Component  */
  ngOnInit() {
    /* Get current project details from localStorage */

    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    this.getMasterData();
    this.getProjectCompany();
    this._get_Dates();
  }

  /* Get all master Data from DB including filters */
  getMasterData() {
    let filteringArray: any;
    this.masterData = this.route.snapshot.data['masterData']['payload'];
    console.log("getMasterData()", this.masterData)
    this.filterItem.Task_Type = (this.masterData['task_type']);
  }
  /* End of the master Data function */


  lookaheadFilter(data) {
    // alert()
    this.filterData = true;
    console.log(data, "filtyerData")
  }

  _get_Dates() {
    this.ganttService.lookaheadDateInfo(this.projectDetails.projectId).subscribe(data => {
      this.filterData = true;
      console.log("dates", data['payload'].lookaheadDate);
      sessionStorage.setItem("startDate", data['payload'].lookaheadDate);
      console.log("tradeData", data['payload'].lookaheadDate);
      //this.filterItem.Responsibility.push(data['payload'].projectCompany);
      this.filterItem.Trade_Type = data['payload'].trade;
      if(this.filterItem.Trade_Type == null || this.filterItem.Trade_Type == undefined){
        //alert("from trade if condition");
        this.filterItem.Trade_Type = ["No Trade Associated To This Project."];
      }
      console.log("for trade if condition",this.filterItem.Trade_Type);
      console.log(this.filterItem, "filtered obj");
      sessionStorage.setItem("filterItem", JSON.stringify(this.filterItem));
      this.getProjectCompany();



      $(document).ready((event) => {
        setTimeout(() => {
          $('#option-droup-demo').multiselect({
            buttonWidth: '160px',
            background: 'red',
            enableClickableOptGroups: false,
            enableCollapsibleOptGroups: true,
            collapseOptGroupsByDefault: true,
            includeResetOption: true,
            resetText: "X  Clear Filter",
            columns: 1,
            placeholder: 'Filter'
            // onChange:(element,checked,select)=>{
            // console.log("element",select,typeof element.val(),element.val());
            //}
          });
          $('#clear_filter').click((val) => {
            this.Isfilter = false;
            this.fiteredITems = {
              "accessibility": [],
              "ballInCourt": [],
              "scheduleAvailability": "",
              "task": [],
              "taskStatus": [],
              "trade": []
            };
            this.getResponse();

          })
        }, 1500);


        $('#option-droup-demo').change((val) => {

          console.log("val", val);
          var fiteredITems = $('#option-droup-demo').val();
          console.log("fiteres items", fiteredITems);

          fiteredITems = fiteredITems.map(value => {
            if (value.indexOf(':') > -1) {
              return value.split(":")[1].replace(/'/g, '').trim();
            }
            //return value.replace(/[0-9:']/g, '').trim();
          });
          console.log("filterItem", fiteredITems, this.filterItem);
          let filterKeys = Object.keys(this.filterItem);
          let filterObj = { 'Responsibility': 'ballInCourt', 'Task Status': 'taskStatus', 'Task_Type': 'task', 'Trade_Type': 'trade' };
          //let filterValues =Object.values(this.filterItem);
          let filterArr = {};
          filterKeys.forEach(value => {
            filterArr[filterObj[value]] = [];
            fiteredITems.forEach(v1 => {
              console.log(v1, value, this.filterItem[value].some(filtervalue => filtervalue.name));
              if (this.filterItem[value].some(filtervalue => filtervalue.projectCompanyId == v1)) {
                console.log(v1);
                filterArr[filterObj[value]].push(v1);
              } else {
                if (this.filterItem[value].indexOf(v1) > -1) {
                  console.log(v1);
                  filterArr[filterObj[value]].push(v1);
                }
              }

            })
          });
          filterArr['accessibility'] = [];
          filterArr['scheduleAvailability'] = "";
          console.log("filterArr", filterArr);
          this.fiteredITems = filterArr;
          console.log("this.fiteredITems", this.fiteredITems);

          if (this.fiteredITems != null) {
            this.Isfilter = true;
            this.getResponse();
          }
        });


      });


    })


  }


  /* routing & navigation function */
  _route_navigation() {
    this.router.navigateByUrl('/authenticate/list/project');
  }
  /* End of the routing function */

  /* Pagination function */
  getPaginationData(event) {
    this._sort_record(this.selectedValue);
    this.clickPagination = true;
    this.isDateClicked = false;
    this.pageNo = event.pageNo;
    this.pageSize = event.pageSize;
    this.Isfilter = false;
    this.Issearch = false;
    this.getResponse();

    // console.log(this.selectedValue)

  }
  /* End of the pagination */


  /* Get list of tasks and subtasks from the backend service call */
  getResponse() {

    // if(this.query == null || this.query == "" || !this.query){
    //   alert("im in search if")
    //   this.Issearch = false;
    // }
    let payload = {};
    //console.log("clickPagination", this.clickPagination, this.Issearch, this.Isfilter);

    if (this.clickPagination && !this.Issearch && !this.isDateClicked && !this.Isfilter) {
      payload = {
        "pageNo": this.pageNo,
        "pageSize": 10,
        "fromDate": this.fromDate,
        "projectId": this.projectDetails.projectId,
        "toDate": this.toDate,
        "search": this.Issearch,
        "searchTerm": this.query,
        "filter": this.Isfilter,
        "filterDTO": this.fiteredITems
      }
    } else {

      payload = {
        "pageNo": 1,
        "pageSize": 10,
        "fromDate": this.fromDate,
        "projectId": this.projectDetails.projectId,
        "toDate": this.toDate,
        "search": this.Issearch,
        "searchTerm": this.query,
        "filter": this.Isfilter,
        "filterDTO": this.fiteredITems
      }
    }
    this.spinner.show();
    // console.log("timestatrt", timeStart)
    this.restserviceService._getEffortcompletion(payload).subscribe(success => {
      this.spinner.hide();
      this.noRecords = success['success'];
      console.log("this.norecords", this.noRecords);
      this.listData = success['payload'];
      console.log("this.listDAta", this.listData);
      let subtask = [];
      console.log("success['pageable']", success['pageable'])
      if (success['pageable'] && success['pageable'].totalRecords != 0) {
        this.emitter.getPagination(success['pageable']);
      }
    });
  }
  /* End of service call for list page */

  /* Date Range picker function  ----> Date Range Picker Component */
  getDateRange(event) {
    // alert()
    this.isDateClicked = true;
    //alert(this.isDateClicked)
    console.log("event in dat-picer", event);
    let datePipe = new DatePipe('en-US');
    this.selectedValue = 'sort';
    this.fromDate = datePipe.transform(event.start['_d'], 'dd-MMM-yyyy');
    this.toDate = datePipe.transform(event.end['_d'], 'dd-MMM-yyyy');
    this.getResponse();

  }

  /* End of DatePicker Function */

  /* search the data from wholelist */
  _search_Record() {
    this.selectedValue = 'sort';
    if (this.query == "" || this.query == null || !this.query && this.clickPagination) {
      this.xclick();
      this.Issearch = false;
      this.pageNo = this.pageNo;
      alert("Please enter the word to Search");
    }
    else {
      this.Issearch = true;
      this.getResponse();
    }
  }
  /* end of the search function */
  xclick() {
    this.Issearch = false;
    this.pageNo =1;
    this.query = "";
    this.xmark = false;
    this.getResponse();
  }
  showXmark() {
    this.xmark = true;
  }

  /* Sort Tasks */
  _sort_record(event) {
    // if(this.clickPagination || this.clickPagination == true){
    //   alert("from if condiSS");
    //   event = [{}];
    //   this.selectedValue=[];
    // }else{
    console.log("evcent", event)
    let sortedData = []
    console.log("list", this.listData)
    sortedData = this.ascDesc.transform(this.listData, event.order, event.field)
    console.log("sortedData", sortedData);
    this.listData = sortedData;
  }

  getProjectCompany() {
    let projectId = { 'projectId': this.projectDetails.projectId };
    console.log('this.projectId', projectId);
    this.restserviceService.projectCompany(projectId).subscribe((success) => {
      this.projectCompanyList = success['payload']['projectCompanies'];
      sessionStorage.setItem('projectCompanyList', JSON.stringify(this.projectCompanyList));
      console.log("companyList", this.projectCompanyList);
      this.filterItem.Responsibility = success['payload']['projectCompanies'];
     // alert(this.filterItem.Responsibility);
      // .map(value => {
      //   return value.name;
      // });

      console.log('this.projectCompanyList', this.filterItem.Responsibility, this.projectCompanyList);
    }, err => {
      console.log('err', err);
    });
  }
  goRouterPage() {
    this.router.navigateByUrl('/authenticate/edit/project/details');
  }

  printpdf() {
    // console.log("from pdf function")
    let payload = {
      "projectId": this.projectDetails.projectId,
      "download": true,
      "fromDate": this.fromDate,
      "toDate": this.toDate,
      "search": this.Issearch,
      "searchTerm": this.query,
      "filter": this.Isfilter,
      "filterDTO": this.fiteredITems
    }
    this.restserviceService._pdfDownload(payload).subscribe(success => {
      console.log('success', success);
      const file = new Blob([success], { type: 'application/pdf' });
      let filename = 'percentageComplete.pdf';
      FileSaver.saveAs(file, filename);
    }, (err) => {
      if (err) {
        this.emitter.getMessage('No Data Found');
      }
    });
  }

  getMessage(data: string) {
    this.emailArray = data
    console.log("this.emailArry", this.emailArray)

  }

  sendProjectPlan(projectDetails) {
    const reqPayload = {
      "projectId": this.projectDetails.projectId,
      "download": false,
      "fromDate": this.fromDate,
      "toDate": this.toDate,
      "email": this.emailArray,
      "search": this.Issearch,
      "searchTerm": this.query,
      "filter": this.Isfilter,
      "filterDTO": this.fiteredITems
    }

    if (reqPayload.email.length > 0) {
      this.noUsers = false;
      this.restserviceService._sendEmail(reqPayload).subscribe(res => {
        console.log("success", res);
        if (res['success']) {
          this.emailUsers = [];
          $("#projectPlan").modal('hide');
          this.emitter.getMessage(res['message']);
        }
      }, (err) => {
        console.log('err', err);
        this.emailUsers = [];
        $('#projectPlan').modal('hide');
        this.emitter.getMessage(err['error']['message']);
      })
    } else {
      this.noUsers = true;
    };
  }

  /*send email get users list integration */
  goEmail() {
    this.emailUsers = [];
    let project = {
      projectId: this.projectDetails.projectId
    }
    console.log("after delete Array", this.emailArray);
    this.restserviceService.getSharedUsers(project).subscribe(success => {

      this.emailUsers = success['payload'];

    })

  }


}