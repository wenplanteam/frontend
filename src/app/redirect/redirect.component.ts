import { Component, OnInit } from '@angular/core';
import { Router,ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-redirect',
  templateUrl: './redirect.component.html',
  styleUrls: ['./redirect.component.scss']
})
export class RedirectComponent implements OnInit {

  constructor(private route: ActivatedRoute,private router:Router) { }

  ngOnInit() {
      this.route.queryParams.subscribe(params => {
        this.router.navigate([params['q']]);
      });
  }

}
