import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot,CanActivate,CanActivateChild,Router } from '@angular/router';
import { RestserviceService } from './restservice.service';

@Injectable({
  providedIn: 'root'
})
export class MasterService implements Resolve<any> {

  constructor(public restservice:RestserviceService) { }

resolve(route: ActivatedRouteSnapshot) {
    return  this.restservice.getMasterData();
}
}

@Injectable({
  providedIn: 'root'
})
export class ProjectCompanyService implements Resolve<any> {

  constructor(public restservice:RestserviceService) { }

  resolve(route: ActivatedRouteSnapshot) {
    const projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    //console.log('projectDetails', projectDetails);
    const companyPayload = {
      projectId: projectDetails.projectId
    };
    return  this.restservice.company(companyPayload);
  }
}


@Injectable({
  providedIn: 'root'
})
export class OnlyLoggedInUsersGuard implements CanActivate, CanActivateChild {

constructor(private router: Router) { }

canActivate() {
    if (sessionStorage.getItem('loginDetails') == '' || sessionStorage.getItem('loginDetails') == null) {
      return  true;
    } else {
       this.router.navigate(['authenticate/list/project']);
      return false;
    }
}
canActivateChild() {
    if (sessionStorage.getItem('loginDetails') != null && sessionStorage.getItem('loginDetails') != undefined){
      return  true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}