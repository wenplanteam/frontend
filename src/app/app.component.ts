import { Component,OnInit } from '@angular/core';
import { EmitterService } from './emitter.service';
import {ActivatedRoute, Router,Event, NavigationStart, NavigationEnd, NavigationError} from '@angular/router';

declare const $:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  
})
export class AppComponent {
  title = 'app';
  public message:any;
  public successmessage= "Email has been verified successfully";
  public routingId: any;
  public userIssue: boolean = false;

  constructor(public emitter: EmitterService, public router: Router, public route: ActivatedRoute) {
    router.events.subscribe((val) => {
      if(val['error']){
        this.router.navigate(['/login']);
      }
    });
  }

  ngOnInit(){
    this.emitter.responseActivatedRoute.subscribe(item => {
      if (item) {
        this.routingId = item;
      }
    });

    this.emitter.responseMessageSource.subscribe(item => {
      if (item) {
        this.message = item;
        $('#myModal').modal('show');
        if(this.message == 'You have not saved your work, would you like to leave anyway?') {
          this.userIssue = true;
          console.log('i am in userIssue true.');
        }
      }
    });
  }
  sendData(){
    this.emitter.changeMessage("");
    }
    unSavedPopup(data){
      this.emitter.unSaveDataTrigger(data);
      }
  goListPage(){
    this.router.navigate(['/authenticate/list/'+ this.routingId])
  }
  focusElement(){
    console.log('this.message',this.message);
    if(this.message == 'Your license has expired. Please purchase a license.') {
      this.router.navigate(['/authenticate/list/project']);
    }
    if(this.message=='Email Not Found'){
      if($('#emailId')){
        $('#emailId').focus();
      }
    }
    if(this.message=='Invalid password'){
      if($('#password')){
        $('#password').focus();
      }
    }
    if(this.router.url.match('forgotPassword')){
      this.router.navigate(['login']);
    }
    this.route.queryParams.subscribe((param)=>{
      if(param['source']=='sharedUser'){
        this.router.navigate(['login'],{"queryParams":{"email":param['email']}});
      }
    });
  }
  messageClick(page) {
    sessionStorage.removeItem('routing');
    if(page == 'ok') {
      let url = sessionStorage.getItem('url');
      console.log('ok ok',page,url);
      if(url == 'details') {
        this.router.navigate(['/authenticate/edit/project/details']);
        sessionStorage.removeItem('url');
      }
      if(url == 'planC') {
        this.router.navigate(['/authenticate/edit/project/configuration']);
        sessionStorage.removeItem('url');
      }
      if(url == 'lookahead') {
        this.router.navigate(['/authenticate/lookaheadplan']);
        sessionStorage.removeItem('url');
      }
      if(url == 'company') {
        this.router.navigate(['/authenticate/edit/project/company']);
        sessionStorage.removeItem('url');
      }
      if(url == 'user') {
        this.router.navigate(['/authenticate/edit/project/user']);
        sessionStorage.removeItem('url');
      }
      if(url == 'trade') {
        this.router.navigate(['/authenticate/edit/project/trade']);
        sessionStorage.removeItem('url');
      }
      if(url == 'landing') {
        this.router.navigate(['/authenticate/list/project']);
        sessionStorage.removeItem('url');
      }
      if(url == 'project') {
        this.router.navigate(['/authenticate/list/project']);
        sessionStorage.removeItem('url');
      }
      if(url == 'user-permission') {
        this.router.navigate(['/authenticate/list/user-permission']);
        sessionStorage.removeItem('url');
      }
      if(url == 'license') {
        this.router.navigate(['/authenticate/list/license']);
        sessionStorage.removeItem('url');
      }
      if(url == 'plan') {
        this.router.navigate(['/authenticate/list/plan']);
        sessionStorage.removeItem('url');
      }
      if(url == 'user-log') {
        this.router.navigate(['/authenticate/list/user-log']);
        sessionStorage.removeItem('url');
      }
      if(url == 'changePassword') {
        this.router.navigate(['./authenticate/changePassword']);
        sessionStorage.removeItem('url');
      }
      if(url == 'edit-profile') {
        this.router.navigate(['./authenticate/edit-profile']);
        sessionStorage.removeItem('url');
      }
    }

  }
  routerNavigate(url) {
    if(url == 'project') {
      this.router.navigate(['/authenticate/list/project']);
    }
    else {
      this.router.navigate(['/authenticate/buyLicense']);
    }
  }
}
