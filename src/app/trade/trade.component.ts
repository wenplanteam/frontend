import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import {FormValidationService} from '../form-validation.service';

declare let $: any;

@Component({
  selector: 'app-trade',
  templateUrl: './trade.component.html',
  styleUrls: ['./trade.component.scss',
  '../company/company.component.scss'
]
})
export class TradeComponent implements OnInit {

  public projectDetails: any;
  public routepage: any;
  public page: any;
  public operation: any;
  public panelData: any;
  public panels: any;
  public form: any;
  public formHandler: any;
  public associatedTrade: any = [];
  public availableTrade: any = [];
  public companyChecked = false;
  public trade: any;
  public projectTrade = {
    "projectId":'',
    "trade":[]
  }
  public newCompany: any;

  constructor(public restservice: RestserviceService, private router: Router, public fb:FormBuilder, public formValidation:FormValidationService) { 
    this.formHandler =fb;
  }

  ngOnInit() {
    
    
    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    let ss = this.router.url.split('/');
    this.routepage = ss[ss.length-1];
    this.page = ss[2];
    this.getJsonDataForProject(this.page, this.routepage);
    if(this.projectDetails){
      this.getCompanyById(this.projectDetails.projectId);
    }else{
      let id = JSON.parse(sessionStorage.getItem("listId"));
      this.getCompanyById(id.value);
    }
    this.getAllTrade();
    setTimeout(()=>{
      this.checkAddedTrade();
    }, 500);
  }

  getJsonDataForProject(operation, pageName){
    this.restservice.getRenderingJson(operation, pageName).subscribe(success =>{
      this.operation = success;
      this.panelData = success;
      this.panels=this.panelData['panels'];
      this.form =this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) =>{
      console.log("err", err);
    });
  }

  searchCompany(companyName){
    let tableRowsClass = $('.available-comapany-row td .search-value');
    let ss = $('.available-comapany-row');
    let tableBody = $('.no-result-found');
    tableRowsClass.each( function(i, val) {
      var rowText = $(val).text().toString().toLowerCase();
      var inputText = companyName.toString().toLowerCase();
      if( rowText.indexOf( inputText ) == -1 ){
        ss.eq(i).hide();
      }else{
        ss.eq(i).show();
      }
    });
    if(tableRowsClass.children(':visible').length == 0){
      sessionStorage.setItem("newCompany", companyName);
      if(!$(".no-result-found div").hasClass( "no-entries" )){
        tableBody.append('<div class="no-entries">No entries found.</div>');
        $(".no-entries").css({"padding":"20px 0px", "text-align":"center"});
      }
    }else{
      $(".no-entries").remove();
    }
  }

  getCompanyById(projectId){
    this.projectTrade.projectId = projectId;
    let payload = {
      projectId: projectId
    }
    this.restservice.trade(payload).subscribe(success => {
      this.associatedTrade=success['payload'];
    },(err)=>{
      console.log("err", err);
    });
  }



  getAllTrade(){
    this.restservice.getMasterData().subscribe(success => {
      //console.log("success", success);
      success['payload']['trade'].forEach(element => {
        let emptyObj = {}
        emptyObj['name'] = element;
        this.availableTrade.push(emptyObj);  
      });
    },(err)=>{
      console.log("err", err);
    });
  }

  addTrade(item){
    this.associatedTrade.push(item.name);
    this.projectTrade['trade'] = this.associatedTrade;
    sessionStorage.setItem('projectTrade', JSON.stringify(this.projectTrade));
    this.checkAddedTrade();
  }

  removeTrade(index){
    let removedTrade = this.associatedTrade[index];
    this.associatedTrade.splice(index, 1);
    this.projectTrade['trade'] = this.associatedTrade;
    sessionStorage.setItem('projectTrade', JSON.stringify(this.projectTrade));
    this.checkAddedTrade(removedTrade);
  }


  clearCompany(){
    this.trade = '';
    this.searchCompany(this.trade);
  }


  checkAddedTrade(removedTrade?){
    if(removedTrade){
      this.availableTrade.forEach(item =>{
        if(item.name == removedTrade){
          item['isAvailable'] = false;
        }
      })
    }else{
      this.associatedTrade.forEach(item =>{
        this.availableTrade.forEach(availableItem =>{
          if(item == availableItem.name){
            availableItem['isAvailable'] = true;
          }
        });
      });
    }
    this.companyChecked = true;
  }
}
