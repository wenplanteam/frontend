import { Component, OnInit, HostListener ,Input} from '@angular/core';
import { EmitterService } from '../emitter.service';
import { Router,NavigationEnd } from '@angular/router';
import { load } from 'ssf/types';
declare const $:any;

@Component({
  selector: 'app-sidenavbar',
  templateUrl: './sidenavbar.component.html',
  styleUrls: ['./sidenavbar.component.scss']
})
export class SidenavbarComponent implements OnInit {

  public projectDetails: any;
  public page: any;
  public url: any;
  public currentUrl:any;
  public getscrheight:number;
  public getscrwidth:number;
  public mobileScrDropdown:boolean = false;
  @Input() dynamicPageName: any;
  @HostListener('window:resize',['$event'])

 
  getScreenSize(event?){
    
    this.getscrheight = window.innerHeight;
    this.getscrwidth = window.innerWidth;
    console.log("screen width is :",this.getscrwidth+"X"+this.getscrheight)
    if(this.getscrwidth <= 991){
      this.mobileScrDropdown = true;
    }else {
      this.mobileScrDropdown = false;
    }
  }

  constructor(public emitter: EmitterService, public router: Router) { 
    router.events.filter((event: any) => event instanceof NavigationEnd)
        .subscribe(event => {
            this.currentUrl =event.url;
        });
       this.getScreenSize()
  }

  ngOnInit() {
   this.dynamicPageName =  sessionStorage.getItem('dynamicPageName')
    let ss = this.router.url.split('/');
    //console.log("ss",ss);
    this.page = ss[2];
    this.emitter.projectDetails.subscribe(item=>{
      if(item){
        this.projectDetails=true;
      }else{
        this.projectDetails=false;
      }
    });
    console.log('this.router.url',this.router.url);
  }
  

  pageRoute(url) {
    if(sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing')!=undefined) {
      this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
      sessionStorage.setItem('url',url);
    }
    else {
      if(url == 'details') {
       this.dynamicPageName = "Details"
        $('.details').addClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/details']);
      }
      if(url == 'planC') {
        this.dynamicPageName = "Plan Configuration"
        $('.details').removeClass('activeSec');
        $('.plan').addClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/configuration']);
      }
     else if(url == 'lookahead') {

        this.router.navigate(['/authenticate/lookaheadplan']);
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').addClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
      }
     else  if(url == 'company') {
        this.dynamicPageName = "Company Directory"
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').addClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/company']);
      }
    else  if(url == 'user') {
        this.dynamicPageName = "Users & Permissions"
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').addClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/user']);
      }
     else if(url == 'trade') {
        this.dynamicPageName = "Trade"
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').addClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/trade']);
      }
     else if(url == 'specDivision') {
      this.dynamicPageName = "Spec Division"
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').addClass('activeSec');
        $('.import-data').removeClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/specDivision']);
      }
      else if(url == 'import-data'){
        this.dynamicPageName = "Import Schedule"
        $('.details').removeClass('activeSec');
        $('.plan').removeClass('activeSec');
        $('.lookahead').removeClass('activeSec');
        $('.company').removeClass('activeSec');
        $('.user').removeClass('activeSec');
        $('.trade').removeClass('activeSec');
        $('.specDivision').removeClass('activeSec');
        $('.import-data').addClass('activeSec');
        this.router.navigate(['/authenticate/edit/project/import-data']);
      }
      
    }
  }

}
