import { Component, OnInit, AfterViewInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import { EmitterService } from '../../emitter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { Item } from 'angular2-multiselect-dropdown';
import { HostListener } from '@angular/core';
import { chownSync } from 'fs';
import { exit } from 'shelljs';


declare let $: any;
@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent implements OnInit, AfterViewInit {

  @Input() buttonData: any; //importing button for project create from create component
  @Input() panelData: any;  //importing the json payload for project create from create component.
  @Input() isDisabled: any;
  public formHandler: any;
  public message: any;
  public href: any;
  public id: any;
  public email: string;
  public emailUrlService: any;
  @Input() form: FormGroup;
  @Output() notifyInput: EventEmitter<string> = new EventEmitter<string>();
  addUser: any[];
  public shareProject: any;
  public addMember: any;
  public gcCompany: any;
  public projectDetail: any;
  scrHeight: number;
  scrWidth: number;
  dropdownMobile: boolean;

   
  @HostListener('window:load,resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth, "->>->>>>>>>");

    if(this.scrWidth > 320 && this.scrWidth < 576){
      this.dropdownMobile = true;
    }else {
      this.dropdownMobile = false;
    }
  }
  constructor(public restservice: RestserviceService, private http: HttpClient,
    public router: Router, private activatedRoute: ActivatedRoute, public formBuilder: FormBuilder, public emitter: EmitterService) {
    this.formHandler = formBuilder;
    this.getScreenSize()
  }

  //  disbledButton()
  //   {
  //     if (this.form.valid) {
  //     }
  //   }
  //   if(this.form){
  //     return this.form.valid;
  //   }else {
  //     return false;
  //   }  
  //  }

  validateAllFormFields(formGroup: FormGroup) {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateAllFormFields(control);
      }
    });
  }

  ngOnInit() {

    $(document).keydown((event) => {
      // console.log('event keycode',event.keycode);
      if (event.keyCode == 13) {

        // alert(this.buttonData.id)
        var x = document.getElementById(this.buttonData.id).click();
      }
    });
    let listId = JSON.parse(sessionStorage.getItem("listId"));
  }

  onhandleUserUpdated(e) {
    this.notifyInput.emit(e);
  }

  addForm(panelData, buttonData, form) {
    let addMember = {};
    if (form.valid) {
      panelData.panels.forEach(panelItem => {
        if (panelItem.type == 'simple') {
          panelItem.items.forEach(item => {
            if (item.bindingDbFieldName) {
              if (item.bindingDbFieldName == 'lookaheadAccess') {
                addMember[item.bindingDbFieldName] = item.value.map(k => k['itemName']);
              } else {
                addMember[item.bindingDbFieldName] = item.value;
              }
            } else {
            }
          })
        }
      });
      this.emitter.shareToPeople(addMember);
      form.reset();
    } else {
      this.validateAllFormFields(form);
    }
  }

  tocamelCase(str) {
    return str
      .replace(/\s(.)/g, function ($1) { return $1.toUpperCase(); })
      .replace(/\s/g, '')
      .replace(/^(.)/, function ($1) { return $1.toLowerCase(); });
  };

  goListPage() {
    this.router.navigate(['/authenticate/list/project'])
  }


  ngAfterViewInit() {
    const getId = <HTMLButtonElement>document.getElementById(this.buttonData.id);
    const obj = new ButtonComponent(this.restservice, this.http, this.router, this.activatedRoute, this.formHandler, this.emitter);
    if (this.buttonData.events) {
      for (const action of this.buttonData.events) {
        getId[action.type] = () => {
          for (const fn of action.functions) {
            if (typeof obj[fn.name] === 'undefined') {
            } else {
              obj[fn.name](this.panelData, this.buttonData, this.form);
            }
          }
        }
      }
    }
  }

  search(panelData) {
    let advancePayload = {};
    panelData["items"].forEach(panelItem => {
      let emtpyArray = [];
      let emtpyObject = {};
      if (panelItem.value) {
        advancePayload[panelItem.bindingDbFieldName] = panelItem.value;
      }
    })
    this.emitter.changeMessage(advancePayload);
    $('#advancedSearch-id').removeClass("show");
  }

  clear(panelData) {
    this.emitter.changeMessage({});
    panelData.panels.forEach(item => {
      item['items'].forEach(panelItem => {
        if (!panelItem.readOnly) {
          panelItem.value = "";
        }
      })
    })
  }

  cancel(panelData, buttonData) {
    if (buttonData.redirectUrl) {
      this.router.navigate([buttonData.redirectUrl]);
    } else {
      if (sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing') != undefined) {
        this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
        sessionStorage.setItem('url', 'project');
      } else {
        this.router.navigate(['/authenticate/list/project']);
      }
    }
  }

  done(panelData, buttonData, form) {
    if (panelData.restServiceUrl == 'pmc/project') {
      this.editProj(panelData, buttonData, form);
    }
    if (panelData.restServiceUrl == 'pmc/projectInfo') {
      this.saveSubItem(panelData, buttonData, form);
    }
    if (panelData.restServiceUrl == 'pmc/projectCompany') {
      this.saveCompany(panelData);
    }
    if (panelData.restServiceUrl == 'pmc/shareProject') {
      this.saveUserPermissons(panelData);
    }
    if (panelData.restServiceUrl == 'pmc/trade') {
      this.saveTrade(panelData);
    }
    if (panelData.restServiceUrl == 'pmc/specDivision') {
      this.saveSpecDivision(panelData);
    }
    this.router.navigate([buttonData.redirectUrl]);
  }

  /* project create post method server integration*/

  save(panelData, buttonData, form) {
    // console.log('panelData save', panelData);
    // alert("save")
    console.log('form data', form);
    let checkCaptcha = true;
    let uiStartTime = new Date().getTime();
    if (form.valid) {
      let uiEndTime = new Date().getTime();
      let displayString = (uiEndTime - uiStartTime) + ",";
      let createCustomRequestPayload = {} ;
      let amount = {
        'monthly': 0,
        'annually': 0
      }
      let extractDataStartTime = new Date().getTime();
      panelData.panels.forEach(item => {
        item['items'].forEach(panelItem => {
          if (panelItem.bindingDbFieldName == 'captcha') {
            if (panelItem.value != '') {
              checkCaptcha = false;
            }
          }
          let emtyArray = [];
          let emtyObject = {};
          if (item.panelName == 'Plan Details') {
            if (panelItem.bindingDbFieldName == 'monthly' || panelItem.bindingDbFieldName == 'annually') {
              if (panelItem.bindingDbFieldName == 'monthly') {
                amount.monthly = parseInt(panelItem.value);
              } else {
                amount.annually = parseInt(panelItem.value);
              }
              createCustomRequestPayload['amount'] = amount;
            } else {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
            }
          } else {
            if (panelItem.bindingDbFieldType == 'object') {
              panelItem.defaultObj['value'] = panelItem.value;
              emtyArray.push(panelItem.defaultObj);
            } else if (panelItem.bindingDbFieldType == 'integer') {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = parseInt(panelItem.value);
            } else {
              if (panelItem.bindingDbFieldName == 'email') {
                createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value.toLowerCase();
              } else {
                if (panelItem.bindingDbFieldName != 'termsOfService' && panelItem.bindingDbFieldName != 'captcha') {
                  createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
                }
              }
            }
          }
        });
      })
      let extractDataEndTime = new Date().getTime();
      displayString = displayString + (extractDataEndTime - extractDataStartTime) + ",";
      let serviceCallStart = new Date().getTime();
      if (panelData.restServiceUrl == "auth/user") {
        var password = (<HTMLInputElement>document.getElementById("password")).value;
        var confirmPassword = (<HTMLInputElement>document.getElementById("confirmPassword")).value;
        if (password != confirmPassword) {
          alert("Passwords do not match.");
          var passid = document.getElementById("confirmPassword").focus();
          (<HTMLInputElement>document.getElementById("confirmPassword")).selectionStart = 0;
          (<HTMLInputElement>document.getElementById("confirmPassword")).selectionEnd = (<HTMLInputElement>document.getElementById("confirmPassword")).value.length + 1;
          // this.router.navigate(['/registration']);
          return false;
        }
      }
      if (checkCaptcha) {
        //console.log("if check captcha" ,createCustomRequestPayload)
        this.restservice.create(createCustomRequestPayload, panelData.restServiceUrl).subscribe(success => {
          if (panelData.restServiceUrl == "pmc/project") {
            sessionStorage.setItem("projectCreate", JSON.stringify(success['payload']));
            this.emitter.getCreatedProject(success['payload']);
          }
          if (success) {
            let serviceCallEnd = new Date().getTime();
            displayString = displayString + (serviceCallEnd - serviceCallStart) + ",";
            if (panelData.restServiceUrl == "auth/user") {
              this.emitter.getMessage(success['message']);
              // this.router.navigate(['/landing']);
            } else {
              if (panelData.restServiceUrl == "auth/login") {
                const local = sessionStorage.setItem('loginDetails', JSON.stringify(success['payload']));
                this.emitter.getLoginDetails(success['payload']);
                this.emitter.updateLogin(success['payload']);
                this.restservice.gcByUser().subscribe(suc => {
                  sessionStorage.setItem('gcCompany', JSON.stringify(suc['payload']));
                  if (suc['payload'].length > 1) {
                    this.router.navigate(['/selectGc']);
                  } else {
                    this.router.navigate(['/license-expired']);
                  }
                }, (err) => {
                  console.log('err', err);
                });
              }
            }
            if (panelData.restServiceUrl != "auth/login") {
              this.emitter.getMessage(success['message']);
            }
            if (buttonData.redirectUrl) {
              this.router.navigate([buttonData.redirectUrl]);
            }
          }
        }, (err) => {
          if (err['error']['message'] == "Email Already Exist") {
            this.emitter.getMessage(err['error']['message']);
            $(document).ready(function () {
              $(".msgBtn").click(function () {
                var eid = document.getElementById("businessEmailId").focus();
                (<HTMLInputElement>document.getElementById("businessEmailId")).selectionStart = 0;
                (<HTMLInputElement>document.getElementById("businessEmailId")).selectionEnd = (<HTMLInputElement>document.getElementById("businessEmailId")).value.length + 1;
              });
            });
          }
          if (err['error']['message']) {
            this.emitter.getMessage(err['error']['message']);
          }
        })
      }
    } else {
      this.validateAllFormFields(form);
    }
  }

  savePopup(panelData, buttonData, form) {
    // alert("popup")
    let uiStartTime = new Date().getTime();
    if (form.valid) {
      let uiEndTime = new Date().getTime();
      let displayString = (uiEndTime - uiStartTime) + ",";
      let createCustomRequestPayload = {};
      let extractDataStartTime = new Date().getTime();
      panelData.panels.forEach(item => {
        item['items'].forEach(panelItem => {
          let emtyArray = [];
          let emtyObject = {};
          if (panelItem.bindingDbFieldType == "object") {
            panelItem.defaultObj['value'] = panelItem.value;
            emtyArray.push(panelItem.defaultObj);
          } else if (panelItem.bindingDbFieldType == "integer") {
            createCustomRequestPayload[panelItem.bindingDbFieldName] = parseInt(panelItem.value);
          } else {
            if (panelItem.bindingDbFieldName == 'companyName') {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem['value'].name;
              createCustomRequestPayload['companyId'] = panelItem['value'].id;
            } else {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
            }
          }
        });
      })
      let extractDataEndTime = new Date().getTime();
      displayString = displayString + (extractDataEndTime - extractDataStartTime) + ",";
      let serviceCallStart = new Date().getTime();
      if (panelData.restServiceUrl == "auth/user") {
        var password = (<HTMLInputElement>document.getElementById("password")).value;
        var confirmPassword = (<HTMLInputElement>document.getElementById("confirmPassword")).value;
        if (password != confirmPassword) {
          alert("Passwords do not match.");
          var passid = document.getElementById("confirmPassword").focus();
          (<HTMLInputElement>document.getElementById("confirmPassword")).selectionStart = 0;
          (<HTMLInputElement>document.getElementById("confirmPassword")).selectionEnd = (<HTMLInputElement>document.getElementById("confirmPassword")).value.length + 1;
          return false;
        }
      }
      this.restservice.create(createCustomRequestPayload, panelData.restServiceUrl).subscribe(success => {
        if (success) {
          if (panelData.restServiceUrl == 'ulc/provideLicense') {
            $('#giveToModal').modal('hide');
          } else {
            $('#createModel').modal('hide');
          }
          if (panelData.restServiceUrl == 'ulc/company') {
            $('#createCompanyPopup').modal('hide');
          }
          if (panelData.restServiceUrl == 'ulc/createUserByUserInfo') {
            $('#createUserPopup').modal('hide');
            // }else{
          }
          this.emitter.getActivatedRoute();
          this.emitter.getMessage(success['message']);
          this.emitter.changeMessage({});
          this.emitter.getMessage('');
          this.emitter.getDataSource({});
        }
      }, (err) => {
        console.log('err', err);
        if (err['error']['message']) {
          this.emitter.getMessage(err['error']['message']);
        }
      });
    } else {
      this.validateAllFormFields(form);
    }
  }

  saveSubItem(panelData, buttonData, form) {
  // alert("subs")

    console.log("form", form)
    console.log("panelData", panelData)
    console.log("button", buttonData)
    let id = JSON.parse(sessionStorage.getItem("listId"));
    let projectDetail = JSON.parse(sessionStorage.getItem("projectDetails"));

    let reqPayload = {
      "type":[], 
      "information":[],
       "leaveDays":[]
      
    };
    if (projectDetail) {
      reqPayload['projectId'] = projectDetail.projectId;
    } else {
      reqPayload['projectId'] = id.value;
    }

    panelData.panels.forEach(panels => {
      // console.log("panels in button", panels)
      if (panels.type == "simple") {
        panels.items.forEach(simplePanelItem => {
          if (simplePanelItem.bindingDbFieldType == "integer") {
            reqPayload[simplePanelItem.bindingDbFieldName] = parseInt(simplePanelItem.value);
          } else {


            // console.log(" simplePanelItem.value", simplePanelItem.value)
            reqPayload[simplePanelItem.bindingDbFieldName] = simplePanelItem.value;
          }
        });
      }
      if(panels.type == 'radioButton'){
        panels.items.forEach(radiobuttonItem =>{
         
                     console.log("radiobuttonItem123", radiobuttonItem)
                   

                    if(radiobuttonItem.value == 'Daily'){     
                      // alert("daily")                  
                      reqPayload['frequency'] =  'Daily';

                    }else if(radiobuttonItem.value == 'Weekly'){
                      reqPayload['frequency'] =  'Weekly';
                      reqPayload['dayOfTheWeek'] = sessionStorage.getItem('frequencyDay');
                    }
                  
          
        })
      }
      if (panels.type == "multichekbox") {
        console.log("panels", panels)
        panels.items.forEach(multiCheckpanelItem => {
          // console.log("multiCheckpanelItem ", multiCheckpanelItem )
          let leaveDays = [];
          let information = [];
          let frequency = [];
          let type = [];
          multiCheckpanelItem.subItem.forEach(subItem => {
            // console.log("subitem", subItem)
            if (subItem.value && panels.panelName == "Project Plan Configuration") {
              leaveDays.push(subItem.checkedValue);
              reqPayload["leaveDays"] = leaveDays;
            } else if (subItem.value && panels.panelName == "Notification Information") {
                     console.log("subItem.value", subItem)

                information.push(subItem.checkedValue)
                reqPayload['information'] = information;
              // if(subItem.checkedValue === "ProjectTaskSummary"){
              //   reqPayload['information'].push('ProjectTaskSummary') 
 
              // }
              // else if(subItem.checkedValue != 'ProjectTaskSummary') {
                    
              //      information.push(subItem.checkedValue)
              //   reqPayload['information'] = information;
       
              // }
              // console.log('req payload information',reqPayload['information']);
              // else if(subItem.checkedValue != 'ProjectTaskSummary' && information.length <= 4){
              //   information.push(subItem.checkedValue)
              //   reqPayload['information'] = information;
               
            } else if (subItem.value && panels.panelName == "Notification Type") {
              type.push(subItem.checkedValue);
              reqPayload['type'] = type;
            } else {
              console.log("error")
            }
          });


        });
      }
      if (panels.type == "floatDaysText") {
        panels.items.forEach(floatDaysPanel => {
          // console.log('floatDaysPanel', floatDaysPanel);
          reqPayload[floatDaysPanel.bindingDbFieldName] = parseInt(floatDaysPanel.value);
        })
      }
    });
    if (reqPayload["leaveDays"].length ==  0) {
      this.emitter.getMessage('Please select atleast one off day');
    } else if (reqPayload['weekStartDay'] == '') {
      this.emitter.getMessage('Please select first day of week');
    } 
    else if(reqPayload['notification']  && reqPayload['type'].length == 0 ){
      this.emitter.getMessage('Please select Notification Type');     
    }
    else if(reqPayload['notification']  && reqPayload['information'].length == 0 ){
      this.emitter.getMessage('Please select Notification information');     
    } 
     else{
      console.log('req Payload', reqPayload);
      this.restservice.create(reqPayload, panelData.restServiceUrl).subscribe(success => {
        this.emitter.getMessage(success['message']);
      }, (err) => {
        console.log("err", err);
      });
    }
  }

  // Save Project Company

  saveCompany(panelData) {
    // alert("12")
    const x = JSON.parse(sessionStorage.getItem('projectCompany'));
    x['projectCompanies'].forEach(element => {
      delete element.isProjectCompany;
      delete element.isActive;
      delete element.createdBy;
      delete element.createdOn;
      delete element.modifiedBy;
      delete element.modifiedOn;
      delete element.isAvailable;
    });
    this.restservice.create(x, panelData.restServiceUrl).subscribe(success => {
      this.emitter.getMessage(success['message']);
    }, (err) => {
      console.log("err", err);
    });
  }

  /* Save Project Company */

  saveTrade(panelData) {
    let x = JSON.parse(sessionStorage.getItem("projectTrade"));
    if (x) {
      this.restservice.create(x, panelData.restServiceUrl).subscribe(success => {
        this.emitter.getMessage(success['message']);
      }, (err) => {
        // console.log("err", err);
      });
    }
  }

  saveSpecDivision(panelData) {
    let x = JSON.parse(sessionStorage.getItem("projectSpec"));
    if (x) {
      this.restservice.create(x, panelData.restServiceUrl).subscribe(success => {
        this.emitter.getMessage(success['message']);
      }, (err) => {
        // console.log("err", err);
      });
    }
  }

  /* Save Project Company */

  saveUserPermissons(panelData) {
    const x = JSON.parse(sessionStorage.getItem('projectUser'));
    x['projectUsers'].forEach(element => {
      delete element.isProjectUser;
      delete element.isProjectUserEdit;
      delete element.isAvailable;
    });
    this.restservice.create(x, panelData.restServiceUrl).subscribe(success => {
      this.emitter.getMessage(success['message']);
    }, (err) => {
      console.log('err', err);
    });
  }
  /* project Update put method server integration*/

  editProj(panelData, buttonData, form) {
    // console.log('buttonData',buttonData,form,panelData);
    if (form.valid) {
      let createCustomRequestPayload = {};
      let amount = {
        'monthly': 0,
        'annually': 0
      }
      let extractEditDataStart = new Date().getTime();
      panelData.panels.forEach(item => {
        item['items'].forEach(panelItem => {
          // if( panelItem.name == 'projectName') {
          //    let projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
          //    console.log('projectsDetails',projectDetails);
          //    projectDetails.projectName =  panelItem.value;
          //    sessionStorage.setItem('projectDetails',JSON.stringify(projectDetails));
          // }
          let emtyArray = [];
          let emtyObject = {};
          if (item.panelName == 'Plan Details') {
            if (panelItem.bindingDbFieldName == 'monthly' || panelItem.bindingDbFieldName == 'annually') {
              if (panelItem.bindingDbFieldName == 'monthly') {
                amount.monthly = parseInt(panelItem.value);
              } else {
                amount.annually = parseInt(panelItem.value);
              }
              createCustomRequestPayload['amount'] = amount;
            } else {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
            }
          } else {
            if (panelItem.bindingDbFieldType == "object") {
              panelItem.defaultObj['value'] = panelItem.value;
              emtyArray.push(panelItem.defaultObj);
            } else if (panelItem.bindingDbFieldType == "integer") {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = parseInt(panelItem.value);
            } else {
              if (panelItem.bindingDbFieldName == 'companyName') {
                createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem['value'].name;
              } else {
                createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
              }
            }
          }
        });
      })
      let extractEditDataEnd = new Date().getTime();
      let editDisplayString = (extractEditDataEnd - extractEditDataStart) + ",";
      let editServiceStart = new Date().getTime();
      this.restservice.edit(createCustomRequestPayload, panelData.fetchRestServiceUrl).subscribe(success => {
        if (success) {
          const navicate = this.router.url.split('/');
          const ss = JSON.parse(sessionStorage.getItem('loginDetails'));
          if (navicate[3] == 'user-permission' && ss['id'] == success['payload']['id']) {
            ss['firstName'] = success['payload']['firstName'];
            ss['lastName'] = success['payload']['lastName'];
            this.emitter.getLoginDetails(ss);
            sessionStorage.setItem('loginDetails', JSON.stringify(ss));
          }
          let editServiceEnd = new Date().getTime();
          editDisplayString = editDisplayString + (editServiceEnd - editServiceStart) + ",";
          this.emitter.getMessage(success['message']);
          this.router.navigate([buttonData.redirectUrl]);
        }
      }, (err) => {
        console.log("err", err);
        if (err) {
          this.emitter.getMessage(err['error']['message']);
        }
      })
    } else {
      this.validateAllFormFields(form);
    }
  }

  addFunction(panel) {
    panel.items.push(panel.items[0]);
  }

  removeFunction(panel, buttonData) {
    let index = panel.items
    panel.items.splice(index, 1);
  }
  changePassword(panelData, buttonData, form) {
    if (form.valid) {
      if (this.router.url != '/forgotPassword') {
        const password = (<HTMLInputElement>document.getElementById('newPassword')).value;
        const confirmPassword = (<HTMLInputElement>document.getElementById('confirmPassword')).value;
        if (password != confirmPassword) {
          alert('Passwords do not match.');
          const passid = document.getElementById('confirmPassword').focus();
          (<HTMLInputElement>document.getElementById('confirmPassword')).selectionStart = 0;
          (<HTMLInputElement>document.getElementById('confirmPassword')).selectionEnd = (<HTMLInputElement>document.getElementById('confirmPassword')).value.length + 1;
          // this.router.navigate(['/registration']);
          return false;
        }
      }
      let createCustomRequestPayload = {};
      let extractEditDataStart = new Date().getTime();
      panelData.panels.forEach(item => {
        item['items'].forEach(panelItem => {
          let emtyArray = [];
          let emtyObject = {};
          if (panelItem.bindingDbFieldName != 'confirmPassword') {
            if (panelItem.bindingDbFieldType == 'object') {
              panelItem.defaultObj['value'] = panelItem.value;
              emtyArray.push(panelItem.defaultObj);
            } else if (panelItem.bindingDbFieldType == 'integer') {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = parseInt(panelItem.value);
            } else {
              createCustomRequestPayload[panelItem.bindingDbFieldName] = panelItem.value;
            }
          }
        });
      })
      let extractEditDataEnd = new Date().getTime();
      let editDisplayString = (extractEditDataEnd - extractEditDataStart) + ",";
      let editServiceStart = new Date().getTime();
      this.restservice.create(createCustomRequestPayload, panelData.restServiceUrl).subscribe(success => {
        if (success) {
          this.emitter.getMessage(success['message']);
          let editServiceEnd = new Date().getTime();
          editDisplayString = editDisplayString + (editServiceEnd - editServiceStart) + ",";
          let x = JSON.parse(sessionStorage.getItem('loginDetails'));
          x.token = success['payload']['token'];
          this.emitter.getMessage(success['message']);
          this.router.navigate(['/authenticate/list/project']);
          form.reset();
        }
      }, (err) => {
        this.emitter.getMessage(err['error']['message']);
      })
    } else {
      this.validateAllFormFields(form);
    }
  }
}
