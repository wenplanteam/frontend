import { Component, OnInit, AfterViewInit, Input, HostListener, EventEmitter, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../../emitter.service';
declare let $: any;
@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.scss']
})
export class FilterComponent implements OnInit {
  @Output() goFilter: EventEmitter<any> = new EventEmitter();
  @Input() filter: any;
  public filterData: any;
  public routingId: any;
  scrHeight: any;
  scrWidth: any;

  @HostListener('window:resize , load', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    console.log(this.scrHeight, this.scrWidth, "->>->>>>>>>");
    if (this.scrWidth < 992) {
      $("#fill-head").addClass("dropdown-toggle");
      $(".form-group").addClass("dropdown-menu");
      $(".menu-filter").addClass("dropdown-item");
      $("#fa-icon").addClass("fa fa-caret-down")
    } else if (this.scrWidth > 992) {
      $("#fill-head").removeClass("dropdown-toggle");
      $(".form-group").removeClass("dropdown-menu");
      $(".menu-filter").removeClass("dropdown-item");
      $("#fa-icon").removeClass("fa fa-caret-down")
    }
   
  }
  constructor(public restservice: RestserviceService, public router: Router, public route: ActivatedRoute, public emitter: EmitterService, public formBuilder: FormBuilder) {
    this.getScreenSize()
  }

  ngOnInit() {

    if (this.scrWidth < 992) {
      $("#fill-head").addClass("dropdown-toggle");
      $(".form-group").addClass("dropdown-menu");
      $(".menu-filter").addClass("dropdown-item");
      $("#fa-icon").addClass("fa fa-caret-down")
    } else if (this.scrWidth > 992) {
      $("#fill-head").removeClass("dropdown-toggle");
      $(".form-group").removeClass("dropdown-menu");
      $(".menu-filter").removeClass("dropdown-item");
      $("#fa-icon").removeClass("fa fa-caret-down")
    }
    setTimeout(() => {
      this.route.params.subscribe(params => {
        this.routingId = params['id'];
      });
    }, 500)
    this.emitter.getRouterUrl();
  }

  setFilter(field, value, item) {
    item.forEach((data) => {
      if (field != data.bindingDbFieldName)
        data.value = false;
    });

    let filterObj = {};
    if (!value) {
      let filterData = {};
      filterData[field] = !value;
      filterObj[this.routingId] = filterData
    }
    this.goFilter.emit(filterObj);
  }
}
