import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {RestserviceService} from '../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';
declare const $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  providers: [FormBuilder]
})
export class LoginComponent implements OnInit {
  public loginUser: any;
  form: FormGroup;

  public formHandler: any;
  public panels: any;
  public page: any;
  public routepage: any;
  public panelItems: any;
  public operation: any;
  public formObj;
  public panelData: any;
  public validation: any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router,
    private route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {

    setTimeout(() => {
     // console.log("panels", this.panels);
      this.route.queryParams.subscribe((params) => {
        // console.log("params", params);
        const email = params['email'];
        if (email) {
          this.panels.forEach(panelItem => {
            // console.log("panelItem", panelItem);
            panelItem.items.forEach(item => {
              // console.log("item", item);
              if (item.bindingDbFieldName == 'email') {
                item.value = email;
              }
            });
          });
        }
      });
    }, 500);

    setTimeout(() => {
    const confirmMail = sessionStorage.getItem('confirmMail');
    if (confirmMail) {
      this.panels.forEach(panelItem => {
        panelItem.items.forEach(item => {
          if (item.bindingDbFieldName == 'email') {
            item.value = confirmMail;
          }
        });
      });
    }
  }, 100);


  this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
  const currentUrl = this.router.url;
  this.page = 'create';
  this.getJsonDataForLogin(this.page, 'login');
  if (sessionStorage.getItem('userDetails')) {
      this.router.navigate(['/authenticate/landing']);
    }
  }
  onhandleUserUpdated(e) {
    this.notifyPanel.emit(e);
  }

  /* Remove visible false from rendering json*/

  removeByAttr(arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {
        arr.splice(i, 1);
      }
    }
    return arr;
  }

  /* Get UI Rendering JSON for create from Server */

  getJsonDataForLogin(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      const buttonData = success;
      this.operation = buttonData;
      console.log(this.operation);
      
      this.panelData = buttonData;
      this.panelData.panels.forEach(item => {
        this.removeByAttr(item.items, 'visible', '');
      });
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }

  getValidations() {
    this.restservice.getValidations().subscribe(success => {
      sessionStorage.setItem('validationMessages', JSON.stringify(success));
    }, (err) => {
      console.log('err', err);
    });
  }
}
