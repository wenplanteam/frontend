// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  baseUrl : "http://13.58.21.163/api/emzee/",
  // baseUrl : "http://192.168.1.160:9900/api/emzee/"
  // baseUrl : "http://192.168.1.184:9900/api/emzee/"

};

//baseUrl : "http://18.217.143.236:9900/api/emzee/"
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw errorhttp://18.217.143.236
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
//http://13.58.21.163