import { Component, OnInit, Directive, Input,HostListener } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { EmitterService } from '../emitter.service';
import { RestserviceService } from '../restservice.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { GanttChartComponent } from '../components/ganttchart/ganttchart.component';

declare const $: any;
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
// @Directive({
//   selector: '[routerLinkActive]',
//   exportAs: 'routerLinkActive'
// })

export class HeaderComponent implements OnInit {

  public routerUrl: any;
  public userDetails: any;
  public logoutDisable: boolean = false;
  public searchItem: any;
  public selectedLanguage: any;
  public languageData: any;
  public currentRoutingUrl: any;
  public isUserNameVisible: boolean = false;
  public firstChar: any;
  public loginDetailData: any;
  public gcCompany: any;
  public gcCompanyPayload: any;
  public localGcCompany: any;
  public gcCreateCompany: any;
  public selectedGcData: any;
  public selectedCompanyProject: any;
  public superAdminLogin: any;
  public close = true;
  scrHeight: number;
  scrWidth: number;
  public listFormobile:boolean = false;
  @HostListener('window: load ,resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    console.log(this.scrHeight, this.scrWidth, "->>->>>>>>>");
 
  }
  constructor(public router: Router, public activatedRoute: ActivatedRoute,
    public emitter: EmitterService, public restservice: RestserviceService, private spinner: NgxSpinnerService) { 
      this.getScreenSize();
    }


  //   showSpinner() {
  //     this.spinner.show();
  //     setTimeout(() => {
        
  //         this.spinner.hide();
  //     }, 9000);
    
  // }



  ngOnInit() {
// this.showSpinner();

  //   $(document.body).click( ()=> {
  //     console.log("hi body", this.close);
  //     if(!this.close){
  //       this.closeNav();
  //     }else{
  //       this.openNav();      
  //     }
  // });

    if (sessionStorage.getItem('gcCompany') != null && sessionStorage.getItem('gcCompany') != undefined) {
      console.log('header loaded1');
      this.localGcCompany = JSON.parse(sessionStorage.getItem('gcCompany'));
      if (sessionStorage.getItem('popupGcName') != null && sessionStorage.getItem('popupGcName') != undefined)
        this.gcCompany = parseInt(sessionStorage.getItem('popupGcName'));
      else {
        this.gcCompany = JSON.parse(sessionStorage.getItem('gcCompany'))[0]['userId'];
      }
      this.getCreateCompanyName(this.gcCompany);
    }
    this.emitter.getSendGcCompanyFromList.subscribe((itemFromList: any) => {
      this.localGcCompany = JSON.parse(itemFromList);
      this.gcCompany = JSON.parse(itemFromList)[0]['userId'];
      this.getCreateCompanyName(this.gcCompany);
    });
    this.selectedGcData = this.emitter.selectedGcData.subscribe(item => {
      if (item) {
        this.localGcCompany = JSON.parse(sessionStorage.getItem('gcCompany'));
        this.gcCompany = parseInt(item);
      }
      this.getCreateCompanyName(this.gcCompany);
    });
    $(document).ready(function () {
      $('.cus-tooltip').tooltip();
    });
    this.isUserNameVisible = false;
    this.loginDetailData = this.emitter.loginDetailData.subscribe(item => {
      const userDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
      if (item) {
        this.emitter.updateLogin('');
        this.userDetails = item;
        this.userDetails = item;
        this.setDisplayChar(item);
      } else {
        this.userDetails = userDetails;
        if (this.userDetails) {
          this.setDisplayChar(this.userDetails);
        }
      }
    });

    this.emitter.routerUrlSource.subscribe(item => {
      this.searchItem = '';
    });

    this.currentRoutingUrl = this.router.url;
    if (this.router.url.match('/login')) {
      this.isUserNameVisible = true;
    }
    $(document).ready(function () {
      $('#dropDownId').change(function () {
        window.location.reload(true);
      });
    });
    this.emitter.routerUrlSource
      .subscribe(item => {
        if (item.match('list')) {
          this.routerUrl = true;
        } else if (item.match('landing')) {
          this.routerUrl = false;
        } else {
          this.routerUrl = false;
        }
      });

    this.emitter.userRoleDetails.subscribe(item => {
      this.superAdminLogin = item;
    });
  }

  setDisplayChar(userDetails) {
    const splitUserDetails = userDetails.lastName.split('');
    this.firstChar = splitUserDetails[0];
  }
  getCreateCompanyName(id) {
    console.log('id id',id);
    sessionStorage.setItem('popupGcName',id);
    const x = this.localGcCompany.filter(item => item.userId == id);
    this.gcCreateCompany = x[0].createCompanyName;
    this.emitter.gcCompanyId(this.gcCompany);
    console.log('this.gcCompany',this.gcCompany);
  }

  openNav() {
    this.close = false;
    this.listFormobile = false;
    if(this.scrWidth > 576){
      document.getElementById("mySidenav").style.width = "220px";
    } else{
      this.listFormobile = true;
      document.getElementById("mySidenav").style.width = "100%";
      document.getElementById("mySidenav").style.top = "2px";
     
      // console.log("document.getElementById", document.getElementById("mySidenav").style)
    }
   
  }
  closeNav() {
  // alert()
    this.close = true;
    document.getElementById("mySidenav").style.width = "0";
  }

  routeNav(page) {
    if(sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing')!=undefined) {
      this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
      sessionStorage.setItem('url',page);
      document.getElementById("mySidenav").style.width = "0";
    } 
     else if(sessionStorage.getItem('unsaved') != null && sessionStorage.getItem('unsaved')!=undefined){
      this.emitter.getMessage('You have unsaved data on this page. How do you want to proceed?');
      sessionStorage.setItem('status',page);
      document.getElementById("mySidenav").style.width = "0";
     }
    else {
    this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() =>
      this.router.navigate(["/authenticate/list/" + page])
    );
    document.getElementById("mySidenav").style.width = "0";
  }
  }
  

  otherRoute(page) {
    this.router.navigate(["/authenticate/" + page])
    document.getElementById("mySidenav").style.width = "0";
  }

  logout() {
    document.getElementById("mySidenav").style.width = "0";
    sessionStorage.clear();
    sessionStorage.clear();
    this.loginDetailData.unsubscribe();
    this.emitter.loginDetailData.subscribe(item => {
      const userDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
      if (item) {
        this.emitter.updateLogin('');
        this.userDetails = item;
        this.userDetails = item;
        this.setDisplayChar(item);
      } else {
        this.userDetails = userDetails;
        if (this.userDetails) {
          this.setDisplayChar(this.userDetails);
        }
      }
    });
  }
  pageRoute(url) {
    if(sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing')!=undefined) {
      this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
      sessionStorage.setItem('url',url);
    } else if(sessionStorage.getItem('unsaved') != null && sessionStorage.getItem('unsaved')!=undefined){
      this.emitter.getMessage('You have unsaved data on this page. How do you want to proceed?');
      sessionStorage.setItem('status',url);
    }
    else {
      //this.router.navigate([url]);
      this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() =>
      this.router.navigate(["/authenticate/list/project"])
    );
    }
  }
  headerUrl(url){
    if(sessionStorage.getItem('routing') != null && sessionStorage.getItem('routing')!=undefined) {
      this.emitter.getMessage('You have not saved your work, would you like to leave anyway?');
      sessionStorage.setItem('url',url);
    } else if(sessionStorage.getItem('unsaved') != null && sessionStorage.getItem('unsaved')!=undefined){
      this.emitter.getMessage('You have unsaved data on this page. How do you want to proceed?');
      sessionStorage.setItem('status',url);
    }
    
    else {
      if(url == 'changePassword') {
        this.router.navigate(['./authenticate/changePassword']);
      }
      if(url == 'edit-profile') {
        this.router.navigate(['./authenticate/edit-profile']);
      }
    }
  }
}
