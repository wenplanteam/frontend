import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators} from '@angular/forms';
import {FormValidationService} from '../form-validation.service';
import { RestserviceService } from '../restservice.service';
import { Router } from '@angular/router';
import { CompanyComponent } from 'src/app/company/company.component';

declare let $ :any;
@Component({
  selector: 'app-spec-division',
  templateUrl: './spec-division.component.html',
  styleUrls: ['./spec-division.component.scss',
  '../company/company.component.scss']
})
export class SpecDivisionComponent implements OnInit {
  public projectDetails:any;
  public routepage:any;
  public page: any;
  public availableSpecDivision: any = [];
  public associatedSpecDivision: any = [];
  public companyChecked = false;
  public operation: any;
  public panelData: any;
  public panels:any;
  public form: any;
  public formHandler: any;
  public specDivision:any;
  public projectSpec = {
    "projectId":'',
    "specDivision":[]
  }
  public newCompany: any;

  constructor(public restservice : RestserviceService , private router: Router, public fb:FormBuilder, public formValidation:FormValidationService) {
    this.formHandler = fb;
   }

  ngOnInit() {

    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    let ss = this.router.url.split('/');
    this.routepage = ss[ss.length-1];

    this.page = ss[2];
    console.log("Spec-Division", this.page, this.routepage)
    this.getJsonDataForProject(this.page,this.routepage);
    if(this.projectDetails){
      this.getCompanyById(this.projectDetails.projectId);
    }else{
      let id = JSON.parse(sessionStorage.getItem("listId"));
      this.getCompanyById(id.value);
    }
    this.getAllSpecDivision();
    setTimeout(()=> {
      this.checkAddedSpecDivision();
    },500);
  }

  getJsonDataForProject(operation,pageName){
    this.restservice.getRenderingJson(operation,pageName).subscribe(success =>{
      this.operation = success;
      this.panelData = success;
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    },(err)=>{
      console.log('error',err);
    });
  }

  // searchCompany(companyName){
  //   let tableRowsClass = $('.available-comapany-row td .search-value');
  //   let ss =$('.available-compony-row');
  //   let tableBody =$('.no-result-found');
  //   tableRowsClass.each(function(i,val){
  //     var rowText = $(val).text().toString.toLowerCase();
  //     var inputText = companyName.toString.toLowerCase();
  //     if(rowText.indexOf(inputText) == -1){
  //       ss.eq(i).hide();
  //     }else{
  //       ss.eq(i).show();
  //     }
  //   }); 
  //   if(tableRowsClass.Children(':visible').length == 0){
  //     sessionStorage.setItem("newCompany",companyName);
  //     if(!$(".no-result-found div").hasClass("no-entries")){
  //       tableBody.append('<div class = "no-entries">No entries found.</div>');
  //       $(".no-entries").css({"padding":"20px 0px", "text-align":"center"});
  //     }
  //     else{
  //       $(".no-entries").remove();
  //     }
  //   }
  // }
  getCompanyById(projectId){
    this.projectSpec.projectId = projectId;
    let payload = {
      projectId: projectId
    }
    this.restservice.specDivision(payload).subscribe(success=>{
      this.associatedSpecDivision = success['payload'];
    },(err)=>{
      console.log('err' ,err);
    });
  }
  getAllSpecDivision(){
    this.restservice.getMasterData().subscribe(success=>{
      console.log('success',success['payload']['Spec Division']);
      success['payload']['spec_division'].forEach(element => {
        let emptyObj = {}
        emptyObj['name'] = element;
        this.availableSpecDivision.push(emptyObj);
      });      
    },(err)=>{
      console.log('err',err);
    });
  }
  addSpecDivision(item){
    this.associatedSpecDivision.push(item.name);
    this.projectSpec['specDivision'] = this.associatedSpecDivision;
    sessionStorage.setItem('projectSpec',JSON.stringify(this.projectSpec));
    this.checkAddedSpecDivision();
  }

  removeSpecDivision(index){
    let removedSpecDivision = this.associatedSpecDivision[index];
    this.associatedSpecDivision.splice(index,1);
    this.projectSpec['specDivision'] = this.associatedSpecDivision;
    sessionStorage.setItem('projectSpec',JSON.stringify(this.projectSpec));
    this.checkAddedSpecDivision(removedSpecDivision);
  }
  
  clearCompany(){
    this.specDivision = '';
   // this.searchCompany(this.specDivision);
  }
  checkAddedSpecDivision(removedSpecDivision?){
    if(removedSpecDivision){
      this.availableSpecDivision.forEach(item=> {
        if(item.name == removedSpecDivision){
          item['isAvailable'] = false;
        }
      })
    }else{
      this.associatedSpecDivision.forEach(item=>{
        this.availableSpecDivision.forEach(availableItem=>{
          if(item == availableItem.name){
            availableItem['isAvailable'] = true;
          }
        });
      });
    }
    this.companyChecked = true;
  }
  
}
