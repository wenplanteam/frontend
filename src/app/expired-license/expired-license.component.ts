import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../emitter.service';
@Component({
  selector: 'app-expired-license',
  templateUrl: './expired-license.component.html',
  styleUrls: ['./expired-license.component.scss']
})
export class ExpiredLicenseComponent implements OnInit {

  constructor(public router: Router,public emitter: EmitterService) { }

  ngOnInit() {
    let license = JSON.parse(sessionStorage.getItem('loginDetails'));
    if(license.trial || license.licenseAvailable) {
      this.router.navigate(['/authenticate/list/project']);
    }
    else {
      this.emitter.getMessage('Your license has expired. Please purchase a license.');
    }
  }

}
