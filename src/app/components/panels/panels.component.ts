import { Component, OnInit, Input } from '@angular/core';
import { FormGroup,FormBuilder,FormControl,Validator }  from '@angular/forms';
import {FormValidationService} from '../../form-validation.service';

declare const $:any;
@Component({
  selector: 'app-panels',
  templateUrl: './panels.component.html',
  styleUrls: ['./panels.component.scss']
})
export class PanelsComponent implements OnInit {

  @Input() panelData: any;
  form: FormGroup;
  public formHandler:any;
  constructor(public formValidation: FormValidationService,public fb:FormBuilder) {
      this.formHandler =fb;
   }

  ngOnInit() {
    console.log("panelData in panel", this.panelData);
    this.form=this.formHandler.group(this.formValidation.toFormGroup(this.panelData.panels));
  }

}
