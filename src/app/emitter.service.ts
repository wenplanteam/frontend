import { Injectable, EventEmitter } from '@angular/core';
import { BehaviorSubject, Subject, Observable } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, AfterViewInit, ViewChild, ElementRef, Input, Output } from '@angular/core';

declare let $: any;


@Injectable({
  providedIn: 'root'
})



export class EmitterService {

  public panelData: any;
  public panels: any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();


  private messageSource = new BehaviorSubject('');
  private userRole = new BehaviorSubject('');
  private operation = new BehaviorSubject('');
  private getProjectDetails = new BehaviorSubject('');
  private routerUrl = new BehaviorSubject('');
  private message = new BehaviorSubject('');
  private dataCaptcha = new BehaviorSubject('');
  private dataLoginDetails = new BehaviorSubject('');
  private activatedRoute = new BehaviorSubject('');
  private deleteData = new BehaviorSubject('');
  private basicSearch = new BehaviorSubject('');
  private pagination = new BehaviorSubject('');
  public selectedGcCompany = new Subject();
  private sharedPanel = new BehaviorSubject('');
  private loginData = new BehaviorSubject('');
  private responseData = new BehaviorSubject('');
  private responseSendGcCompanyFromList = new Subject();
  private sharedUserData = new BehaviorSubject('');
  private projectNameData = new BehaviorSubject('');
  private gcCompanyHeaderId = new BehaviorSubject('');
  private unSavedData =  new BehaviorSubject('');


  currentMessage = this.messageSource.asObservable();
  userRoleDetails = this.userRole.asObservable();
  currentOperation = this.operation.asObservable();
  projectDetails = this.getProjectDetails.asObservable();
  routerUrlSource = this.routerUrl.asObservable();
  responseMessageSource = this.message.asObservable();
  captchaEmittData = this.dataCaptcha.asObservable();
  updateLogingDetails = this.dataLoginDetails.asObservable();
  deleteDataSource = this.deleteData.asObservable();
  responseActivatedRoute = this.activatedRoute.asObservable();
  basicSearchData = this.basicSearch.asObservable();
  paginationData = this.pagination.asObservable();
  selectedHeaderCompany = this.gcCompanyHeaderId.asObservable();
  selectedGcData: Observable<any> = this.selectedGcCompany.asObservable();
  sharedToPanel = this.sharedPanel.asObservable();
  loginDetailData = this.loginData.asObservable();
  getResponseData = this.responseData.asObservable();
  getSendGcCompanyFromList = this.responseSendGcCompanyFromList.asObservable();
  userData = this.sharedUserData.asObservable();
  projectName = this.projectNameData.asObservable();
  unSaveCall =  this.unSavedData.asObservable();

  share: EventEmitter<any> = new EventEmitter();

  constructor(public router: Router, public route: ActivatedRoute) { }

  unSaveDataTrigger(data){
    this.unSavedData.next(data);
  }
  changeMessage(message: any) {
    this.messageSource.next(message);
  }
  roleDetail(role: any) {
    this.userRole.next(role);
  }
  sendOperation(message: any) {
    this.operation.next(message);
  }
  getRouterUrl() {
    this.routerUrl.next(this.router.url);
  }

  getCreatedProject(message) {
    this.getProjectDetails.next(message);
  }

  getMessage(data: any) {
    this.message.next(data);
    if (data == "A mail has been sent to your business email id.") {
      this.router.navigate(['/landing']);
    }
    else {
      $(document).ready(function () {
        $(".msgBtn").click(function () {
          if (document.getElementById("businessEmailId")) {
            var eid = document.getElementById("businessEmailId").focus();
            (<HTMLInputElement>document.getElementById("businessEmailId")).selectionStart = 0;
            (<HTMLInputElement>document.getElementById("businessEmailId")).selectionEnd = (<HTMLInputElement>document.getElementById("businessEmailId")).value.length + 1;
          }
        });
      });
    }
  }

  captcha(data: any) {
    this.dataCaptcha.next(data);
  }

  updateLogin(data: any) {
    this.dataLoginDetails.next(data);
  }


  getActivatedRoute() {
    const routeId = this.router.url.split('/');
    if (routeId.length === 5) {
      this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() => {
        this.router.navigate(['/authenticate/edit/project/' + routeId[4]]);
      });
    } else {
      this.activatedRoute.next(routeId[1]);
    }
  }
  getDataSource(deletedData: any) {
    this.deleteData.next(deletedData);
  }
  getBasicSearch(data: any) {
    this.basicSearch.next(data);
  }
  getPagination(data: any) {
    this.pagination.next(data);
  }
  shareToPeople(data: any) {
    this.share.emit(data);
  }

  gcCompanyItem(data) {
    this.selectedGcCompany.next(data);
  }
  gcCompanyId(data) {
    this.gcCompanyHeaderId.next(data);
  }
  shareToPeopleData() {
    return this.share;
  }
  sharePanelData(data: any) {
    this.sharedPanel.next(data);
  }
  getLoginDetails(data: any) {
    this.loginData.next(data);
  }
  getSharedUsers(data: any) {
    this.sharedUserData.next(data);
  }
  getProjectName(data: any) {
    this.projectNameData.next(data);
  }
  setResponseData(data: any) {
    this.responseData.next(data);
  }
  sendGcCompanyFromList(data) {
    this.responseSendGcCompanyFromList.next(data);
  }
}
