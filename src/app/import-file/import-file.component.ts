import { Component, OnInit, ViewChild } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { GanttServices } from "./../services/gantt.services";
import { NgxSpinnerService } from 'ngx-spinner';
import * as FileSaver from 'file-saver';
// declare var $:JQueryStatic;
// import $ = require('jquery');
import * as $AB from 'jquery';

@Component({
  selector: 'app-import-file',
  templateUrl: './import-file.component.html',
  styleUrls: ['./import-file.component.scss']
})

export class ImportFileComponent implements OnInit {

  public operation: any = {};
  @ViewChild('childModal') public childModal: ModalDirective;
  @ViewChild('successModal') public successModal: ModalDirective;
  public fileInput: File;
  public fileMessage: any = {};
  public status: any;
  public projectCompanyList: any;
  public currentCompanyId: any;
  public projectCompanyid: any;
  public payload: any;
  public projectDetails: any;
  public isDownload: any;
  public successmsg: any;
  public baseUrl: any;
  projectName: any;
  importHistory: any;
  listOfImports: any;
  panels: any;
  pageDatapopup: Object;
  form: any;
  formHandler: any;
  formValidation: any;
  routepage: string;
  modalTitle: string;
  clearMessage: string;
  confirmMessage: any;
  importHistoryData: any;

  constructor(private spinner: NgxSpinnerService, public restservice: RestserviceService, private router: Router, public httpClient: HttpClient, private ganttservices: GanttServices) {
    this.baseUrl = this.restservice.baseUrl
  }

  ngOnInit() {
    // this.onChangeFunction('e');
    
    
    this.restservice.importHistoryJson('create', 'importHistory').subscribe((result) => {
      this.importHistory = result;
      console.log(this.importHistory);
      this.listOfImports = result['panels'][0]['items'];
      console.log(this.listOfImports)
    });
    let projects = JSON.parse(sessionStorage.getItem('projectDetails'));
    this.projectName = projects['projectName'];
    console.log('projects', projects['projectName']);
    this.projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
    this.currentCompanyId = sessionStorage.getItem('currentCompanyId');
    this.ganttservices.lookaheadDateInfo(this.projectDetails['projectId']).subscribe(success => {
      this.projectCompanyid = success['payload'].projectCompanyId;
    })
    this.getProjectCompany();
    this.getImportJson();
    this.importHistoryList();
  }
  
  getImportJson() {
    this.restservice.getImportJson().subscribe((data: any) => {
      this.isDownload = data;
    })
  }


  getProjectCompany() {
    let projectId = { 'projectId': this.projectDetails.projectId };
    this.restservice.projectCompany(projectId).subscribe((success) => {
      this.projectCompanyList = success['payload']['projectCompanies'];
    }, err => {
      console.log(err);
    });
  }


  importData(file) {
    this.fileInput = <File>file.target.files[0];
  }

  onSubmit() {
    this.spinner.show();
    this.restservice.importExcel(this.projectCompanyid, this.projectDetails['projectId'], this.fileInput).subscribe((response: any) => {
      this.successmsg = response.headers.get("Message");
      this.confirmMessage = response.headers.get('Message');
      console.log("successmsg", this.successmsg);
      this.spinner.hide();
      this.status = response.headers.get("Code");
      this.fileMessage = response;
      console.log('sdfsdfdsf', this.fileMessage);
      if (response !== null) {
        $('#createModel').modal('hide');
        this.spinner.show();
        this.childModal.show();
        this.spinner.hide();
      }

    }, (error) => {
      this.spinner.hide();
      this.childModal.show();
    });
  }

  successNavigate(message) {
    if (message == "200") {
      this.childModal.hide();
      this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() =>
        this.router.navigate(["/authenticate/edit/project/import-data"])
      );
    }
    else {
      this.childModal.hide();
      this.successModal.show();
    }
  }

  fileDownload(file) {
    const data: Blob = new Blob([file.body], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' });
    let fileName = 'errorExcelfile.xls';
    FileSaver.saveAs(data, fileName);
    this.successModal.hide();
    this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() =>
      this.router.navigate(["/authenticate/edit/project/import-data"])
    );
  }


  close() {
    this.childModal.hide();
    this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() =>
      this.router.navigate(["/authenticate/edit/project/import-data"])
    );
    this.successModal.hide();
  }

  goRoute() {
    this.router.navigate(['/authenticate/edit/project/details']);
  }

  addNewSchedule(updateSchdule) {
    const ss = this.router.url.split('/');
    this.routepage = ss[ss.length - 1];
    if (this.routepage === 'import-data') {
      this.routepage = 'importHistory';
    }
    console.log('sssss', this.routepage);
    this.restservice.getRenderingJson('create', this.routepage).subscribe(success => {
      this.operation = success['tools'];
      this.panels = success['panels'];
      this.pageDatapopup = success;
      if (updateSchdule == 'addNewSchedule') {
        this.modalTitle = 'Upload New Schedule';
        // this.isDownload.updateMessage
        $('#createModel').modal('show');
      }
    }, (err) => {
      console.log('err', err);
    });
  }
  /** update exist import history */
  updateSchdule(updateSchdule) {
    const ss = this.router.url.split('/');
    this.routepage = ss[ss.length - 1];
    if (this.routepage === 'import-data') {
      this.routepage = 'importHistory';
    }
    console.log('sssss', this.routepage);
    this.restservice.getRenderingJson('create', this.routepage).subscribe(success => {
      this.operation = success['tools'];
      this.panels = success['panels'];
      this.pageDatapopup = success;
      if (updateSchdule == 'updateSchdule') {
        this.modalTitle = 'Update Schedule';
        $('#createModel').modal('show');
      }
    }, (err) => {
      console.log('err', err);
    });
  }

  /** clear import history */
  clearSchedule(clearSchedule) {
    const ss = this.router.url.split('/');
    this.routepage = ss[ss.length - 1];
    if (this.routepage === 'import-data') {
      this.routepage = 'importHistory';
    }
    console.log('sssss', this.routepage);
    this.restservice.getRenderingJson('create', this.routepage).subscribe(success => {
      this.operation = success['tools'];
      this.panels = success['panels'];
      this.pageDatapopup = success;
      if (clearSchedule == 'clearSchedule') {
        this.modalTitle = 'Message';
        this.clearMessage = 'Are you sure you want to clear all schedules';
        $('#successModal').modal('show');
      }
    }, (err) => {
      console.log('err', err);
    });
  }
  DisableHistoryBtn() {
    
  }
  importHistoryList() {
    let projectDetails= JSON.parse(sessionStorage.getItem('projectDetails'));
    this.payload = {
      'projectId': projectDetails.projectId
    }
    this.restservice.getResponseimportHistory(this.payload).subscribe((result) => {
      this.importHistoryData = result['payload'];
      console.log(this.importHistoryData);
    })
  }
}
