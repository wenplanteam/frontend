import { Component, OnInit, Input } from '@angular/core';
import { FormControl,FormBuilder, FormGroup, Validators } from "@angular/forms";

declare const $:any;
declare let gantt:any;
@Component({
  selector: 'app-datepicker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers:[FormBuilder]
})
export class DatepickerComponent implements OnInit {
  //public Datemodel: any = { date: { year: 2018, month: 8, day: 1 } };
  @Input() datePickerData: any;
  @Input() form: FormGroup;

  formHandler: any;

  constructor(public fb: FormBuilder) {
    this.formHandler = fb;
  }

  ngOnInit() {
    this.datepicker();  
  }

  datepicker() {
    $(document).ready(()=>{
    $('#'+this.datePickerData.name).datepicker({
      dateFormat: 'dd-M-yy',
   //   maxDate:'now',
      changeMonth: true,
      changeYear: true,
      onSelect:(date)=>{
        if(date){
          this.datePickerData.value=date;
          $('#'+this.datePickerData.name).val(date);
        }
     }
 });
});
}
dateFormat(date){
    let MonthArr =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Spe','Oct','Nov','Dec'];
    let dateStr =new Date(date);
    return (dateStr.getDate()<10?"0"+dateStr.getDate():dateStr.getDate())+"-"+MonthArr[dateStr.getMonth()]+"-"+dateStr.getFullYear();
}

}
