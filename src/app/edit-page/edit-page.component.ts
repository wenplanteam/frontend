import { Component, OnInit, Input } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';

@Component({
  selector: 'app-edit-page',
  templateUrl: './edit-page.component.html',
  styleUrls: ['./edit-page.component.scss'],
  providers: [FormBuilder]
})

export class EditPageComponent implements OnInit {
  form: FormGroup;
  @Input() panels: any;
  public formHandler: any;
  public page: any;
  public routepage: any;
  public responseData: any;
  public updateCustom: any;
  public id: any;
  public createdUserId: any;
  public operation: any;
  public panelData: any;

  constructor(public restservice: RestserviceService, private router: Router,
    private route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    this.id = JSON.parse(sessionStorage.getItem('listId'));
    this.emitter.getRouterUrl();
    let routepage = this.router.url.split('/');
    console.log('THissssss.', this.router.url);
    if (routepage.length === 6) {
      this.routepage = routepage[routepage.length - 2];
    } else {
      this.routepage = routepage[routepage.length - 1];
    }
    if (this.router.url.match('edit')) {
      this.page = 'edit';
    }
    this.getViewJsonDataFromServer(this.page, this.routepage);
    this.emitter.selectedHeaderCompany.subscribe(item => {
      this.createdUserId = item;
    });
  }

  /*Remove visible false from rendering json*/
  removeByAttr(arr, attr, value) {
    var i = arr.length;
    while (i--) {
      if (arr[i]
        && arr[i].hasOwnProperty(attr)
        && (arguments.length > 2 && arr[i][attr] === value)) {
        arr.splice(i, 1);
      }
    }
    return arr;
  }

  getResponseJsonData() {
    const params = {};
    params['created'] = this.createdUserId;
    this.route.params.subscribe(routeParams => {
      if (routeParams.id) {
        params['id'] = routeParams.id;
      } else {
        if ((this.routepage != 'details' && this.routepage != 'configuration') && this.id) {
          params[this.id.field] = this.id.value;
        } else {
          const x = JSON.parse(sessionStorage.getItem('projectCreate'));
          const project = x ? x : JSON.parse(sessionStorage.getItem('projectDetails'));
          this.emitter.getProjectName(project.projectName);
          params['projectId'] = project.projectId;
        }
      }
    });

    this.restservice.getResponseData(this.panelData.fetchRestServiceUrl, params, {}).subscribe(success => {
      this.responseData = success['payload'];
      if (this.routepage == 'user-permission' || this.routepage == 'project-user') {
        this.responseData['name'] = this.responseData.firstName + ' ' + this.responseData.lastName;
      }
      if (this.routepage == 'details') {
        this.emitter.getProjectName(this.responseData.projectName);
  // console.log("edit", this.responseData)

        sessionStorage.setItem('projectDetails', JSON.stringify(this.responseData));
      } else {
        this.emitter.getProjectName(this.responseData.name);
      }
      this.panels.forEach(item => {
        item['items'].forEach(panelItems => {
          if ((panelItems.bindingDbFieldName == 'projectStatus' && !this.responseData.inactivate) || (this.routepage == 'user-permission' && !this.responseData.edit && panelItems.bindingDbFieldName == 'companyName')) {
            panelItems.readOnly = true;
          }
          for (let obj in this.responseData) {
            if((this.routepage == 'plan' && panelItems.bindingDbFieldName == 'monthly' && obj == 'amount') || (this.routepage == 'plan' && panelItems.bindingDbFieldName == 'annually' && obj == 'amount')){
                panelItems['value'] = this.responseData[obj][panelItems.bindingDbFieldName];
            }else{
              if (panelItems.bindingDbFieldName == obj) {
                panelItems['value'] = this.responseData[obj];
              }
            }
          }
        });
      });
    }, (err) => {
      console.log('err', err);
    });
  }

  /*get json data from server*/
  getViewJsonDataFromServer(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      const buttonData = success;
      this.operation = buttonData;
      this.panelData = buttonData;
      const x = this.router.url.split('/');
      if (x.length === 5) {
        const projectRole = JSON.parse(sessionStorage.getItem('projectDetails')).projectRole;
        if (projectRole != 'GC-Project Admin' && projectRole != 'GC-Project Manager') {
          success['tools']['ComponentItem'] = [];
        }
      }
      this.panelData.panels.forEach(item => {
        this.removeByAttr(item.items, 'visible', '');
      });
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
      this.getResponseJsonData();
    }, (err) => {
      console.log('err', err);
    });
  }
}