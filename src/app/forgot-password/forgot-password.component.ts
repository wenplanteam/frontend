import { Component, OnInit, Output, EventEmitter} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {RestserviceService} from '../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../emitter.service';
import { FormValidationService } from '../form-validation.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss'],
  providers: [FormBuilder]
})
export class ForgotPasswordComponent implements OnInit {
  form: FormGroup;
  public formHandler: any;
  public operation: any;
  public panelData: any;
  public panels: any;
  public validation: any;
  public routepage: any;
  public page: any;
  public email: any;
  public message: any;

  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router,
    private route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
    this.route.queryParams.subscribe(params => {
      this.email = params['email'];
      this.message = params['msg'];
    });
    if (this.message) {
      this.emitter.getMessage(this.message);
    }
    this.page = 'create';

    if (this.email) {
      this.getJsonDataForForgotPassword(this.page, 'resetPassword');
    } else {
      this.getJsonDataForForgotPassword(this.page, 'forgotPassword');
    }
    setTimeout(() => {
      const emailId = this.panels[0].items.filter(item => {
        return item.bindingDbFieldName === 'email';
      });
      emailId[0].value = this.email;
    }, 500);
  }

  getJsonDataForForgotPassword(operation, pageName) {
    this.restservice.getRenderingJson(operation, pageName).subscribe(success => {
      const buttonData = success;
      this.operation = buttonData;
      this.panelData = buttonData;
      this.panels = this.panelData['panels'];
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }

}
