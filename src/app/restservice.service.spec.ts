import { TestBed, inject } from '@angular/core/testing';

import { KnowledgeHubService } from './knowledge-hub.service';

describe('KnowledgeHubService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [KnowledgeHubService]
    });
  });

  it('should be created', inject([KnowledgeHubService], (service: KnowledgeHubService) => {
    expect(service).toBeTruthy();
  }));
});
