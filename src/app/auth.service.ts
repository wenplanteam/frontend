import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { Observable } from 'rxjs/Rx'

import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
//import { Observable } from 'rxjs/Observable';
import { UUID } from 'angular2-uuid';
import { Router } from '@angular/router';
@Injectable({
  providedIn: 'root'
})

export class AuthService implements HttpInterceptor {
  constructor(private router:Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let uuid = UUID.UUID();
    //console.log("request",request);
    let token="";
    if(sessionStorage.getItem('loginDetails')!=null && sessionStorage.getItem('loginDetails')!=undefined){
       token =JSON.parse(sessionStorage.getItem('loginDetails')).token
    }
    request = request.clone({
      setHeaders: {
        'request-id': uuid,
        'Authorization':'Bearer '+token
      }
     // withCredentials: true
    });
    return next.handle(request)
    .map((event: HttpEvent<any>) => {
        if (event instanceof HttpResponse && ~~(event.status / 100) > 3) {
          //console.info('HttpResponse::event =', event, ';');
        } else{ 
          //console.info('event =', event, ';');
        }
        return event;
      })
      .catch((err: any, caught) => {
        if (err instanceof HttpErrorResponse) {
          if(err.status ===401){
            sessionStorage.clear();
            this.router.navigate(['/login']);
          }
          /*if (err.status === 403) {
            this.router.navigate(['/forbidden']);
          }*/
          return Observable.throw(err);
        }
      });
  }
}