import { Component, OnInit, Input ,HostListener} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../../emitter.service';
import {FormValidationService} from '../../form-validation.service';
import { NgxSpinnerService } from 'ngx-spinner';
declare let $: any;
@Component({
  selector: 'app-grid-list',
  templateUrl: './grid-list.component.html',
  styleUrls: ['./grid-list.component.scss']
})
export class GridListComponent implements OnInit {

  @Input() pageData: any;
  @Input() gridListPanelData: any;
  @Input() gridViewPanelData: any;
  @Input() buttonData: any;
  @Input() header: any;
  @Input() detailsData: any;
  @Input() count: any;
  public dynamicPageName : any;
  
  formHandler: any;
  public noResult: any;
  public listData = [];
  public deleteData: string [] = [];
  public getResponseData: any;
  public routingId: any;
  public page: any;
  public fetchRestServiceUrl: any;
  public sharedUsers: any;
  public panels: any;
  public buttons: any;
  form: FormGroup;
  public licenseCount: any;
  public revokeItem: any;
  public loginDetails: any;
  createdId: string;
  scrHeight: number;
  scrWidth: number;
  public dropdownMobile : boolean =  false;
  spinner: any;
  
  @HostListener('window:load, resize', ['$event'])
  getScreenSize(event?) {
    this.scrHeight = window.innerHeight;
    this.scrWidth = window.innerWidth;
    // console.log(this.scrHeight, this.scrWidth, "->>->>>>>>>");

    if(this.scrWidth < 578){
      this.dropdownMobile = true;
    }else{
      this.dropdownMobile = false;
    }
  }
  constructor(public restservice: RestserviceService, public router: Router,
     public route: ActivatedRoute, public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;

    this.getScreenSize();
  }

  ngOnInit() {
    console.log("sdfdsfsdfdsf",this.router.url);
    this.loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    this.emitter.getLoginDetails(JSON.parse(sessionStorage.getItem('loginDetails')));
    sessionStorage.removeItem('projectCreate');
    this.noResult = true;
    this.emitter.routerUrlSource.subscribe(item => {
      this.emitter.getDataSource(this.deleteData);
    });
    if (this.router.url.match('list')) {
      this.page = 'list';
    }
    let id;
    this.emitter.getRouterUrl();
    this.route.params.subscribe(params => {
      this.deleteData = [];
      id = params['id'];
      this.routingId = id;
    });

    $(document).ready(() => {
      $('.selectBtn').click(function() {
        $('.individual').prop('checked', $(this).prop('checked'));
        });
    });

    $(document).ready(function() {
      $('#select-all').click(function() {
      $('.individual').prop('checked', $(this).prop('checked'));
      if ($('.individual').prop('checked')) {
      $('.checkselect').hide();
      $('.unselect').show();
      } else {
      $('.checkselect').show();
      $('.unselect').hide();
      }
      });
    });
  }

  getLicenseCount() {
    this.restservice.getLicenseCount().subscribe(success => {
      this.licenseCount = success['payload'];
    }, (err) => {
      console.log('err', err);
    });
  }

  addToggleClass(){
    $("#editbtn").addClass("dropdown-menu");
  }
  /* Go View Page */
  goView(item) {
    let id = {};
    id['field'] = this.gridListPanelData['checkbox'];
    id['value'] = item[this.gridListPanelData['checkbox']];
    sessionStorage.setItem('listId', JSON.stringify(id));

    console.log("this.routingId", '/authenticate/view/' + this.routingId)
    this.router.navigate(['/authenticate/view/' + this.routingId]);
  }

  goEdit(item) {
    this.dynamicPageName = "Details"
   sessionStorage.setItem("dynamicPageName", this.dynamicPageName)
    const id = {};
    id['field'] = this.gridListPanelData['checkbox'];
    id['value'] = item[this.gridListPanelData['checkbox']];
    sessionStorage.setItem('listId', JSON.stringify(id));
    sessionStorage.setItem('projectDetails', JSON.stringify(item));
    if (this.routingId == 'project') {
      this.router.navigate(['/authenticate/edit/project/details']);
    } else {
      this.router.navigate(['/authenticate/edit/' + this.routingId]);
    }
  }

  goPlan(item) {
    
    sessionStorage.setItem('projectDetails', JSON.stringify(item));
    this.router.navigate(['/authenticate/lookaheadplan']);
  }

  goShare(item) {
    this.dynamicPageName = "Users & Permissions"
    sessionStorage.setItem("dynamicPageName", this.dynamicPageName)
    sessionStorage.setItem('projectDetails', JSON.stringify(item));
    const id = {};
    id['field'] = this.gridListPanelData['checkbox'];
    id['value'] = item[this.gridListPanelData['checkbox']];
    sessionStorage.setItem('listId', JSON.stringify(id));
    
    this.router.navigate(['/authenticate/edit/project/user']);
  }

  getItBack(item) {
    if (item.givenTo == 'Self') {
      item.givenTo = JSON.parse(sessionStorage.getItem('loginDetails')).email;
    }
    this.revokeItem = item;
    $('#getItBack').modal('show');
  }

  confirmGetItBack(){
    const request = {
      'id': this.revokeItem.licenseId,
      'licenseNo': this.revokeItem.licenseNo,
      'email': this.revokeItem.givenTo
    };
    this.restservice.getItback(request).subscribe(success => {
      if (success) {
        this.emitter.getActivatedRoute();
        this.emitter.getMessage(success['message']);
        this.emitter.changeMessage({});
        this.emitter.getMessage('');
        this.emitter.getDataSource({});
      }
    }, (err) => {
      console.log('err', err);
    });
  }


  getContactData(value, operation) {
    if (operation == 'selectAll') {
      this.gridListPanelData.responseObj.forEach(element => {
      
        if (element.dalete) {
          this.deleteData.push(element.projectId);
        }
      });
    } else if (operation == 'unSelectAll') {
      this.deleteData = [];
    } else {
      const index = this.deleteData.indexOf(value);
      if (index == -1) {
        this.deleteData.push(value);
      } else {
        this.deleteData.splice(index, 1);
      }
    }
    this.emitter.getDataSource(this.deleteData);
   }

   getPaginationData(event) {
         console.log("event before", event);
     if (sessionStorage.getItem('popupGcName') == null || sessionStorage.getItem('popupGcName') == undefined) {  
      this.emitter.setResponseData(event);
     }
     else {
      this.createdId = sessionStorage.getItem('popupGcName'); 
      event.created = this.createdId;
      this.emitter.setResponseData(event);
      console.log("event after", event);
     }
    
  }

  modelPopup() {
    $('#targetModal').modal('show');
  }


  openProvide(data) {
    //alert(this.pageData);
    this.restservice.getRenderingJson('create', 'giveto').subscribe(success => {
      this.pageData = success;
      console.log("this.pageData",this.pageData);
      this.panels = success['panels'];
      this.panels.forEach(panel => {
        panel.items.forEach(panelItem => {
          if (panelItem.bindingDbFieldName == 'licenseNo') {
            panelItem.value = data[panelItem.bindingDbFieldName];
          }
        });
      });
      this.buttons = success['ComponentItem'];
      if (this.panels) {
        $('#giveToModal').modal('show');
      }
      this.form = this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    }, (err) => {
      console.log('err', err);
    });
  }
  

  
  gopage(item){
    sessionStorage.setItem('projectDetails', JSON.stringify(item));
    this.router.navigate(['/authenticate/effortCompletion']);
  }

}
