import { Component, ElementRef, OnInit, ViewChild, ComponentFactoryResolver, Input, OnDestroy, HostListener } from "@angular/core";
import { GanttServices } from "../../services/gantt.services"
import { GanttData } from "../../model/gantt.model";
import { DatePipe, WeekDay } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { FieldConfig } from "../../field.interface";
import { IMyDpOptions, IMyDateModel } from 'mydatepicker';
import { RestserviceService } from '../../restservice.service';
import { EmitterService } from '../../emitter.service';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { FormsModule } from '@angular/forms';
import { Item } from "angular2-multiselect-dropdown";
import * as _ from 'lodash';
import { isAbsolute } from "path";
import { DomSanitizer } from '@angular/platform-browser';
import * as FileSaver from 'file-saver';
import { AscDescPipe } from '../../pipes/asc-desc/asc-desc.pipe'
import { b } from "@angular/core/src/render3";
import { empty } from "rxjs";
import { environment } from '../../../environments/environment';
import { NgSelectModule } from "@ng-select/ng-select";
import { flattenStyles } from "@angular/platform-browser/src/dom/dom_renderer";
import { ModalDirective } from 'ngx-bootstrap/modal';
import swal from 'sweetalert';

//import { INTERNAL_BROWSER_PLATFORM_PROVIDERS } from "@angular/platform-browser";
declare let $: any;
declare let gantt: any

@Component({
  selector: "gantt",
  styles: ['./ganttchart.component.css'],
  templateUrl: './ganttchart.component.html',
})
export class GanttChartComponent implements OnInit, OnDestroy {
  public options = [
    "Task No A-Z", "Task No Z-A", "Responsibility A-Z", "Responsibility Z-A", "Task Start Date A-Z", "Task Start Date Z-A"
  ]

  // @ViewChild('taskOverrideMsg') public taskOverrideMsg : ModalDirective;
  @ViewChild('childModal') public childModal: ModalDirective;
  query: any = ""
  selectedValue: any = ""
  searchText: any;
  sorts: boolean = false;
  sortType: any;
  columname: any;
  searchedArray = [];
  checksearch: Boolean = false;
  taskids = [];
  subtaskids = [];
  public searcharray = []
  documents: Array<any> = [];
  selectedDocuments: Array<any> = [];
  public ascen = 0;
  public descen = 1;
  selectOnDrag = true;
  selectMode = false;
  public getAllTask: any;
  disable = false;
  isDesktop = false;
  selectWithShortcut = false;
  public currentDate: any;
  pStartDate: any;
  field: FieldConfig;
  public backups = [];
  public selOptVal;
  // public ball = [];
  public baseUrl = environment.baseUrl;
  public taskList: any;
  public ganttChart = gantt;
  public projectId: any;
  public projectDetails: any;
  public requestPayload: any;
  public payload: boolean = true;
  public emailUsers: any;
  public emailArray: any = [];
  public lookaheadDate: any;
  public lookaheadSend: boolean = false;
  public loginDetails: any;
  public projectCompany: any;
  public viewOnlyAccess: boolean = false;
  public click_pagination: boolean = false;
  public click_datechange: boolean = false;
  public projectCompanyList: any;
  public getTrade: any;
  public selectedLookaheadDate: any;
  public filterItem: any = {
    "taskStatus": [],
    "accessibility": [],
    "ballInCourt": [],
    "task": [],
    "trade": [],
    "specDivision": [],
    "scheduleAvailability": "All"
  };
  public pageOfItems: Array<any>;
  public ganttMgs: any;
  public masterData: any;
  public projectRole: any;
  public all: any = 'all';
  public projectAssociatedCompany: any;
  public week = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
  public isResponsibility = [];
  public scheduled: any;
  public unscheduled: any;
  currentvalue: any;
  menu: boolean = true
  filterid: any;
  clickedValue: any;
  filterArray = [];
  checkedValue = [];
  xmark: boolean = false;
  emitterData: any;
  splited_data: any;
  getAllTasks: any;
  paginationData: any;
  pageNo: any;
  pageSize: any;
  newTask: boolean = false;
  isPopup: boolean = false;
  popupid: any;
  constructor(private ganttService: GanttServices, public emitter: EmitterService, private sanitizer: DomSanitizer,
    public router: Router, public restservice: RestserviceService, private datePipe: DatePipe, private http: HttpClient, public route: ActivatedRoute,
    public eRef: ElementRef,
    public ascDesc: AscDescPipe, private spinner: NgxSpinnerService
  ) { };
  _gData: GanttData[];

  public Datemodel: any = { date: { year: 2018, month: 9, day: 1 } };
  @Input() ganttchartprint: any;
  //formatting gantt data to db 
  // @HostListener('document:click', ['$event'])
  // onClickEvent(event: MouseEvent) {
  //   var target = event.target ||event.srcElement;
  //   var id = target['id']
  //   this.clickedValue = id;
  //    if($("#filter").hasClass('open') && (this.clickedValue !== "filter" )|| (this.clickedValue !== "filters" && this.clickedValue !== "ballInCourt" && this.clickedValue !== "task" && this.clickedValue !== "trade" && this.clickedValue !== "schedule" && this.clickedValue !== "accessibility")){
  //      this.menu = false

  //     //  $(".addclose").addClass("hide-class");
  //     //  $(".addopen").addClass("show-class");
  //     $('#filterChild').hide()
  //   }else{
  //     this.menu = true
  //    $('#filter') .show()
  //     $('#filterChild').show()
  //   }
  // }
  // ngOnInit() {
  // }

  /* Routing Function for project Edit */
  goRoute(url) {

    if (url == 'projectname') {
      if (sessionStorage.getItem('unsaved') != null || sessionStorage.getItem('unsaved') != undefined) {
        sessionStorage.setItem('status', 'projectname');
        // $("#unsavedDatapopup").modal('show');
        this.emitter.getMessage('You have unsaved data on this page. How do you want to proceed?');
      } else {
        let projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
        let id = {};
        id['field'] = "projectId";
        id['value'] = projectDetails.projectId;
        //   item[this.buttonData['checkbox']];
        sessionStorage.setItem("listId", JSON.stringify(id));
        this.router.navigate(['/authenticate/edit/project/details']);
      }
      // [routerLink]="['/authenticate/edit/project/details']"
      // [routerLink]="['/authenticate/edit/project/details']"
    } else {
      if (sessionStorage.getItem('unsaved') != null || sessionStorage.getItem('unsaved') != undefined) {
        sessionStorage.setItem('status', 'project');
        // $("#unsavedDatapopup").modal('show');
        this.emitter.getMessage('You have unsaved data on this page. How do you want to proceed?')
      } else {
        this.router.navigate(['/authenticate/list/project']);
      }
    }
  }
  /* End Of Routing Function for project Edit  */

  /*Display the CompanyId */
  findProjectCompany(companyId) {
    if (companyId) {
      let company = this.projectAssociatedCompany.filter(item => item.projectCompanyId == parseInt(companyId));
      if (company.length > 0) {
        return company[0].name;
      } else {
        let dCompany = this.projectAssociatedCompany.filter(item => item.name == companyId);
        return dCompany[0].name;
      }
    } else {
      return '';
    }
  }

  /* Destroy function in Angular */
  ngOnDestroy() {
    $('body').removeAttr("style");
    // this.emitterData.unsubscribe();
  }
  /* End of Destroy function in Angular */

  /*Get Project CompanyId */
  findProjectCompanyId(company) {
    let projectCompany = this.projectAssociatedCompany.filter(item => item.name == company);
    if (projectCompany.length > 0) {
      // alert("if"+ projectCompany[0].projectCompanyId)
      return projectCompany[0].projectCompanyId;
    } else {
      let dProjectCompany = this.projectAssociatedCompany.filter(item => item.name == company.ballInCourt);
      return dProjectCompany[0].projectCompanyId;
      // let dProjectCompany = this.projectAssociatedCompany.filter(item => item.name == company);
      // // return dProjectCompany[0].projectCompanyId;
      // return dProjectCompany[0].projectCompanyId;
      // alert("else"+ dProjectCompany[0].projectCompanyId)
    }
  }


  /* Merge db Data & GanttData for Preparing the Request Json */
  processRequestPayload(data: any, type: boolean, status?) {

    const taskRes = this.makeTaskDescriptionMandatory("update");

    if (taskRes.length > 0) {
      this.emitter.getMessage("Please enter the task description to the following task(s) No: " + taskRes.join());
    } else {
      this.getTaskDescriptionNotes();
      let updatePayload = {
        "tasks": []
      }
      console.log("this.requestPAyload", this.requestPayload)
      updatePayload['tasks'] = this.requestPayload.tasks.filter(item => item.id);

      if (updatePayload && updatePayload.tasks.length > 0) {
        // alert("1")
        updatePayload['projectId'] = this.projectDetails.projectId;
        updatePayload['lookaheadDate'] = this.pStartDate;
        updatePayload['submit'] = type;
        let update = false;
        let updateResPayload;
        updatePayload.tasks.forEach(item => {
          updateResPayload = data.filter(mdata => {
            return mdata.taskId == item.id;
          });

          if (updateResPayload.length > 0) {
            // alert("2")
            item['taskFinishDate'] = updateResPayload[0]['taskFinishDate'] ? updateResPayload[0]['taskFinishDate'] : null;
            item['requiredCompletionDate'] = updateResPayload[0]['requiredCompletionDate'] ? updateResPayload[0]['requiredCompletionDate'] : null;
            item['taskStartDate'] = updateResPayload[0]['taskStartDate'] ? updateResPayload[0]['taskStartDate'] : null;
            item['deadLineDate'] = updateResPayload[0]['deadLineDate'] ? updateResPayload[0]['deadLineDate'] : null;
            item['ballInCourt'] = updateResPayload[0]['ballInCourt'] ? updateResPayload[0]['ballInCourt'] : '';
            item['trade'] = updateResPayload[0]['trade'] ? updateResPayload[0]['trade'] : '';
            item['specDivision'] = updateResPayload[0]['specDivision'] ? updateResPayload[0]['specDivision'] : '';
          }
          if (item.ballInCourt) {
            // alert("3")
            item['projectCompanyId'] = this.findProjectCompanyId(item.ballInCourt);
          }
          if (item.id && item.subTasks) {
            // alert("4")
            update = true;
            item.subTasks.forEach(subItems => {

              updateResPayload = data.filter(mdata => {

                return mdata.subTaskId == subItems.id;
              });

              if (updateResPayload.length > 0) {
                // alert("5")
                subItems['taskFinishDate'] = updateResPayload[0]['taskFinishDate'] ? updateResPayload[0]['taskFinishDate'] : null;
                subItems['requiredCompletionDate'] = updateResPayload[0]['requiredCompletionDate'] ? updateResPayload[0]['requiredCompletionDate'] : null;
                subItems['taskStartDate'] = updateResPayload[0]['taskStartDate'] ? updateResPayload[0]['taskStartDate'] : null;
                subItems['deadLineDate'] = updateResPayload[0]['deadLineDate'] ? updateResPayload[0]['deadLineDate'] : null;
                subItems['ballInCourt'] = updateResPayload[0]['ballInCourt'] ? updateResPayload[0]['ballInCourt'] : null;
                subItems['trade'] = updateResPayload[0]['trade'] ? updateResPayload[0]['trade'] : '';
                subItems['specDivision'] = updateResPayload[0]['specDivision'] ? updateResPayload[0]['specDivision'] : '';

              }
              if (subItems.id == this.popupid && this.isPopup) {
                subItems['popup'] = this.isPopup;
              } else {
                subItems['popup'] = false;
              }

              if (subItems.ballInCourt) {
                // alert("6")
                subItems['projectCompanyId'] = this.findProjectCompanyId(subItems.ballInCourt);
              }
              if (updateResPayload[0] && updateResPayload[0].timeline.length > 0) {
                // alert("7")
                if (subItems.schedules && subItems.schedules.length == 0) {
                  // alert("8")

                  if (updateResPayload && updateResPayload.length > 0) {
                    // alert("9")
                    if (updateResPayload[0]['taskStartDate'] != this.datePipe.transform(updateResPayload[0].timeline[0].columnDate)
                      || updateResPayload[0]['taskFinishDate'] != this.datePipe.transform(updateResPayload[0].timeline[updateResPayload.timeline.length - 1].columnDate)) {
                      // alert("10")

                      var start = updateResPayload[0]['taskStartDate'],
                        end = new Date(updateResPayload[0]['taskFinishDate']),
                        currentDate = new Date(start),
                        between_days = [],
                        date;

                      // 
                      let scheduledArr = [];
                      updateResPayload[0].timeline.forEach(data2 => {
                        // 
                        scheduledArr.push({ 'workingDate': this.datePipe.transform(new Date(data2.columnDate), 'dd-MMM-yyyy'), 'workerCount': data2.value == 'x' ? 0 : data2.value, 'id': data2.id });

                      });


                      while (currentDate <= end) {

                        date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));

                        // 
                        if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
                          // alert("11")
                          between_days.push({ "workingDate": date, 'workerCount': 0 });
                        }
                        currentDate.setDate(currentDate.getDate() + 1);
                        // 
                      }

                      let addSchedules = between_days.filter(data => {

                        return !scheduledArr.some(data1 => {
                          return Date.parse(data1.workingDate) == Date.parse(data.workingDate);
                        });
                      });
                      if (addSchedules.length > 0) {
                        // alert("1.2")
                        scheduledArr = scheduledArr.concat(addSchedules);
                      }

                      let deletedSchedules = scheduledArr.filter(data => {
                        return !between_days.some(data1 => {
                          return Date.parse(data1.workingDate) == Date.parse(data.workingDate);
                        });
                      });
                      if (deletedSchedules.length > 0) {
                        // alert("13")

                        deletedSchedules.forEach(data4 => {
                          data4.workerCount = "";
                          delete data4.workingDate;
                        })

                      }

                      // });
                      //subItems.schedules =[];

                      subItems.schedules = scheduledArr;
                    } else {
                      // alert("14")
                      subItems.schedules.forEach(schedulesItem => {
                        if (schedulesItem.workerCount == 'x' || schedulesItem.workerCount == 'X') {
                          schedulesItem.workerCount = 0;
                        }
                        if (schedulesItem.workingDate) {
                          schedulesItem.workingDate = this.datePipe.transform(schedulesItem.workingDate, 'dd-MMM-yyyy')
                        }
                      })
                    }
                  }
                } else {
                  // alert("15")

                  subItems.schedules.forEach(schedulesItem => {
                    if (schedulesItem.workerCount == 'x' || schedulesItem.workerCount == 'X') {
                      // alert("16")
                      schedulesItem.workerCount = 0;
                    }
                    if (schedulesItem.workingDate) {
                      // alert("17")
                      schedulesItem.workingDate = this.datePipe.transform(schedulesItem.workingDate, 'dd-MMM-yyyy')
                    }
                  })
                }

                // } else{
                //   

                // }
              } else {
                // alert("18")

                if (updateResPayload.length > 0) {
                  // alert("19")
                  var start = updateResPayload[0]['taskStartDate'],
                    end = new Date(updateResPayload[0]['taskFinishDate']),
                    currentDate = new Date(start),
                    between_days = [],
                    date;
                  // 
                  if (start !== null && end !== null) {
                    while (currentDate <= end) {

                      date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));

                      // 
                      if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
                        // alert("20")
                        between_days.push({ "workingDate": date, 'workerCount': "0" });
                      }
                      currentDate.setDate(currentDate.getDate() + 1);
                    }
                  }

                  //subItems.schedules =[];
                  subItems.schedules = between_days;
                }
              }


            })
          }
        });
        /* if Update true call a Gantt  PUT service */
        if (update && !this.newTask) {
          // alert("new task " + this.newTask)
          // alert("im called by update")

          console.log(updatePayload.tasks, "updatePayload")
          if (status == 'popUp') {
            updatePayload.tasks.forEach(data => {
              // console.log(data.subTasks,"sub task level")
              for (let i = 0; i < data.subTasks.length; i++) {
                console.log(data.subTasks[i].schedules)
                delete data.subTasks[i].schedules;
              }
            })
          }

          console.log("after deleteed schedues", updatePayload)
          // this.spinner.show();
          this.ganttService.updateTask(updatePayload).subscribe(success => {
            // this.spinner.hide();
            //$("#loadermodel").modal('hide');

            if (success["success"]) {
              // alert("22")
              if (!type) {
                // alert("23")
                $('#updateMsg').modal('show');

              }
              this.requestPayload = {
                tasks: []
              }
              this.payload = true;
            }
            this.refreshData();
          });
        }
      }
      this.requestPayload = {
        tasks: []
      };
      let requestpayload = {
        "tasks": [],
        "subTaskIds": []
      };
      requestpayload['projectId'] = this.projectDetails.projectId;
      if (!data) data = [];
      let tasks = [];
      let submitData = data.filter(item => {
        return item.taskId == ''
      });
      let parantTask = data.filter(item => {
        return item.parent == '0';
      });
      let finalData = [];
      parantTask.forEach(item => {
        item['subTask'] = [];
        data.forEach(dataItem => {
          if (item.id == dataItem.parent) {
            // alert("24")
            item["subTask"].push(dataItem);
          }
        });
        finalData.push(item);
        this.refreshData();
      });

      if (finalData.length > 0) {
        // alert("25")
        finalData.forEach(parent => {
          let parentObj = {
            "id": '',
            "description": '',
            "taskType": "",
            "trade": '',
            "accessTo": '',
            "taskStartDate": '',
            "taskFinishDate": '',
            "requiredCompletionDate": '',
            "deadLineDate": '',
            "child": false,
            "note": '',
            "sno": '',
            "subTasks": [],
            "specDivision": []
          }
          parentObj.id = parent.taskId ? parent.taskId : '';
          parentObj.description = parent.taskName ? parent.taskName : '';
          parentObj.taskType = parent.taskType ? parent.taskType : '';
          parentObj.taskStartDate = parent.taskStartDate ? parent.taskStartDate : null;
          parentObj.taskFinishDate = parent.taskFinishDate ? parent.taskFinishDate : null;
          parentObj.requiredCompletionDate = parent.requiredCompletionDate ? parent.requiredCompletionDate : null;
          parentObj.deadLineDate = parent.deadLineDate ? parent.deadLineDate : null;
          parentObj.trade = parent.trade ? parent.trade : '';
          parentObj.specDivision = parent.specDivision ? parent.specDivision : '';
          parentObj.accessTo = parent.accessTo ? parent.accessTo : '';


          if (parent.taskId) {
            // alert("26")
            if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
              // alert("27")
              parentObj['ballInCourt'] = parent.ballInCourt ? parent.ballInCourt : '';
              parentObj['projectCompanyId'] = parent.projectCompanyId ? parent.projectCompanyId : '';
            } else {
              // alert("28")
              //
              // parentObj['ballInCourt'] = JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
              // parentObj['projectCompanyId'] = this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
              parentObj['ballInCourt'] = this.projectAssociatedCompany[0].name;
              parentObj['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
            }
          } else {
            // alert("29")
            if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
              if (parent.subTask.length === 0) {
                // alert("30")
                let x = this.findProjectCompany(parent.ballInCourt);
                parentObj['ballInCourt'] = x ? x : JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                // parentObj['projectCompanyId'] = parent.ballInCourt ? this.findProjectCompanyId(parent.ballInCourt) : this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                //--This condition is introduced to handle when one GC invites another GC and he adds a task in lookahead--//
                parentObj['projectCompanyId'] = parent.ballInCourt ? this.findProjectCompanyId(parent.ballInCourt) : JSON.parse(sessionStorage.getItem('lookaheadInfo')).projectCompanyId;
              }
            } else {
              // alert("31")
              // 
              // JSON.parse(sessionStorage.getItem('loginDetails')).companyName
              parentObj['ballInCourt'] = this.projectAssociatedCompany[0].name;
              // this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName)
              parentObj['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
            }
          }
          parentObj.note = parent.note ? parent.note : '';
          parentObj.sno = parent.serialno ? parent.serialno : '';
          requestpayload.tasks.push(parentObj);
          var start = parent.taskStartDate,
            end = new Date(parent.expectedDate),
            currentDate = new Date(start),
            between_days = [],
            date;
          // 
          while (currentDate <= end) {
            // 
            date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));

            if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
              // alert("32")
              between_days.push({ "workingDate": date, 'workerCount': "0" });
            }
            currentDate.setDate(currentDate.getDate() + 1);
          }
          // 


          if (parent.subTask.length > 0) {
            // alert("33")
            parentObj.child = true;
            parent.subTask.forEach(child => {

              if (child.taskId == '') {
                // alert("34")
                parentObj['isSubTask'] = true;
              };
              if (parent.subTaskId && parent.timeline && parent.timeline.length == 0) {
                // alert("35")
                requestpayload["subTaskIds"].push(parent.subTaskId);
              }

              if (!child.subTaskId) {
                // alert("36")
                var start = child.taskStartDate,
                  end = new Date(child.taskFinishDate),
                  currentDate = new Date(start),
                  between_days = [],
                  date;
                while (currentDate <= end) {

                  date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));

                  if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
                    // alert("37")
                    between_days.push({ "workingDate": date, 'workerCount': "0" });
                  }
                  currentDate.setDate(currentDate.getDate() + 1);
                  // 
                }
                let subTaskObj = {
                  "id": '',
                  "description": '',
                  "taskType": "",
                  "trade": '',
                  "accessTo": '',
                  "ballInCourt": '',
                  "taskStartDate": '',
                  "taskFinishDate": '',
                  "requiredCompletionDate": '',
                  "deadLineDate": '',
                  "note": '',
                  "sno": '',
                  "schedules": [],
                  "specDivision": []

                };
                subTaskObj.id = child.taskId ? child.taskId : '';
                subTaskObj.description = child.taskName ? child.taskName : '';
                subTaskObj.taskType = child.taskType ? child.taskType : '';
                subTaskObj.trade = child.trade ? child.trade : '';
                subTaskObj.accessTo = child.accessTo ? child.accessTo : '';
                subTaskObj.taskStartDate = child.taskStartDate ? child.taskStartDate : null;
                subTaskObj.taskFinishDate = child.taskFinishDate ? child.taskFinishDate : null;
                subTaskObj.requiredCompletionDate = child.requiredCompletionDate ? child.requiredCompletionDate : null;
                subTaskObj.deadLineDate = child.deadLineDate ? child.deadLineDate : null;
                subTaskObj.specDivision = child.specDivision ? child.specDivision : '';


                subTaskObj.schedules = [];
                if (child.taskId) {
                  // alert("38")
                  if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
                    subTaskObj['ballInCourt'] = child.ballInCourt ? child.ballInCourt : '';
                    subTaskObj['projectCompanyId'] = child.projectCompanyId ? child.projectCompanyId : '';
                  } else {
                    // alert("39")
                    subTaskObj['ballInCourt'] = this.projectAssociatedCompany[0].name;
                    subTaskObj['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
                    // subTaskObj['ballInCourt'] = JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                    // subTaskObj['projectCompanyId'] = this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                  }
                } else {
                  // alert("40")
                  if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
                    const x = this.findProjectCompany(child.ballInCourt);
                    subTaskObj['ballInCourt'] = x ? x : JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                    subTaskObj['projectCompanyId'] = child.ballInCourt ? this.findProjectCompanyId(child.ballInCourt) : this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                  } else {
                    // alert("41")
                    JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                    subTaskObj['ballInCourt'] = this.projectAssociatedCompany[0].name;
                    // this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                    subTaskObj['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
                  }
                }
                subTaskObj.note = child.note ? child.note : '';
                subTaskObj.sno = child.serialno ? child.serialno : '';
                if (child.timeline) {
                  // alert("42")

                  child.timeline.forEach(taskTimeline => {
                    subTaskObj.schedules.push({ workingDate: this.datePipe.transform(taskTimeline.columnDate, 'dd-MMM-yyyy'), workerCount: taskTimeline.value == 'x' || taskTimeline.value == 'X' ? '0' : taskTimeline.value });
                  })
                } else {
                  // alert("43")

                  subTaskObj.schedules = between_days;
                }
                parentObj.subTasks.push(subTaskObj);
              }
            })
          } else {
            // alert("44")

            let mainTaskSchedules = {
              "description": '',
              "taskType": "",
              "trade": '',
              "accessTo": '',
              "ballInCourt": '',
              "taskStartDate": '',
              "taskFinishDate": '',
              "requiredCompletionDate": '',
              "deadLineDate": '',
              "note": '',
              "sno": '',
              "id": '',
              "schedules": [],
              "specDivision": []

            }
            mainTaskSchedules['id'] = parent.taskId ? parent.taskId : '';
            mainTaskSchedules['description'] = parent.taskName ? parent.taskName : '';
            mainTaskSchedules['taskType'] = parent.taskType ? parent.taskType : '';
            mainTaskSchedules['trade'] = parent.trade ? parent.trade : '';
            mainTaskSchedules['specDivision'] = parent.specDivision ? parent.specDivision : '';
            mainTaskSchedules['accessTo'] = parent.accessTo ? parent.accessTo : '';
            mainTaskSchedules['taskStartDate'] = parent.taskStartDate ? parent.taskStartDate : null;
            mainTaskSchedules['taskFinishDate'] = parent.taskFinishDate ? parent.taskFinishDate : null;
            mainTaskSchedules['requiredCompletionDate'] = parent.requiredCompletionDate ? parent.requiredCompletionDate : null;
            mainTaskSchedules['deadLineDate'] = parent.deadLineDate ? parent.deadLineDate : null;
            mainTaskSchedules['schedules'] = between_days;
            var start = parent.taskStartDate,
              end = new Date(parent.taskFinishDate),
              currentDate = new Date(start),
              between_days = [],
              date;

            if (start !== null && end !== null) {
              while (currentDate <= end) {

                date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));

                if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
                  // alert("45")
                  between_days.push({ "workingDate": date, 'workerCount': "0" });
                }
                currentDate.setDate(currentDate.getDate() + 1);
                // 
              }
            }

            if (parent.taskId) {
              // alert("46")
              if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
                // alert("47")
                mainTaskSchedules['ballInCourt'] = parent.ballInCourt ? parent.ballInCourt : '';
                mainTaskSchedules['projectCompanyId'] = parent.projectCompanyId ? parent.projectCompanyId : '';
              } else {
                // alert("48")
                mainTaskSchedules['ballInCourt'] = this.projectAssociatedCompany[0].name;
                mainTaskSchedules['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
                // mainTaskSchedules['ballInCourt'] = JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                // mainTaskSchedules['projectCompanyId'] = this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
              }
            } else {
              // alert("49")
              if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
                // alert("50")
                let x = this.findProjectCompany(parent.ballInCourt);
                mainTaskSchedules['ballInCourt'] = x ? x : JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                var projectCompanyIdFromStorage = JSON.parse(sessionStorage.getItem('lookaheadInfo')).projectCompanyId;
                // mainTaskSchedules['projectCompanyId'] = parent.ballInCourt ? this.findProjectCompanyId(parent.ballInCourt) : this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                //---This condition is introduced to handle when one GC invites another GC and he adds a task in lookahead-----//
                mainTaskSchedules['projectCompanyId'] = parent.ballInCourt ? this.findProjectCompanyId(parent.ballInCourt) : JSON.parse(sessionStorage.getItem('lookaheadInfo')).projectCompanyId;;
              } else {
                // alert("51")
                // JSON.parse(sessionStorage.getItem('loginDetails')).companyName;
                mainTaskSchedules['ballInCourt'] = this.projectAssociatedCompany[0].name;
                // this.findProjectCompanyId(JSON.parse(sessionStorage.getItem('loginDetails')).companyName);
                mainTaskSchedules['projectCompanyId'] = this.projectAssociatedCompany[0].projectCompanyId;
              }
            }

            mainTaskSchedules['note'] = parent.note ? parent.note : '';
            mainTaskSchedules['sno'] = parent.serialno ? parent.serialno : '';

            if (parent.timeline) {
              // alert("52")
              parent.timeline.forEach(taskTimeline => {
                mainTaskSchedules.schedules.push({ workingDate: this.datePipe.transform(taskTimeline.columnDate, 'dd-MMM-yyyy'), workerCount: taskTimeline.value == 'x' || taskTimeline.value == 'X' ? '0' : taskTimeline.value });
              })
              parentObj.subTasks.push(mainTaskSchedules);
            } else {
              // alert("53")
              mainTaskSchedules.schedules = between_days;
              parentObj.subTasks.push(mainTaskSchedules);
            }
          }
        });
      } else {
        // alert("54")
        requestpayload.tasks = [];
      }
      let payloadIsTask = requestpayload.tasks.filter(item => {
        return item.id == '' || item.isSubTask;
      });
      payloadIsTask.forEach(item => {
        delete item.isSubTask;
        item.subTasks.forEach((subItem, key) => {
          if (subItem.id != '') {
            item.subTasks.splice(key, 1);
          }
        });
      });
      let payloadData = {
        projectId: this.projectDetails.projectId,
        tasks: [],
        subTaskIds: requestpayload['subTaskIds']
      }
      payloadData['lookaheadDate'] = this.pStartDate;
      // 
      payloadData['submit'] = type;
      payloadData.tasks = payloadIsTask;
      return payloadData;
    }
  }

  // make Task Description Mandatory
  /* make Task Description Mandatory */
  makeTaskDescriptionMandatory(fun) {
    // alert("make")
    let isDescription = [];
    const emptyDecTask = gantt.serialize().data.filter(allItem => !allItem.taskName || allItem.taskName == '');
    if (emptyDecTask.length > 0) {
      emptyDecTask.forEach(element => {
        isDescription.push(element.serialno);
      });
    } else {
      isDescription = [];
    }
    return isDescription;
  }

  /* event:on click save btn */
  submitGanttChart(type: boolean, status?) {
    //  alert("hi im subnit" + this.newTask)
    this.selectedValue = "";
    sessionStorage.removeItem('unsaved');
    const _ganttSerialize = gantt.serialize().data;

    let mData = this.processRequestPayload(_ganttSerialize, type, status);
    if (status == 'popUp') {
      mData.tasks.forEach(data => {
        // console.log(data.subTasks,"sub task level")
        for (let i = 0; i < data.subTasks.length; i++) {
          console.log(data.subTasks[i].schedules)
          delete data.subTasks[i].schedules;
          data.subTasks[i].schedules = [];
          this.refreshData();

        }
      })
      console.log("popup", mData)
    } else {
      let mData = this.processRequestPayload(_ganttSerialize, type, status);

      console.log("without popup", mData)
    }

    if (!type) {
      // alert("im  in if")

      if (mData['tasks'] && mData['tasks'].length > 0) {
        // alert("if 1")

        this.ganttService.addTask(mData).subscribe(res => {
          // $("#loadermodel").modal('hide');
          if (status != 'popUp') {
            // alert("if 2")
            if (res["success"]) {
              // alert(" if 3")
              this.emitter.getMessage(res["message"]);
              mData['tasks'] = [];
              
            }
            this.refreshData();
          }
          this.refreshData();
        });
      }

    } else {
      // alert("im in else")
      // this.currentvalue= ""; 
      this.ganttService.addTask(mData).subscribe(res => {
        // $("#loadermodel").modal('hide');
        if (status != 'popUp') {
          if (res["success"]) {
            this.emitter.getMessage(res["message"]);
            mData['tasks'] = [];
            
          }
          this.refreshData();
        }
        this.refreshData();
      });
    }
    let totalHeight = $(window).height();
    let topSpace = 273;
    let topHeight = totalHeight - topSpace;
    let bottomSpace = 20;
    let ganttHeight = topHeight - bottomSpace;
    let ganttFullHeight = ganttHeight + 2;
    let ganttScrollHeight = ganttHeight - 87;
    let ganttGridHeight = ganttHeight - 90;

    setTimeout(function () {
      $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
      $('.gantt_layout_y').css('height', ganttHeight + 'px');
      $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
      $('.gantt_layout_content').css('height', ganttHeight + 'px');
      $('.gantt_data_area').css('height', ganttGridHeight + 'px');
      $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
      $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');

    });
  }

  refreshData() {

    gantt.clearAll();
    this.getGanttChart();
  }

  //gantchart component

  @ViewChild("gantt_here") ganttContainer: ElementRef;
  /* get selected value from sortBy dropDownbox */
  getSelectedValue(val) {
    this.currentvalue = val;
    this.sortRows();
    // hit sort function directly to dxmhtl.js
    // if (this.currentvalue == "Task No A-Z") {
    //   // //

    //   let field = "serialno"
    //   let desc = true
    //   let parent
    //   if (desc == true) {
    //     desc = false

    //     gantt.sort(field, desc, parent)
    //   } else {

    //     desc = true
    //     gantt.sort(field, desc, parent)

    //   }
    // }
    // else if (this.currentvalue == "Task No Z-A") {
    //   //  //
    //   let field = "serialno"
    //   let desc = false
    //   let parent
    //   if (desc == false) {
    //     desc = true

    //     gantt.sort(field, desc, parent)
    //   } else {
    //     desc = true
    //     gantt.sort(field, desc, parent)

    //   }

    // }
    // else {
    //   //
    //   this.backup();
    // }
  }


  /* Sort All Fields */
  sortRows() {
    let data = [];
    this.sorts = true
    // if (this.checksearch == true) {
    //   // alert("search true")
    //   data = this.searchedArray;
    // } else {
    //   // alert("searchfalse")
    // this.checksearch = false

    // }

    data = this.searcharray;
    // alert("sort()"+this.currentvalue)

    if (this.currentvalue == "Task No A-Z") {
      this.sortType = "asc"
      this.columname = "serialno"
      data.sort(function (a, b) {
        var nameA = a.serialno;
        var nameB = b.serialno;
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });

    } else if (this.currentvalue == "Task No Z-A") {
      this.sortType = "desc"
      this.columname = "serialno"
      data.sort(function (a, b) {
        var nameA = a.serialno;
        var nameB = b.serialno;
        if (nameA > nameB) {
          return -1;
        } else if (nameB > nameA) {
          return 1;
        } else {
          return 0;
        }
      });

    }
    else if (this.currentvalue == "Responsibility A-Z") {
      this.sortType = "asc"
      this.columname = "RESPONSIBILITY"
      data.sort(function (a, b) {
        var nameA = a.ballInCourt.toUpperCase();
        var nameB = b.ballInCourt.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }
        return 0;
      });

    } else if (this.currentvalue == "Responsibility Z-A") {
      this.sortType = "desc"
      this.columname = "RESPONSIBILITY"
      data.sort(function (a, b) {
        var nameA = a.ballInCourt_max.toUpperCase();
        var nameB = b.ballInCourt_max.toUpperCase();
        if (nameA > nameB) {
          return -1;
        } else if (nameB > nameA) {
          return 1;
        } else {
          return 0;
        }

      });
      //


    } else if (this.currentvalue == "Task Start Date A-Z") {
      this.sortType = "asc"
      this.columname = "SCHEDULES"
      data.sort(function (a, b) {
        // //
        // //
        var dateA = new Date(a.starting_date),
          dateB = new Date(b.starting_date);
        if (!a.starting_date && b.starting_date) return 1;
        else if (a.starting_date && !b.starting_date) return -1;
        else if (dateA === dateB) return 0;
        else return (dateA > dateB) ? 1 : (dateB > dateA ? -1 : 0);
      })
      // //

    } else if (this.currentvalue == "Task Start Date Z-A") {
      this.sortType = "desc"
      this.columname = "SCHEDULES"
      data.sort(function (a, b) {

        var dateA = new Date(a.starting_maxdate),
          dateB = new Date(b.starting_maxdate);
        if (!b.starting_maxdate && a.starting_maxdate) return 1;
        else if (b.starting_maxdate && !a.starting_maxdate) return -1;
        else if (dateA === dateB) return 0;
        else return (dateB > dateA) ? 1 : (dateA > dateB ? -1 : 0);
      })
      // //
    }
    gantt.render();
    gantt.refreshData();
    gantt.clearAll();
    gantt.config.sort = true;
    gantt.parse({ data });
  }
  // gantt.render();

  /* search functionality */
  showXmark() {
    this.xmark = true;
  }
  /* condition for X mark in search Functionality */
  xclick() {
    // alert()
    this.query = "";
    this.xmark = false;
    this.checksearch = false;
    this.loadGanttChart();

  }
  /* Search functionality  */
  searchArray() {
    this.click_pagination = false;
    // this.showSpinner(); 
    let searchval = this.query;
    this.checksearch = true;
    // this.click_pagination = false;
    // alert(searchval)
    // this.query =this.query.replace(/\s+/g,'');
    // alert(this.query)
    if (this.query == "" || this.query == null || !this.query) {
      alert("Please enter the word to Search")
    } else {
      this.loadGanttChart()
    }
    /*      result = this.searcharray.filter(val => val.taskName && val.taskName.replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase()) ||
          val.taskType && val.taskType.replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase()) ||
          val.serialno && val.serialno.toString().replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase()) ||
          val.ballInCourt && val.ballInCourt.replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase()) ||
          val.note && val.note.replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase()) ||
          val.starting_date && val.starting_date.replace(/\s/g, "").toLowerCase().includes(this.query.replace(/\s/g, "").toLowerCase())
        );
     */
    /* 
        if (result.length == 0) {
          // alert("no data found")
          this.query = ""
          $('#searchMgs').modal('show');
          // this.loadGanttChart();
    
        } */

    /*   else {
        for (let i = 0; i < this.searcharray.length; i++) {
          for (let j = 0; j < id.length; j++) {
  
            if (this.searcharray[i].taskId == id[j]) {
          
              this.taskids.push(searchdata[i].taskId)
              this.subtaskids.push(searchdata[i].subTaskId)
              // //
              data.push(this.searcharray[i])
              this.searchedArray.push(searchdata[i])
            }
          }
        } */

    /* render whole ganttChart */
    /*    gantt.render();
       gantt.refreshData();
       gantt.clearAll();
       gantt.config.sort = true;
       gantt.parse({ data });
       gantt.refreshData()
       gantt.render();
     } */

  }

  getPaginationData(event) {
    // this.showSpinner();
    this.click_pagination = event;
    // alert("hji"+ this.selectedValue)
    this.selectedValue = "";
    // alert("after"+ this.selectedValue = "";)

    this.pageNo = event.pageNo;
    this.pageSize = event.pageSize;
    this.emitter.setResponseData(event);
    this.loadGanttChart()

  }
  /* load Gantt data from db and formatted to gantt chart data */
  /* Get Gantt Chart*/
  loadGanttChart() {
    this.newTask = false;
    this.isPopup = false;
    let requestPayload = {}
    if (this.click_pagination) {
      // alert("if ")
      requestPayload = {
        "filter": this.filterItem,
        "lookaheadDate": this.pStartDate,
        "projectId": this.projectDetails.projectId,
        "userId": 0,
        "pageNo": this.pageNo,
        "pageSize": this.pageSize,
        "search": this.checksearch,
        "searchTerm": this.query

      }
    }
    else {
      // alert("else")
      requestPayload = {
        "filter": this.filterItem,
        "lookaheadDate": this.pStartDate,
        "projectId": this.projectDetails.projectId,
        "userId": 0,
        "pageNo": 1,
        "pageSize": 10,
        "search": this.checksearch,
        "searchTerm": this.query
      }
    }

    this.spinner.show();
    this.ganttService.getTask(requestPayload).subscribe(success => {
      this.spinner.hide();

      this.getAllTask = success['payload'];
      this.paginationData = success['pageable'];

      this.emitter.getPagination(success['pageable'])

      this.getAllTasks = this.getAllTask.tasks;


      //  if(type == 'filter'){
      this.taskids = this.getAllTask.taskIds;
      this.subtaskids = this.getAllTask.subTaskIds;
      if (this.getAllTasks.length <= 0 && this.checksearch) {
        $('#searchMgs').modal('show');
      }
      //  }
      // if(this.getAllTask.tasks.length>0) {
      //   this.getAllTask.tasks.forEach(data=>{
      //    this.taskids.push(data.id);
      //    if(data.subTasks.length>0) {
      //      data.subTasks.forEach(data1 => {
      //        this.subtaskids.push(data1.id);
      //      });
      //    }
      //  });
      //  }
      // //
      // //
      // if(this.getAllTask.tasks.length > 0 ){
      //   alert(this.getAllTask.tasks.length)
      // }
      let data = [];
      success['payload']['tasks'].forEach(item => {
        let task = {};
        task['id'] = parseFloat(item.sno.split(".").join(item.id));
        task['taskId'] = item.id;
        task['parent'] = 0;
        task['taskStartDate'] = item.taskStartDate;
        task['taskFinishDate'] = item.taskFinishDate;
        task['requiredCompletionDate'] = item.requiredCompletionDate;
        task['deadLineDate'] = item.deadLineDate;
        task['taskName'] = item.description;
        task['serialno'] = parseFloat(item.sno);
        task['taskType'] = item.taskType;
        task['trade'] = item.trade;
        task['specDivision'] = item.specDivision;
        task['accessTo'] = item.accessTo;
        // task['ballInCourt'] = item.ballInCourt;
        //     task['starting_date'] = item.startDate;
        task['note'] = item.note;
        task['child'] = item.child;
        task['$open'] = item.child;

        task['ballInCourt'] = item.subTasks[0].ballInCourt;
        task['ballInCourt_max'] = item.subTasks[0].ballInCourt;

        task['starting_date'] = item.subTasks[0].startDate;
        task['starting_maxdate'] = item.subTasks[0].startDate;
        // //
        item.subTasks.forEach(subTaskItem => {
          if (null != subTaskItem.startDate && new Date(task['starting_date']) > new Date(subTaskItem.startDate)) {
            task['starting_date'] = subTaskItem.startDate;
          }
          if (null != subTaskItem.startDate && new Date(task['starting_maxdate']) < new Date(subTaskItem.startDate)) {
            task['starting_maxdate'] = subTaskItem.startDate;
          }

          if ("" != subTaskItem.ballInCourt && (task['ballInCourt']) > (subTaskItem.ballInCourt)) {
            task['ballInCourt'] = subTaskItem.ballInCourt;
          }
          if ("" != subTaskItem.ballInCourt && (task['ballInCourt_max']) < (subTaskItem.ballInCourt)) {
            task['ballInCourt_max'] = subTaskItem.ballInCourt;
          }
          if (!item.child) {
            // //
            task['taskType'] = subTaskItem.taskType;
            task['trade'] = subTaskItem.trade;
            task['specDivision'] = subTaskItem.specDivision;
            task['ballInCourt'] = subTaskItem.ballInCourt;
            task['accessTo'] = subTaskItem.accessTo;
            task['subTaskId'] = subTaskItem.id;
            //    task['starting_date'] = subTaskItem.startDate;
            if (subTaskItem.hasOwnProperty("schedules") && Array.isArray(subTaskItem.schedules)) {
              task["timeline"] = [];
              if (subTaskItem.schedules.length > 0) {
                //
                //
                // sorting based on schedule ascending
                if (subTaskItem.schedules) {
                  subTaskItem.schedules.sort(function (a, b) {
                    let dateA: any = new Date(a.workingDate), dateB: any = new Date(b.workingDate)
                    return dateA - dateB; //sort by date ascending
                  });
                }

                task['taskStartDate'] = subTaskItem.taskStartDate;
                task['taskFinishDate'] = subTaskItem.taskFinishDate;
                // task['taskStartDate'] = subTaskItem['taskStartDate'];
                // task['taskFinishDate'] = subTaskItem['taskFinishDate'];
                // //
              }

              subTaskItem.schedules.forEach(scheduleItem => {
                let scheduleItemArr;
                task["timeline"].push({
                  columnDate: new Date(this.dateFormat(scheduleItem.workingDate)).toISOString(),
                  id: scheduleItem.id,
                  value: scheduleItem.workerCount == 0 ? 'x' : String(scheduleItem.workerCount),
                  warning: scheduleItem.warning,
                  behindSchedule: scheduleItem.behindSchedule
                });
              });
            }
          } else if (item.child && item.sno != subTaskItem.sno) {
            let subTask = {};
            subTask['id'] = parseFloat(subTaskItem.sno.split(".").join(subTaskItem.taskId));
            subTask['taskId'] = subTaskItem.taskId;
            subTask['subTaskId'] = subTaskItem.id;
            subTask['starting_date'] = subTaskItem.startDate;
            subTask['starting_maxdate'] = subTaskItem.startDate;
            //   task['starting_date'] = subTaskItem.startDate;
            subTask['taskName'] = subTaskItem.description;
            subTask['parent'] = parseFloat(item.sno.split(".").join(""));
            subTask['taskStartDate'] = subTaskItem.taskStartDate;
            subTask['taskFinishDate'] = subTaskItem.taskFinishDate;
            subTask['requiredCompletionDate'] = subTaskItem.requiredCompletionDate;
            subTask['deadLineDate'] = subTaskItem.deadLineDate;
            subTask['serialno'] = subTaskItem.sno;
            subTask['taskType'] = subTaskItem.taskType;
            subTask['trade'] = subTaskItem.trade;
            subTask['specDivision'] = subTaskItem.specDivision;
            subTask['accessTo'] = subTaskItem.accessTo;
            subTask['ballInCourt'] = subTaskItem.ballInCourt;
            subTask['ballInCourt_max'] = subTaskItem.ballInCourt;
            subTask['note'] = subTaskItem.note;

            if (subTaskItem.hasOwnProperty("schedules") && Array.isArray(subTaskItem.schedules)) {
              subTask["timeline"] = [];
              subTaskItem.schedules.forEach(scheduleItem => {
                let scheduleItemArr;
                subTask["timeline"].push({
                  columnDate: new Date(this.dateFormat(scheduleItem.workingDate)).toISOString(),
                  id: scheduleItem.id,
                  value: scheduleItem.workerCount == 0 ? 'x' : String(scheduleItem.workerCount),
                  warning: scheduleItem.warning,
                  behindSchedule: scheduleItem.behindSchedule
                });
              });
              if (subTask) {
                subTask['timeline'].sort(function (a, b) {
                  let dateA: any = new Date(a.columnDate), dateB: any = new Date(b.columnDate)
                  return dateA - dateB; //sort by date ascending
                });
              }
            }
            data.push(subTask);
          }
          else { }
        })
        // 
        data.push(task);

      });
      this.searcharray = data;
      this.filterArray = data;

      this.arrangeAsExcelFormat(data);
      let totalHeight = $(window).height();
      let topSpace = 273;
      let topHeight = totalHeight - topSpace;
      let bottomSpace = 20;
      let ganttHeight = topHeight - bottomSpace;
      let ganttFullHeight = ganttHeight + 2;
      let ganttScrollHeight = ganttHeight - 87;
      let ganttGridHeight = ganttHeight - 90;

      setTimeout(function () {
        $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
        $('.gantt_layout_y').css('height', ganttHeight + 'px');
        $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
        $('.gantt_layout_content').css('height', ganttHeight + 'px');
        $('.gantt_data_area').css('height', ganttGridHeight + 'px');
        $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
        $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');

      });

      // gantt.config.show_task_cells = false;
      // gantt.config.static_background = true;

      gantt.config.smart_rendering = true;
      gantt.config.branch_loading = true;
      gantt.refreshData();
      gantt.clearAll();
      gantt.config.sort = true;
      gantt.render();
      gantt.parse({ data })


    }
      , error => {

      })

  }



  // onChangePage(pageOfItems: Array<any>) {
  //   let data = []
  //   // 
  //   this.pageOfItems = pageOfItems;
  //   
  //   //  this.loadGanttChart()

  //   data = this.pageOfItems;
  //   gantt.refreshData();
  //   gantt.parse({ data })
  //   gantt.render();

  // }

  changeSubTask(item) {
    if (item.getAttribute('data-parent') == '0') {
      return item.getAttribute('data-tid');
    } else {
      return item.getAttribute('data-staskid');
    }
  }

  getselectedItem(pay, rowId) {
    ////
    ////
    let x = gantt.serialize().data.filter(item => item.id == rowId);
    // //
    ////
    if (pay.getAttribute('data-fieldtype') == 'Task Description') {
      return x;
    } else if (pay.getAttribute('data-fieldtype') == 'Notes') {
      return x;
    } else {
      return x[0][pay.getAttribute('data-fieldtype')];
    }
  }

  makingSchedules(value, item) {
    let schedulesPayload = {};
    ////
    if (item.getAttribute('data-scheduleId')) {
      ////
      schedulesPayload['workerCount'] = value;
      schedulesPayload['id'] = parseInt(item.getAttribute('data-scheduleId'));
      schedulesPayload['subTaskId'] = item.getAttribute('data-staskid');
      // schedulesPayload['workingDate'] = item.getAttribute('data-columndate');
    } else {
      // //
      schedulesPayload['workerCount'] = value;
      schedulesPayload['id'] = '';
      schedulesPayload['subTaskId'] = item.getAttribute('data-staskid');
      schedulesPayload['workingDate'] = item.getAttribute('data-columndate');
      // schedulesPayload['workingDate'] = this.datePipe.transform(item.getAttribute('data-columndate'), 'dd-MMM-yyyy');
    }
    ////
    return schedulesPayload;
  }

  makingSubTask(value, item) {
    let subTaskPayload = {
      schedules: []
    };
    if (item.getAttribute('data-staskid') != 'undefined') {
      ////
      subTaskPayload['id'] = parseInt(item.getAttribute('data-staskid'));
      let schedulePayload = this.makingSchedules(value, item);
      subTaskPayload.schedules.push(schedulePayload);
    } else {
      ////
      ////
      subTaskPayload['id'] = parseInt(item.getAttribute('data-tid'));
      let schedulePayload = this.makingSchedules(value, item);
      subTaskPayload.schedules.push(schedulePayload);
    }
    return subTaskPayload;
  }

  makingFieldType(value, item) {
    ////
    ////
    let fieldTask = {
      subTasks: []
    }
    let fieldSubTask = {
      schedules: []
    }

    // conflict from master

    if (item.getAttribute('data-fieldtype') == 'Task Description' || item.getAttribute('data-fieldtype') == 'Notes') {
      ////
      let selectedItem = this.getselectedItem(item, item.getAttribute('data-rowid'));
      ////
      if (selectedItem[0].parent == '0') {
        fieldTask['description'] = selectedItem[0].taskName;
        fieldTask['note'] = selectedItem[0].note;
        fieldTask['id'] = parseInt(selectedItem[0].taskId);
        fieldSubTask['description'] = selectedItem[0].taskName;
        fieldSubTask['id'] = parseInt(selectedItem[0].subTaskId);
        fieldSubTask['note'] = selectedItem[0].note;
        fieldTask.subTasks.push(fieldSubTask);
      } else {
        ////
        fieldTask['id'] = parseInt(selectedItem[0].taskId);
        fieldSubTask['description'] = selectedItem[0].taskName;
        fieldSubTask['note'] = selectedItem[0].note;
        fieldSubTask['id'] = parseInt(selectedItem[0].subTaskId);
        fieldTask.subTasks.push(fieldSubTask);
      }
    } else {
      if (item.getAttribute('data-parent') == '0') {
        fieldTask[item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        fieldTask['id'] = parseInt(item.getAttribute('data-tid'));
        fieldSubTask[item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        fieldSubTask['id'] = parseInt(item.getAttribute('data-staskid'));
        fieldTask.subTasks.push(fieldSubTask);
      } else {
        fieldSubTask[item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        fieldSubTask['id'] = parseInt(item.getAttribute('data-staskid'));
        fieldTask['id'] = parseInt(item.getAttribute('data-tid'));
        fieldSubTask['schedules'] = [];
        fieldTask.subTasks.push(fieldSubTask);
      }
    }
    ////
    return fieldTask;
  }
  getAlertDates(data, event) {
    // //
    // //
    let requiredCompletionDate = null;
    let deadlineDate = null;
    let minimumDate = null;
    if (data) {
      if (data['requiredCompletionDate'] && data['requiredCompletionDate'] != null) {
        requiredCompletionDate = new Date(data['requiredCompletionDate']);
      }
      if (data['deadLineDate'] && data['deadLineDate'] != null) {
        deadlineDate = new Date(data['deadLineDate']);
      }
    }
    if (requiredCompletionDate == null) {
      minimumDate = deadlineDate;
    } else if (deadlineDate == null) {
      minimumDate = requiredCompletionDate;
    } else {
      if (requiredCompletionDate < deadlineDate) {
        minimumDate = requiredCompletionDate;
      } else if (requiredCompletionDate > deadlineDate) {
        minimumDate = deadlineDate;
      } else {
        minimumDate = requiredCompletionDate;
      }
    }
    let alertDays = [];
    let lookaheadInfo = JSON.parse(sessionStorage.getItem('lookaheadInfo'));
    if (minimumDate != null) {
      for (let i = 0; i <= lookaheadInfo.floatDays; i++) {
        let d = new Date(minimumDate);
        d.setDate(d.getDate() - i);
        alertDays.push(this.datePipe.transform(d, 'dd-MMM-yyyy'));
      }
    }
    let value = $('#' + event.target.id).val();
    let columnDate = new Date($('#' + event.target.id).attr('data-columndate'));
    if (minimumDate != null) {
      if (value != "") {
        if (new Date(alertDays[0]) < new Date(columnDate)) {
          $("#" + event.target.id).addClass('behind-schedule');
        } else if (new Date(alertDays[alertDays.length - 1]) > new Date(columnDate)) {
          $("#" + event.target.id).addClass('highlighted');
        } else {
          if (alertDays.indexOf(columnDate) == -1) {
            $("#" + event.target.id).addClass('warning');
          }
        }
      } else {
        $("#" + event.target.id).removeClass('highlighted');
        $("#" + event.target.id).removeClass('warning');
        $("#" + event.target.id).removeClass('behind-schedule');
      }
    } else {
      if (value == "") {
        $("#" + event.target.id).removeClass('highlighted');
        $("#" + event.target.id).removeClass('warning');
        $("#" + event.target.id).removeClass('behind-schedule');
      } else {
        $("#" + event.target.id).addClass('highlighted');
      }
    }
  }

  updateFieldtype(value, payload, item) {
    ////
    //
    ////
    let changesTaskField = payload.tasks.filter(task => task.id == item.getAttribute('data-tid'));
    ////
    if (changesTaskField.length > 0) {
      let changesSubTaskField = changesTaskField[0].subTasks.filter(subTask => subTask.id == item.getAttribute('data-staskid'));
      ////
      if (changesSubTaskField.length > 0) {
        ////
        ////
        ////
        let selectedItem = this.getselectedItem(item, item.getAttribute('data-rowid'));
        //
        ////
        if (item.getAttribute('data-fieldtype') == 'Task Description') {
          changesSubTaskField[0]['description'] = selectedItem[0].taskName;
          changesTaskField[0]['description'] = selectedItem[0].taskName;
        } else if (item.getAttribute('data-fieldtype') == 'Notes') {
          changesSubTaskField[0]['note'] = selectedItem[0].note;
          changesTaskField[0]['note'] = selectedItem[0].note;
        } else {
          changesSubTaskField[0][item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
          changesTaskField[0][item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        }
      } else {
        ////
        ////
        let newSubTask = {};
        newSubTask['id'] = parseInt(item.getAttribute('data-staskid'));
        newSubTask['schedules'] = [];
        let selectedItem = this.getselectedItem(item, item.getAttribute('data-rowid'));
        ////
        if (item.getAttribute('data-fieldtype') == 'Task Description') {
          newSubTask['description'] = selectedItem[0].taskName;
          //changesTaskField[0]['description'] = selectedItem[0].taskName;
        } else if (item.getAttribute('data-fieldtype') == 'Notes') {
          newSubTask['note'] = selectedItem[0].note;
          //changesTaskField[0]['note'] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        } else {
          newSubTask[item.getAttribute('data-fieldtype')] = this.getselectedItem(item, item.getAttribute('data-rowid'));
        }
        changesTaskField[0].subTasks.push(newSubTask);
      }
    } else {
      ////
      let newTaskPayload = {
        subTasks: []
      };
      newTaskPayload['id'] = parseInt(item.getAttribute('data-tid'));
      let finalSubTask = this.makingFieldType(value, item);
      ////
      newTaskPayload.subTasks.push(finalSubTask);
      payload.tasks.push(finalSubTask);
    }
  }
  /* function for spinner  */
  // showSpinner() {
  //   // alert()
  //   this.spinner.show();
  //   setTimeout(() => {
  //     /** spinner ends after 5 seconds */
  //     this.spinner.hide();
  //   }, 4000);

  // }

  ngOnInit() {
    this.loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    //
    sessionStorage.removeItem('unsaved');
    sessionStorage.removeItem('status')
    // this.showSpinner();


    this.emitterData = this.emitter.unSaveCall.subscribe(data => {
      if (data != "") {
        //  alert("hi ji")
        this.unSavedPopup(data)
      }
    })
    // DatePicker Functionality
    $(document).ready(() => {
      $('#projectStartDate').datepicker({
        dateFormat: 'dd-M-yy',
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        showOtherMonths: true,
        onSelect: (date) => {
          if (date) {
            ////
            this.pStartDate = date;
            this.selectedLookaheadDate = date;
            // //
            $('#projectStartDate').val(date);
            this.onDateChanged(date);
            var selectedDate = new Date(date);
            var endDate = new Date(selectedDate.getTime());
            $("#taskStartDate").datepicker("option", "minDate", endDate);
          }
        }
      });
      /* function for previous day, previous week << < */
      // $('#nextweek,#previousweek').on('click', function (e) {
      //   var date = new Date($('#projectStartDate').val());
      //   e.target.id == 'nextweek' ? date.setDate(date.getDate() + 7) : date.setDate(date.getDate() - 7);
      //   $('#projectStartDate').datepicker('setDate', date);
      //   that.onDateChanged(that.datePipe.transform(date, 'dd-MMM-yyyy'));
      // });
      // /* function for Previous day & next day */
      // $('#previousday,#nextday').on('click', function (e) {
      //   var date = new Date($('#projectStartDate').val());

      //   e.target.id == 'nextday' ? date.setDate(date.getDate() + 1) : date.setDate(date.getDate() - 1);

      //   $('#projectStartDate').datepicker('setDate', date);
      //   that.onDateChanged(that.datePipe.transform(date, 'dd-MMM-yyyy'));
      // });
    });
    this.loginDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    // //
    let totalHeight = $(window).height();
    let topSpace = 273;
    let topHeight = totalHeight - topSpace;
    let bottomSpace = 20;
    let ganttHeight = topHeight - bottomSpace;
    let ganttFullHeight = ganttHeight + 2;
    let ganttScrollHeight = ganttHeight - 87;
    let ganttGridHeight = ganttHeight - 90;

    setTimeout(function () {
      $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
      $('.gantt_layout_y').css('height', ganttHeight + 'px');
      $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
      $('.gantt_layout_content').css('height', ganttHeight + 'px');
      $('.gantt_data_area').css('height', ganttGridHeight + 'px');
      $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
      $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');
    });
    let totalWidth = $(window).width();
    let leftSpace = 50;
    let leftWidth = totalWidth - leftSpace;
    let rightSpace = 100;
    let ganttWidth = leftWidth - rightSpace;
    setTimeout(function () {
      $('.gantt_container').css('width', ganttWidth + 'px');
    }, 1000);
    this.projectDetails = JSON.parse(sessionStorage.getItem("projectDetails"));
    // alert(this.projectDetails.projectId)

    gantt['trade'] = this.projectDetails.trade;
    this.getMasterData();
    $(document).ready(function () {
      $("#projectPlan").on('shown.bs.modal', function () {
        $(this).find('#system-search').focus();
      });
    });
    $(document).ready(function () {
      $('body').css('overflow-y', 'scroll');
    });
    this.getProjectCompanyDetail();
    let that = this;
    var lookaheadsResponse;

    gantt.closeValidation1 = function () {
      $('.validate-message').addClass('hide-validate-message');
    }
    gantt.refreshGanttChart = function () {
      that.loadGanttChart();
    }
    gantt.closeValidation2 = function () {
      $('.validate-message-datepicker').addClass('hide-validate-message');
    }
    gantt.closeValidation3 = function () {
      $('.validate-date-start').addClass('hide-date-start');
    }
    gantt.dateFormatChange = function (date) {
      let dateObj = date.split('-');
      return dateObj[0] + "," + dateObj[1] + " " + dateObj[2];
    }


    /* function for next week and next day  called in dhtmlxgantt.js  > >> */
    gantt.nextDay = function (e) {
      // that.spinner.show();
      if (sessionStorage.getItem('unsaved') != undefined || sessionStorage.getItem('unsaved') != null) {
        $('#unsavedDatapopup').modal('show');
        sessionStorage.setItem('status', e);
      } else {
        var date = new Date($('#projectStartDate').val());
        if (e == 'nextDay') { date.setDate(date.getDate() + 1) }
        else if (e == 'nextWeek') { date.setDate(date.getDate() + 7); }
        $('#projectStartDate').datepicker('setDate', date);
      }
      that.onDateChanged(that.datePipe.transform(date, 'dd-MMM-yyyy'));

    }
    var oldStartDate = '';
    var oldEndDate = '';
    var newStartDate = '';
    var newEndDate = '';
    /* Js function called for updateTask */
    gantt.updateTask = function (label, id, value) {
      //  alert('Within update task');
      //  alert(oldStartDate);
      newStartDate = $('#taskStartDate').val();
      newEndDate = $('#expectedDate').val();
      //alert(newStartDate);

      that.isPopup = true;

      if ($('#textDescription').val() == "" || $('#textDescription').val().trim().length == 0) {
        $('.validate-message').removeClass('hide-validate-message');
        $('#textDescription').focus();
      }
      else if ($('#taskStartDate').val() != "" && $('#expectedDate').val() == "") {
        $('.validate-message-datepicker').removeClass('hide-validate-message');
        $('#expectedDate').focus();
      } else if ($('#taskStartDate').val() == "" && $('#expectedDate').val()) {
        $('.validate-date-start').removeClass('hide-date-start');
        $('#taskStartDate').focus();
      }
      else {
        var customColumn = this.getGridColumns();
        var between_days = [];
        if ($("#taskStartDate").datepicker("getDate") != null && $("#taskStartDate").datepicker("getDate") != null) {
          var start = $("#taskStartDate").datepicker("getDate"),
            end = $("#expectedDate").datepicker("getDate"),
            currentDate = new Date(start),
            date;
        }
        while (currentDate <= end) {
          date = $.datepicker.formatDate("dd-M-yy", new Date(currentDate));
          if (gantt.leaveDays.indexOf(new Date(currentDate).getDay()) == -1) {
            between_days.push({ "workingDate": date, 'workerCount': "0" });
          }
          currentDate.setDate(currentDate.getDate() + 1);
        }
        var defaltColumn = gantt.config.layout.cols;
        //
        //
        var item = gantt.getTask(id);

        // //
        item['note'] = $('#textNotes').val() ? $('#textNotes').val() : '';
        item['taskStartDate'] = $('#taskStartDate').val() ? $('#taskStartDate').val() : '';
        // alert("tsd"+ item['taskStartDate'] );
        item['taskFinishDate'] = $('#expectedDate').val() ? $('#expectedDate').val() : '';
        // alert("tfd"+ item['taskFinishDate']);
        item['requiredCompletionDate'] = $('#requiredDate').val() ? $('#requiredDate').val() : '';
        item['deadLineDate'] = $('#deadlineDate').val() ? $('#deadlineDate').val() : '';
        item['ballInCourt'] = $('#responsibilityOption').val() ? $('#responsibilityOption').val() : '';
        item['taskType'] = $('#taskType').val() ? $('#taskType').val() : '';
        item['accessTo'] = $('#accessTo').val() ? $('#accessTo').val() : '';
        item['taskName'] = $('#textDescription').val() ? $('#textDescription').val() : '';
        item['trade'] = $('#tradeType').val() ? $('#tradeType').val() : '';
        item['specDivision'] = $('#specDivisionData').val() ? $('#specDivisionData').val() : '';

        console.log("item.", item)
        // item['schedules'] = between_days ? between_days : '';
        that.popupid = item.subTaskId
        // $('#myTextDesModal').modal('hide');
        gantt.refreshData();
        // $("body").find("#myTextDesModal").remove();
        // $(".modal-backdrop").remove();
        // $("body").removeClass("modal-open");
        // $('#loadermodel').modal({backdrop: 'static', keyboard: false})  
        // $("#loadermodel").modal('show');
        console.log(oldStartDate != newStartDate);





        if (oldStartDate != null && oldStartDate != "" && (oldStartDate != newStartDate || oldEndDate != newEndDate)) {
          var success;
          swal("When start or end date is updated, split task will be changed to an uninterrupted task.To confirm click OK", {
            buttons: ["Cancel", "Ok"],
          })
            .then(function (isConfirm) {
              if (isConfirm) {
                $('#myTextDesModal').modal('hide');
                gantt.refreshData();
                $("body").find("#myTextDesModal").remove();
                $(".modal-backdrop").remove();
                $("body").removeClass("modal-open");
                that.submitGanttChart(false,'popUp');
              } else {
              //  incase "that" used for "this"
              }
            });
        }
        else {
          $("body").find("#myTextDesModal").remove();
          $(".modal-backdrop").remove();
          // $("body").removeClass("modal-open");
          $(document).ready(function () {
            $('#updateMsg').modal('show');
          });
          that.submitGanttChart(false,'popUp');
        }
      }
    }

    /* Js function called from dxhtml.js , function for open modal popup ,create a newtask  */
    gantt.openModalBoxTask = function (textArea, label, id, value) {
      // //
      var columns = gantt.getGridColumns();


      // //
      var projectDetails = JSON.parse(sessionStorage.getItem('projectDetails'));
      if (textArea == 'taskDescription') {
        $(document).ready(function () {
          $("#myTextDesModal").on('shown.bs.modal', function () {
            $(this).find('.description-focus').focus();
          });
        });
      } else if (textArea == 'requiredDate') {
        $(document).ready(function () {
          if (projectDetails.projectRole != 'SC-Manager') {
            $("#myTextDesModal").on('shown.bs.modal', function () {
              $(this).find('#requiredDate').focus();
            });
          }
        });
      }
      else {
        $(document).ready(function () {

          $("#myTextDesModal").on('shown.bs.modal', function () {
            $(this).find('#textNotes').focus();
          });

        });
      }

      if ($("body").find("#myTextDesModal").length != 0) {
        $("body").find("#myTextDesModal").remove();
      }

      let addedtask = gantt.getTask(id);
      console.log("addedTask", addedtask.taskId)

      if (addedtask.taskId == "") {
        that.newTask = true;
      } else {
        that.newTask = false;
      }
      // //
      var taskTypeOptions;
      if (gantt["taskType"]) {
        // //
        gantt['taskType'].forEach((data) => {
          // //
          taskTypeOptions += addedtask.taskType == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option selected value="' + data + '">' + data + '</option>';
        });
      }

      var accessToOptions;
      if (gantt['visibility']) {
        gantt['visibility'].forEach((data) => {
          // //
          accessToOptions += addedtask.accessTo == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option value="' + data + '">' + data + '</option>';
        });
      }
      var tradeOptions;

      let projectTrade = JSON.parse(window.sessionStorage.getItem('lookaheadInfo'));
      //
      if (projectTrade['trade'] != null && projectTrade['trade'] != undefined) {
        // //
        projectTrade['trade'].forEach((data) => {
          tradeOptions += addedtask.trade == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option value="' + data + '">' + data + '</option>';
        });
      }

      var specDivisionOptions = "";

      let projectSpec = JSON.parse(window.sessionStorage.getItem('lookaheadInfo'));
      //
      if (projectSpec['specDivision'] != null && projectSpec['specDivision'] != undefined) {
        //
        projectSpec['specDivision'].forEach((data) => {
          //
          specDivisionOptions += addedtask.specDivision == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option value="' + data + '">' + data + '</option>';
        });
      }
      //
      //

      // var projectRole = projectTrade.projectRole;
      var responsibilityOptions = '';

      let responsibilityObj = JSON.parse(window.sessionStorage.getItem('projectCompanyList'));
      // //
      if (responsibilityObj) {
        let res = responsibilityObj[0]['name'];
        responsibilityObj.forEach((data) => {
          // //
          // //
          responsibilityOptions += res == data['name'] ? '<option selected value="' + data['name'] + '">' + data['name'] + '</option>' : '<option  value="' + data['name'] + '">' + data['name'] + '</option>';
          // //
        });
      }
      var taskAccessTo;
      if (addedtask.parent == 0 && addedtask.child) {
        taskAccessTo = '';
      } else {
        $('#cmd').click(function () {
          $('#content').append('<br>a datepicker <input class="datepicker_recurring_start"/>');
        });
        $('body').on('focus', ".datepicker_recurring_start", function () {
          $(this).datepicker();
        });
        taskAccessTo = '<div class="row form-group popup-datepickers">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Task Start Date</label>' +
          '</div>' +
          '<div class="col-lg-8 tstart-date" id="content">' +

          '<input type="text" id="taskStartDate" class="form-control" onchange=\'gantt.closeValidation3()\'>' +
          '<div class="validate-date-start hide-date-start">' + '<p class="validate-color">* Please enter the start date</p>' + '</div>' +
          '<i class="calendar-popup1 fa fa-calendar" aria-hidden="true"></i><span onclick=\'gantt.dateXmark()\' id="sDate1"  class="task1 date-close date1">X</span>' +

          '</div>' +
          '</div>' +
          '<div class="row form-group popup-datepickers">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Task Finish Date</label>' +
          '</div>' +
          '<div class="col-lg-8 exp-date" >' +
          '<input type="text" id="expectedDate" class="form-control" onchange=\'gantt.closeValidation2()\' >' + '<i class="calendar-popup2 fa fa-calendar" aria-hidden="true"></i><span class="date2 task2 date-close" id="sDate2"  onclick=\'gantt.dateXmark1()\'>X</span>' +
          '</div>' + '<div class="col-lg-4 popupVertical">' + '</div>' + '<div class="validate-message-datepicker hide-validate-message">' + '<p class="validate-color">* Please enter the expected completion date</p>' + '</div>' +
          '</div>' +
          '<div class="row form-group popup-datepickers">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Target Completion Date</label>' +
          '</div>' +
          '<div class="col-lg-8 req-com">' +
          '<input type="text" id="requiredDate" class="form-control">' + '<i class="calendar-popup3 fa fa-calendar" aria-hidden="true"></i> <span class="task3 date-close date3" id="sDate3" onclick=\'gantt.dateXmark2()\'>X</span>' +
          '</div>' +
          '</div>' +
          '<div class="row form-group popup-datepickers" >' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Deadline</label>' +
          '</div>' +
          '<div class="col-lg-8 deadline-date" >' +
          '<input type="text" id="deadlineDate" class="form-control">' + '<i class="calendar-popup4 fa fa-calendar" aria-hidden="true"></i><span class="task4 date-close date4" id="sDate4" onclick=\'gantt.dateXmark3()\'>X</span>' +
          '</div>' +
          '</div>' +
          '<div class="row form-group">' +
          '<div class="col-lg-4 text-right">' +
          '<label style="margin-top: 5px;">Responsibility</label>' +
          '</div>' +
          '<div class="col-lg-8">' +
          '<select class="form-control" id="responsibilityOption">' +
          responsibilityOptions +
          '</select>' +
          '</div>' +
          '</div>' +
          '<div class="row form-group">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Task Type</label>' +
          '</div>' +
          '<div class="col-lg-8">' +
          '<select class="form-control" id="taskType">' +
          '<option>--select--</option>' +
          taskTypeOptions +
          '</select>' +
          '</div>' +
          '</div>' +
          '<div class="row form-group">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Trade Type</label>' +
          '</div>' +
          '<div class="col-lg-8">' +
          '<select class="form-control" id="tradeType">' +
          '<option>--select--</option>' +
          tradeOptions +
          '</select>' +
          '</div>' +
          '</div>' +
          '<div class="row form-group">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="comment" style="margin-top: 5px;">Spec Division</label>' +
          '</div>' +
          '<div class="col-lg-8">' +
          '<select class="form-control" id="specDivisionData">' +
          '<option>--select--</option>' +
          specDivisionOptions +
          '</select>' +
          '</div>' +
          '</div>' +
          // '<div class="row form-group">' +
          // '<div class="col-lg-4 text-right">' +
          // '<label style="margin-top: 5px;">Access To</label>' +
          // '</div>' +
          // '<div class="col-lg-8">' +
          // '<select class="form-control" id="accessTo">' +
          // accessToOptions +
          // '</select>' +
          // '</div>' +
          // '</div>' +
          '<div class="row form-group">' +
          '<div class="col-lg-4 text-right">' +
          '<label for="note" style="margin-top: 35px;">Notes</label>' +
          '</div>' +
          '<div class="col-lg-8">' +
          '<textarea class="form-control" maxlength="100" style="height: 100px;" rows="5" id="textNotes"></textarea>' +
          '</div>' +
          '</div>';
      }

      var html = '<div class="modal fade" id="myTextDesModal" data-backdrop="static" data-keyboard="false">' +
        '<div class="modal-dialog" style="max-width: 700px;max-height: 600px;">' +
        '<div class="modal-content">' +
        '<div class="modal-header">' +
        '<h4 class="modal-title" style="margin: 0 auto;">Task Description & More Details</h4>' +
        '<button type="button" class="close" data-dismiss="modal" onclick=\'gantt.refreshGanttChart()\'>&times;</button>' +
        // '<button type="button" class="close" data-dismiss="modal" onclick=\'gantt.$click.buttons["delete"](' + id + ')\'>&times;</button>' +
        '</div>' +
        '<div class="modal-body cus-lookahead-model">' +
        '<div class="row form-group">' +
        '<div class="col-lg-4 popupVertical">' +
        '<label for="comment" style="margin-top: 35px;">Task Description *</label>' +
        '</div>' +
        '<div class="col-lg-8">' +
        '<textarea class="form-control description-focus" maxlength="100" style="height: 100px;" autofocus rows="5" id="textDescription" onkeyup=\'gantt.closeValidation1()\'></textarea>' +
        '</div>' + '<div class="col-lg-4 popupVertical">' + '</div>' + '<div class="validate-message hide-validate-message">' + '<p class="validate-color">* Please enter the task description</p>' + '</div>' +
        '</div>' +
        taskAccessTo +
        '</div>' +
        '<div class="modal-footer">' +
        '<button type="button" class="btn saveBtn popup-save" onclick=\'gantt.updateTask("' + label + '","' + id + '","' + escape(value) + '")\'>Save</button>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';
      //if($("body").find("#myTextDesModal").length==0){  
      $('body').append(html);
      // //
      $('#taskStartDate,#expectedDate,#requiredDate,#deadlineDate').keydown(function (e) {
        if (e.keyCode != 9) {
          e.preventDefault();
          return false;
        }
      });
      /* Condition for  project roles  */
      if (projectDetails.projectRole == "Owner" || projectDetails.projectRole == "SC-Observer" || projectDetails.projectRole == "GC-Observer") {
        $('#taskStartDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('#expectedDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('#requiredDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('#deadlineDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('.calendar-popup1').addClass('hide-date-start');
        $('.calendar-popup2').addClass('hide-date-start');
        $('.calendar-popup3').addClass('hide-date-start');
        $('.calendar-popup4').addClass('hide-date-start');

        $(".req-com").find("span").remove();
        $(".deadline-date").find('span').remove();
        $(".tstart-date").find("span").remove();
        $(".exp-date").find('span').remove();
        $('.popup-save').addClass('btn-disabled');
        $('.popup-save').keydown(function (e) {
          e.preventDefault();
          return false;
        });
        // $('#taskStartDate,#expectedDate,#requiredDate,#deadlineDate').keydown(function(e) {
        //   if(e.keyCode !== 9) {
        //     e.preventDefault();

        //   return false;
        //   }
        // });
        $('#taskStartDate,#expectedDate,#requiredDate,#deadlineDate').keydown(function (e) {
          if (e.keyCode == 9) {
            $(this).focus();

            e.preventDefault();
            return false;
          }
        });
      }
      // //
      if (projectDetails.projectRole == "SC-Manager") {

        $('#requiredDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('#deadlineDate').addClass('hideRequiredDate').removeClass('hasDatepicker');
        $('.calendar-popup3').addClass('hide-date-start');
        $('.calendar-popup4').addClass('hide-date-start');
        $(".req-com").find("span").remove();
        $(".deadline-date").find('span').remove();
        $('#taskStartDate,#expectedDate,#requiredDate,#deadlineDate').keydown(function (e) {
          if (e.keyCode == 9) {
            e.preventDefault();
            return false;
          }
        });
      }
      $('#myTextDesModal').modal('show');
      if (textArea == 'taskDescription') {
        $('#textDescription').val(unescape(value).trim() == 'Click here...' ? '' : unescape(value).trim());
      } else {
        $('#textDescription').val(addedtask.taskName == 'Click here...' ? '' : addedtask.taskName);
      }
      if (addedtask.note == null || addedtask.note == undefined) {
        $('#textNotes').val(addedtask.note == '' ? 'Click here...' : addedtask.note);
      }
      else {
        $('#textNotes').val(addedtask.note == 'Click here...' ? '' : addedtask.note);
      }
      //
      if (addedtask['timeline']) {
        addedtask['timeline'].sort(function (a, b) {
          let dateA: any = new Date(a.columnDate), dateB: any = new Date(b.columnDate)
          return dateA - dateB; //sort by date ascending
        });
      }
      //
      $('#requiredDate').val(addedtask.requiredCompletionDate ? addedtask.requiredCompletionDate : '');
      $('#expectedDate').val(addedtask.taskFinishDate ? addedtask.taskFinishDate : '');
      $('#taskStartDate').val(addedtask.taskStartDate ? addedtask.taskStartDate : '');
      $('#deadlineDate').val(addedtask.deadLineDate ? addedtask.deadLineDate : '');
      $('#taskType').val(addedtask.taskType ? addedtask.taskType : '');
      $('#responsibilityOption').val(addedtask.ballInCourt ? addedtask.ballInCourt : '');
      $('#tradeType').val(addedtask.trade ? addedtask.trade : '');
      oldStartDate = addedtask.taskStartDate;
      oldEndDate = addedtask.taskFinishDate;


      var lookaheadInfo = JSON.parse(sessionStorage.getItem('lookaheadInfo'));
      var lookaheadDate = lookaheadInfo.lookaheadDate;
      $(document).ready(function () {
        $('.calendar-popup1').click(function () {
          $("#taskStartDate").focus();
        });
      });
      $(document).ready(function () {
        $('.calendar-popup2').click(function () {
          $("#expectedDate").focus();
        });
      });
      $(document).ready(function () {
        $('.calendar-popup3').click(function () {
          $("#requiredDate").focus();
        });
      });
      $(document).ready(function () {
        $('.calendar-popup4').click(function () {
          $("#deadlineDate").focus();
        });
      });
      // //
      if ($("#taskStartDate").val() == '') {
        $(".date1").removeClass('display-block').addClass('task1');
      } else {
        $(".date1").addClass('display-block').removeClass('task1');
      }
      if ($("#expectedDate").val() == '') {
        $(".date2").removeClass('display-block').addClass('task2');
      } else {
        $(".date2").addClass('display-block').removeClass('task2');
      }
      if ($("#requiredDate").val() == '') {
        $(".date3").removeClass('display-block').addClass('task3');
      } else {
        $(".date3").addClass('display-block').removeClass('task3');
      }
      if ($("#deadlineDate").val() == '') {
        $(".date4").removeClass('display-block').addClass('task4');
      } else {
        $(".date4").addClass('display-block').removeClass('task4');
      }
      $("#taskStartDate").datepicker({
        changeMonth: true,
        // $('.date').datepicker('setDate', null);
        changeYear: true,
        dateFormat: 'dd-M-yy',
        minDate: $('#projectStartDate').val(),
        selectOtherMonths: true,
        showOtherMonths: true,
        onSelect: (date) => {
          var selectedDate = new Date(date);
          $('.validate-date-start').addClass('hide-date-start');
          var endDate = new Date(selectedDate.getTime());
          //Set Minimum Date of EndDatePicker After Selected Date of StartDatePicker
          $("#expectedDate").datepicker("option", "minDate", endDate);
          $(".task1").addClass('display-block').removeClass('task1');
        },
        beforeShowDay: function (d) {
          return [!(d.getDay() == gantt.leaveDays[0] || d.getDay() == gantt.leaveDays[1])]
        },
        // }
      });
      // //
      $("#expectedDate").datepicker({
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        showOtherMonths: true,
        onSelect: (date) => {
          $(".task2").addClass('display-block').removeClass('task2');
          $('.validate-message-datepicker').addClass('hide-validate-message');
        },
        beforeShowDay: function (d) {
          return [!(d.getDay() == gantt.leaveDays[0] || d.getDay() == gantt.leaveDays[1])]
        },
        dateFormat: 'dd-M-yy',
        minDate: $("#taskStartDate").val()
      });
      $("#requiredDate").datepicker({
        changeMonth: true,
        changeYear: true,
        selectOtherMonths: true,
        showOtherMonths: true,
        onSelect: (date) => {
          $(".task3").addClass('display-block').removeClass('task3');
        },
        dateFormat: 'dd-M-yy'
      });
      $("#deadlineDate").datepicker({
        changeMonth: true,
        selectOtherMonths: true,
        showOtherMonths: true,
        onSelect: (date) => {
          $(".task4").addClass('display-block').removeClass('task4');
        },
        changeYear: true,
        dateFormat: 'dd-M-yy'
      });

    }
    let projectDetails = JSON.parse(window.sessionStorage.getItem('projectDetails'));
    gantt.dateXmark = function () {
      $('#taskStartDate').datepicker('setDate', null);
      $(".date1").removeClass('display-block').addClass('task1');
    }
    gantt.dateXmark1 = function () {
      $('#expectedDate').datepicker('setDate', null);
      $(".date2").removeClass('display-block').addClass('task2');
    }
    gantt.dateXmark2 = function () {
      if (projectDetails.projectRole != "SC-Manager") {
        $('#requiredDate').datepicker('setDate', null);
        $(".date3").removeClass('display-block').addClass('task3');
      }
    }
    gantt.dateXmark3 = function () {
      if (projectDetails.projectRole != "SC-Manager") {
        $('#deadlineDate').datepicker('setDate', null);
        $(".date4").removeClass('display-block').addClass('task4');
      }
    }
    /*  function for task edit delete in lookahead */
    gantt.$click = {
      buttons: {
        "edit": function (id) {
        },
        "delete": (id) => {
          const question = gantt.locale.labels.confirm_deleting;
          const title = gantt.locale.labels.confirm_deleting_title;
          gantt._dhtmlx_confirm(question, title, () => {
            if (!gantt.isTaskExists(id)) {
              gantt.hideLightbox();
              return;
            }
            const task = gantt.getTask(id);

            if (task.$new) {
              gantt.silent(function () {
                gantt.deleteTask(id, true);
              });
              gantt.refreshData();
              gantt.render();
            } else {
              ////
              this.ganttService.lookaheadDeleteTask(task).subscribe(res => {
                ////
                if (res["success"]) {
                  $("#deleteMgs").modal("show");
                  let totalHeight = $(window).height();
                  let topSpace = 273;
                  let topHeight = totalHeight - topSpace;
                  let bottomSpace = 20;
                  let ganttHeight = topHeight - bottomSpace;
                  let ganttFullHeight = ganttHeight + 2;
                  let ganttScrollHeight = ganttHeight - 87;
                  let ganttGridHeight = ganttHeight - 90;
                  setTimeout(function () {
                    $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
                    $('.gantt_layout_y').css('height', ganttHeight + 'px');
                    $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
                    $('.gantt_layout_content').css('height', ganttHeight + 'px');
                    $('.gantt_data_area').css('height', ganttGridHeight + 'px');
                    $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
                    $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');
                    //
                  });
                }
                this.refreshData();
              });
            }
            gantt.hideLightbox();
          });
        }
      }
    };

    this.requestPayload = {
      tasks: []
    }

    //.text-ellipsis, 

    $(document).on('change mouseover keyup', '.ellipsis-content, .private-drop, .timeline-checkbox', (e) => {
      ////
      let taskPayload = {
        subTasks: []
      };
      let value = $('#' + e.target.id).val();

      let lookaheadData = gantt.getTask($('#' + e.target.id).attr('data-rowid'));

      //
      if (lookaheadData.timeline) {
        let pendingWorkingDays = lookaheadData.timeline.filter(element => {
          return (!element.id && element.value) || (element.id && element.value == undefined);
        });

        if (pendingWorkingDays.length > 0) {
          sessionStorage.setItem('unsaved', 'true');
        }
      }
      // Two way data binding when keyup action happened
      if (lookaheadData) {
        if (lookaheadData['timeline'] && lookaheadData['timeline'].length > 0) {
          lookaheadData['timeline'].sort(function (a, b) {
            let dateA: any = new Date(a.columnDate), dateB: any = new Date(b.columnDate)
            return dateA - dateB; //sort by date ascending
          });
          let updatedTimeline = lookaheadData['timeline'].filter(data => {
            return data.value != undefined && data.value != "";
          });
          if (updatedTimeline[0] && updatedTimeline[0].value != undefined) {
            lookaheadData['taskStartDate'] = lookaheadData.taskStartDate;
            lookaheadData['taskFinishDate'] = lookaheadData.taskFinishDate;
          }
          else {
            lookaheadData['taskStartDate'] = null;
            lookaheadData['taskFinishDate'] = null;
          }

        }
      }
      // lookaheadData['requiredCompletionDate']=lookaheadData['timeline']
      let lookaheadInfo = JSON.parse(sessionStorage.getItem('lookaheadInfo'));
      if (e.type == 'keyup') {
        this.getAlertDates(lookaheadData, e);
      }
      if (this.requestPayload && this.requestPayload.tasks.length > 0) {
        if (e.target.getAttribute('data-fieldtype')) {
          this.updateFieldtype(value, this.requestPayload, e.target);
        } else {
          let changedTask = this.requestPayload.tasks.filter(task => task.id == e.target.getAttribute('data-tid'));
          if (changedTask.length > 0) {
            let changedSubTask = changedTask[0].subTasks.filter(subTask => subTask.id == e.target.getAttribute('data-staskid'));
            if (changedSubTask.length > 0) {
              let schedules = changedSubTask[0].schedules.filter(schedule => schedule.id == e.target.getAttribute('data-scheduleId'));
              if (schedules.length > 0) {
                schedules[0].workerCount = value;
              } else {
                let checkWorkingDate = changedSubTask[0].schedules.filter(WorkingDate => WorkingDate.workingDate == e.target.getAttribute('data-columndate'));
                if (checkWorkingDate.length > 0) {
                  checkWorkingDate[0]['workerCount'] = value;
                } else {
                  let schedulePayload = that.makingSchedules(value, e.target);
                  changedSubTask[0].schedules.push(schedulePayload);
                }
                ////
              }
              // let filterlookaheadData=lookaheadData.filter(data=>{
              //   return data.taskId ==changedTask[0].id;
              // });
              // //
              // lookaheadData['taskStartDate']=changedSubTask[0].schedules[0].workingDate;
              // lookaheadData['taskFinishDate']=changedSubTask[0].schedules[(changedSubTask[0].schedules.length-1)].workingDate;
            } else {
              ////
              let finalSubTask = that.makingSubTask(value, e.target);
              ////
              ////
              if (e.target.getAttribute('data-staskid') == 'undefined') {
                ////
                let checkParentTask = finalSubTask.schedules.filter(item => item.id == e.target.getAttribute('data-scheduleId'));
                ////
                if (checkParentTask.length > 0) {
                  ////
                } else {
                  ////
                  changedTask[0].subTasks.forEach(item => {
                    ////
                    item.schedules.forEach(schedulesItem => {
                      ////
                      if (schedulesItem.workingDate == finalSubTask.schedules[0].workingDate) {
                        schedulesItem.workerCount = value;
                      }
                    })
                  })
                }
              } else {
                changedTask[0].subTasks.push(finalSubTask);
              }
            }
          } else {
            let newTaskPayload = {
              subTasks: []
            };
            newTaskPayload['id'] = parseInt(e.target.getAttribute('data-tid'));
            let finalSubTask = that.makingSubTask(value, e.target);
            newTaskPayload.subTasks.push(finalSubTask);
            //
            // alert('1 this request payload push');
            this.requestPayload.tasks.push(newTaskPayload);
          }
        }
      } else {
        ////
        if (e.target.getAttribute('data-fieldtype')) {
          ////
          let changedfield = that.makingFieldType(value, e.target);
          ////
          taskPayload = changedfield;
        } else {
          taskPayload['id'] = parseInt(e.target.getAttribute('data-tid'));
          let finalSubTask = that.makingSubTask(value, e.target);
          taskPayload.subTasks.push(finalSubTask);
        }
      }
      if (taskPayload['id'] && taskPayload.subTasks.length > 0) {
        // alert('2 this request payload task payload');
        this.requestPayload.tasks.push(taskPayload);
      }
      ////
    });


    $(document).ready(function () {
      $(".selectall").click(function () {
        $(".individual").prop("checked", $(this).prop("checked"));
        if ($(this).prop("checked")) {
          $(".checkselect").hide();
          $(".unselect").show();
          that.getEmailUsers('selectAll');
        } else {
          $(".checkselect").show();
          $(".unselect").hide();
          that.getEmailUsers('unSelectAll');
        }
      });
      // check git push
      // //select all checkboxes
      // $("#select_all").change(function(){  //"select all" change 
      // var status = this.checked; // "select all" checked status
      // $('.checkbox').each(function(){ //iterate all listed checkbox items
      //   this.checked = status; //change ".checkbox" checked status
      // });
      // });

      // $('.checkbox').change(function(){ //".checkbox" change 
      // //uncheck "select all", if one of the listed checkbox item is unchecked
      // if(this.checked == false){ //if this item is unchecked
      //   $("#select_all")[0].checked = false; //change "select all" checked status to false
      // }

      // //check "select all" if all checkbox items are checked
      // if ($('.checkbox:checked').length == $('.checkbox').length ){ 
      //   $("#select_all")[0].checked = true; //change "select all" checked status to true
      // }
      // });
      $(".individual").click(function () {
        $(this).prop("checked", $(this).prop("checked"));
        $(".selectall").prop("checked", $(this).prop("checked"));
        $('.selectall').removeAttr('checked');
        $('.selectall').prop('checked', false);
        $(".checkselect").show();
        $(".unselect").hide();
      });
      var activeSystemClass = $('.list-group-item.active');
      //something is entered in search form
      $('#system-search').keyup(function () {
        let that = this;
        let tableBody = $('.no-result .text-muted');
        ////
        let tableRowsClass = $('.table-list-search .inner-contains .search-div');
        ////
        $('.search-sf').remove();
        tableRowsClass.each(function (i, val) {
          ////
          ////
          //Lower text for case insensitive
          var rowText = $(val).text().toLowerCase();
          var inputText = $(that).val().toLowerCase();
          if (inputText != '') {
            $('.search-query-sf').remove();
          } else {
            $('.search-query-sf').remove();
          };
          if (rowText.indexOf(inputText) == -1) {
            tableRowsClass.eq(i).hide();
          } else {
            $('.search-sf').remove();
            tableRowsClass.eq(i).show();
          }
        });
        //all tr elements are hidden
        ////
        if (tableRowsClass.children(':visible').length == 0) {
          tableBody.append('<div class="search-sf"><div class="text-muted">No entries found.</div></div>');
          $("#checkSelectAll").css("display", "none");
        } else {
          $("#checkSelectAll").css("display", "block");
        }
      });
    });

    this.ganttService.lookaheadDateInfo(this.projectDetails.projectId).subscribe(success => {
      // //
      sessionStorage.setItem('lookaheadInfo', JSON.stringify(success['payload']));
      /* Set Lookahead Height Start */

      let totalHeight = $(window).height();
      let topSpace = 273;
      let topHeight = totalHeight - topSpace;
      let bottomSpace = 20;
      let ganttHeight = topHeight - bottomSpace;
      let ganttFullHeight = ganttHeight + 2;
      let ganttScrollHeight = ganttHeight - 87;
      let ganttGridHeight = ganttHeight - 90;
      // //
      setTimeout(function () {
        $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
        $('.gantt_layout_y').css('height', ganttHeight + 'px');
        $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
        $('.gantt_layout_content').css('height', ganttHeight + 'px');
        $('.gantt_data_area').css('height', ganttGridHeight + 'px');
        $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
        $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');
        //
      });

      /* Set Lookahead Height end */

      gantt["leaveDays"] = [];
      success['payload']['leaveDays'].forEach(item => {
        this.week.forEach((WeekDay, key) => {
          if (item == WeekDay) {
            gantt["leaveDays"].push(key);
          }
        })
      })
      gantt.projectRole = success['payload']['projectRole'];
      gantt.currentDate = success['payload']['currentDate'];
      this.pStartDate = success['payload']['lookaheadDate'];
      this.currentDate = success['payload']['currentDate'];
      this.projectCompany = success['payload']['projectCompany'];
      this.projectRole = success['payload']['projectRole'];
      this.getTrade = success['payload']['trade'];
      // sessionStorage.setItem('projectTrade',this.getTrade);
      if ((this.projectRole == 'GC-Project Admin' || this.projectRole == 'GC-Project Manager' || this.projectRole == 'SC-Manager')
        && (this.loginDetails.trial || this.loginDetails.licenseAvailable)) {
        this.viewOnlyAccess = true;
      } else {
        this.viewOnlyAccess = false;
      }
      if (this.pStartDate) {
        this.getGanttChart();
      }
      this.onDateChanged(this.pStartDate);
    }, (err) => {
      // //
    });


    $(function () {
      var isMouseDown = false;
      $(document).mouseup(function () {
        let thisVal;
        /* -----mouse down function start -- */
        $(".gantt_task_bg .gantt_task_cell").find("input")
          .mousedown(function () {
            let mousedown = $(this).attr('id');
            isMouseDown = true;
            thisVal = $(this).val();
            $(this).focus();
            let ganttDates = gantt.getTask($(this).attr('data-rowid'));
            // 
            let selectedCellDate = null;
            selectedCellDate = new Date($(this).attr('data-columndate'));
            let requiredCompletionDate = null;
            let deadlineDate = null;
            let minimumDate = null;
            if (ganttDates) {
              if (ganttDates['requiredCompletionDate'] && ganttDates['requiredCompletionDate'] != null) {
                requiredCompletionDate = new Date(ganttDates['requiredCompletionDate']);
              }
              if (ganttDates['deadLineDate'] && ganttDates['deadLineDate'] != null) {
                deadlineDate = new Date(ganttDates['deadLineDate']);
              }
            }
            if (requiredCompletionDate == null) {
              minimumDate = deadlineDate;
            } else if (deadlineDate == null) {
              minimumDate = requiredCompletionDate;
            } else {
              if (requiredCompletionDate < deadlineDate) {
                minimumDate = requiredCompletionDate;
              } else if (requiredCompletionDate > deadlineDate) {
                minimumDate = deadlineDate;
              } else {
                minimumDate = requiredCompletionDate;
              }
            }
            let alertDays = [];
            let lookaheadInfo = JSON.parse(sessionStorage.getItem('lookaheadInfo'));
            if (minimumDate != null) {
              for (let i = 0; i <= lookaheadInfo.floatDays; i++) {
                let d = new Date(minimumDate);
                d.setDate(d.getDate() - i);
                alertDays.push($.datepicker.formatDate("dd-M-yy", new Date(d)));
              }
            }
            if (thisVal == "") {
              $("#" + mousedown).val($("#" + mousedown).val()).removeClass("highlighted");
              $("#" + mousedown).val($("#" + mousedown).val()).removeClass("warning");
              $("#" + mousedown).val($("#" + mousedown).val()).removeClass("behind-schedule");
              $("#" + mousedown).focus();
            } else {
              if (minimumDate != null) {
                if (new Date(alertDays[alertDays.length - 1]) > new Date(selectedCellDate)) {
                  $("#" + mousedown).val(thisVal).addClass('highlighted').focus();
                } else if (new Date(alertDays[0]) < new Date(selectedCellDate)) {
                  $("#" + mousedown).val(thisVal).addClass('behind-schedule').focus();
                } else {
                  if (alertDays.indexOf(selectedCellDate) == -1) {
                    $("#" + mousedown).val(thisVal).addClass('warning').focus();
                  }
                }
              } else {
                $("#" + mousedown).val(thisVal).addClass('highlighted').focus();
              }
            }
            return false;
          })
          /* -----mouse down function end -- */

          /* mouseOver  function start  */
          .mouseover(function () {
            if (isMouseDown) {
              let mouseover = $(this).attr('id');
              $(this).focus();
              let ganttDates = gantt.getTask($(this).attr('data-rowid'));
              let selectedCellDate = null;
              selectedCellDate = new Date($(this).attr('data-columndate'));
              let requiredCompletionDate = null;
              let deadlineDate = null;
              let minimumDate = null;
              if (ganttDates) {
                if (ganttDates['requiredCompletionDate'] && ganttDates['requiredCompletionDate'] != null) {
                  requiredCompletionDate = new Date(ganttDates['requiredCompletionDate']);
                }
                if (ganttDates['deadLineDate'] && ganttDates['deadLineDate'] != null) {
                  deadlineDate = new Date(ganttDates['deadLineDate']);
                }
              }
              /* ---- This should be in common function ---- */
              if (requiredCompletionDate != null && deadlineDate != null) {
                if (requiredCompletionDate < deadlineDate) {
                  minimumDate = requiredCompletionDate;
                } else if (requiredCompletionDate > deadlineDate) {
                  // alert(calculatedDate + "2")

                  minimumDate = deadlineDate;
                } else {
                  // alert(calculatedDate + "3")
                  minimumDate = requiredCompletionDate;
                }
              } else if (requiredCompletionDate == null) {
                // alert(calculatedDate + "4")

                minimumDate = deadlineDate;
              } else {
                // alert(calculatedDate + "5")
                minimumDate = requiredCompletionDate;
              }
              let alertDays = [];
              let lookaheadInfo = JSON.parse(sessionStorage.getItem('lookaheadInfo'));
              if (minimumDate != null) {
                for (let i = 0; i <= lookaheadInfo.floatDays; i++) {
                  let d = new Date(minimumDate);
                  d.setDate(d.getDate() - i);
                  alertDays.push($.datepicker.formatDate("dd-M-yy", new Date(d)));
                }
              }
              if (thisVal == "") {
                $("#" + mouseover).val(thisVal).removeClass("highlighted");
                $("#" + mouseover).val(thisVal).removeClass("warning");
                $("#" + mouseover).val(thisVal).removeClass("behind-schedule");
                $("#" + mouseover).focus();
              } else {
                if (minimumDate != null) {
                  if (new Date(alertDays[alertDays.length - 1]) > new Date(selectedCellDate)) {
                    $("#" + mouseover).val(thisVal).addClass('highlighted').focus();
                  } else if (new Date(alertDays[0]) < new Date(selectedCellDate)) {
                    $("#" + mouseover).val(thisVal).addClass('behind-schedule').focus();
                  } else {
                    if (alertDays.indexOf(selectedCellDate) == -1) {
                      $("#" + mouseover).val(thisVal).addClass('warning').focus();
                    }
                  }
                } else {
                  $("#" + mouseover).val(thisVal).addClass('highlighted').focus();
                }
              }
            }
          });
      });
      $(document)
        .mouseup(function () {
          isMouseDown = false;
        });
    });
    this.filter();
    this.getProjectCompany();
    $(document).ready(() => {
      $(".selectiongroup input[ngtype='text']").prop('readonly', true);
    });

  }
  /*send email get users list integration */
  goEmail(projectDetails) {
    let project = {
      projectId: projectDetails.projectId
    }
    this.restservice.getSharedUsers(project).subscribe(success => {
      this.emailUsers = success['payload'];
      this.emailUsers.forEach(item => {
        item['checked'] = false;
      })
      let tableRowsClass = $('.table-list-search .inner-contains .search-div');
      let tableBody = $('.no-result .text-muted');
      if (this.emailUsers.length == 0) {
        tableBody.append('<div class="search-sf"><div class="text-muted">No entries found.</div></div>');
        $("#checkSelectAll").css("display", "none");
      } else {
        $("#checkSelectAll").css("display", "block");
      }
    })
    this.emailArray = [];
  }

  showPreviousDays(e) {
    this.spinner.show();
    if (sessionStorage.getItem('unsaved') != undefined || sessionStorage.getItem('unsaved') != null) {
      $('#unsavedDatapopup').modal('show');
      sessionStorage.setItem('status', e);
      this.spinner.hide();
    } else {
      // alert(e)
      this.spinner.hide();
      var date = new Date($('#projectStartDate').val());
      if (e == 'previousday') { date.setDate(date.getDate() - 1) }
      else { date.setDate(date.getDate() - 7); }
      $('#projectStartDate').datepicker('setDate', date);
      this.onDateChanged(this.datePipe.transform(date, 'dd-MMM-yyyy'));
    }
  }

  /* PopUp trigger when schedules not saved yet */
  unSavedPopup(title) {
    // alert("alert in un saved popup   "+title)
    if (title == 'dontsave') {
      sessionStorage.removeItem('unsaved');
      let arrowStatus = sessionStorage.getItem('status');
      if (arrowStatus == 'nextDay' || arrowStatus == 'nextWeek') {
        gantt.nextDay(arrowStatus)
      }
      else if (arrowStatus == 'project') {
        this.router.navigate(['/authenticate/list/project']);
      } else if (arrowStatus == 'projectname') {
        this.router.navigate(['/authenticate/edit/project/details']);
      }

      else if (arrowStatus == 'changePassword') {
        this.router.navigate(['./authenticate/changePassword']);
      }
      else if (arrowStatus == 'edit-profile') {
        this.router.navigate(['./authenticate/edit-profile']);
      } else if (arrowStatus == 'landing') {
        this.router.navigate(['/authenticate/list/project']);
      } else if (arrowStatus == 'company') {
        this.router.navigate(['/authenticate/list/company']);
      } else if (arrowStatus == 'user-permission') {
        this.router.navigate(['/authenticate/list/user-permission'])
      } else if (arrowStatus == 'license') {
        this.router.navigate(['/authenticate/list/license'])
      }
      else {
        this.showPreviousDays(arrowStatus);
      }
    } else if (title == 'save') {
      this.submitGanttChart(false, 'popUp');
      sessionStorage.removeItem('unsaved');
      let arrowStatus = sessionStorage.getItem('status');
      if (arrowStatus == 'nextDay' || arrowStatus == 'nextWeek') {
        gantt.nextDay(arrowStatus)
      }
      else if (arrowStatus == 'project') {
        this.router.navigate(['/authenticate/list/project']);
      } else if (arrowStatus == 'projectname') {
        this.router.navigate(['/authenticate/edit/project/details']);
      } else if (arrowStatus == 'changePassword') {
        this.router.navigate(['./authenticate/changePassword']);
      }
      else if (arrowStatus == 'edit-profile') {
        this.router.navigate(['./authenticate/edit-profile']);
      } else if (arrowStatus == 'landing') {
        this.router.navigate(['/authenticate/list/project']);
      } else if (arrowStatus == 'company') {
        this.router.navigate(['/authenticate/list/company']);
      } else if (arrowStatus == 'user-permission') {
        this.router.navigate(['/authenticate/list/user-permission'])
      } else if (arrowStatus == 'license') {
        this.router.navigate(['/authenticate/list/license'])
      }
      else {
        this.showPreviousDays(arrowStatus);
      }

      // if(url == 'changePassword') {
      //   this.router.navigate(['./authenticate/changePassword']);
      // }
      // if(url == 'edit-profile') {
      //   this.router.navigate(['./authenticate/edit-profile']);
      // }

    }
    else {
      sessionStorage.removeItem('unsaved');
    }
  }

  getEmailUsers(index) {
    if (index == 'selectAll') {
      this.emailArray = this.emailUsers.map(a => a.email);
      this.lookaheadSend = false;
    } else if (index == 'unSelectAll') {
      this.emailArray = [];
    } else {
      let idx = this.emailArray.indexOf(this.emailUsers[index].email);
      if (idx == -1) {
        this.emailArray.push(this.emailUsers[index].email);
        this.lookaheadSend = false;
      } else {
        $(".checkselect").show();
        $(".unselect").hide();
        this.emailArray.splice(idx, 1);
        //this.lookaheadSend = true;
      }
    }
  }

  sendProjectPlan(projectDetails) {
    if (this.emailArray.length == '0') {
      this.lookaheadSend = true;
    } else {
      this.lookaheadSend = false;
    }
    // let reqPayload = {
    //   "lookaheadDate": this.pStartDate,
    //   "projectId": this.projectDetails.projectId,
    //   "email": this.emailArray,
    // }
    const reqPayload = {
      "filter": this.filterItem,
      "lookaheadDate": this.pStartDate,
      "projectId": this.projectDetails.projectId,
      "userId": 0,
      "email": this.emailArray,
      "sortType": this.sortType,
      "columnName": this.columname,
      "search": this.checksearch,
      "taskIds": this.taskids,
      "subTaskIds": this.subtaskids
    }

    if (reqPayload.email.length > 0) {
      this.restservice.sendEmail(reqPayload).subscribe(res => {
        if (res['success']) {
          $("#projectPlan").modal('hide');
          this.emitter.getMessage(res['message']);
        }
      }, (err) => {
        $('#projectPlan').modal('hide');
        this.emitter.getMessage(err['error']['message']);
      })
    };
    // else{
    //   this.emitter.getMessage("Please select atleast one team member");
    // }
  }

  public myDatePickerOptions: IMyDpOptions = {
    // other options...
    dateFormat: 'dd-mmm-yyyy',
    showClearDateBtn: false
  };

  onDateChanged(event) {
    // Old Datepicker
    // if(event.formatted) {
    //   this.pStartDate = event.formatted;
    // }
    // New DatePicker
    this.click_pagination = false;
    if (event) {
      this.pStartDate = event;
    }
    this.submitDatePicker(event);
    this.loadGanttChart();
    // this.submitDatePickers(event);
  }

  addDays(date, noDays) {
    date.setDate(date.getDate() + parseInt(noDays));
    return date;
  }

  // Old Submit
  submitDatePickers(dateObj) {
    if (dateObj != null && dateObj.date && dateObj.date.day) {
      let sDate: any = new Date(dateObj.date.month + "." + dateObj.date.day + "." + dateObj.date.year);
      let eDate: any = new Date(sDate);
      eDate = this.addDays(eDate, 28);
      gantt.config.start_date = sDate;
      gantt.config.end_date = new Date(eDate);
    } else {
      let gantEndDate = this.addDays(new Date(this.dateFormat(dateObj)), 28);
      gantt.config.start_date = new Date(this.dateFormat(dateObj));
      gantt.config.end_date = gantEndDate;
    }
    gantt.render();
  }

  /* New Submit  */

  submitDatePicker(dateObj) {

    if (dateObj != null && dateObj) {
      let sDate: any = new Date(dateObj);
      let eDate: any = new Date(sDate);
      eDate = this.addDays(eDate, 28);
      gantt.config.start_date = sDate;
      gantt.config.end_date = new Date(eDate);
    } else {
      let gantEndDate = this.addDays(new Date(this.dateFormat(dateObj)), 28);
      gantt.config.start_date = new Date(this.dateFormat(dateObj));
      //
      gantt.config.end_date = gantEndDate;
    }
    gantt.render();
  }

  /* format the given Date */
  dateFormat(date) {
    let dateObj = date.split('-');
    return dateObj[0] + "," + dateObj[1] + " " + dateObj[2];
  }
  // onload="window.print();window.close()"
  getGanttChart() {
    gantt.config.min_column_width = 20;
    gantt.config.scale_offset_minimal = true;
    gantt.config.row_height = 42;
    gantt.config.scale_height = 90;
    gantt.config.scale_unit = "month";
    gantt.config.date_scale = "%F, %Y";

    // week scalling in gantchart
    var weekScaleTemplate = function (date) {
      var dateToStr = gantt.date.date_to_str("%M %Y");
      var endDate = gantt.date.add(gantt.date.add(date, 1, "week"), -1, "day");
      return dateToStr(date) + " - " + dateToStr(endDate);
    };
    gantt.config.subscales = [
      { unit: "day", step: 1, date: "%D" },
      { unit: "day", step: 1, date: "%d" }
    ];
    // gantchart column inline edition
    var textEditor = { type: "text", map_to: "taskName" };
    var dateEditor = { type: "date", map_to: "start_date", min: new Date(2018, 0, 1), max: new Date(2019, 0, 1) };
    gantt.config.columns = [
      {
        name: "delete", label: '', width: 25, resize: true,
        template: function (task) {
          return "";
        }
      },
      { name: "serialno", label: 'No', width: 30, resize: true, editor: '' },
      { name: "taskName", label: 'Task Description', tree: true, width: 190, resize: true },
      {
        name: "", label: "", width: 28, template: (task) => {
          return "";
        }
      },
      // {
      //   name: "taskType", label: "Task Type",  width: 90, template:  (task)=> {
      //     let options = '<option value="">Select</option>';
      //     this.masterData['task_type'].forEach((data)=>{
      //       options += task.taskType == data ? '<option selected value="'+data+'">'+data+'</option>' : '<option value="'+data+'">'+data+'</option>';
      //     });
      //     return '<select class="task-type private-drop" id="taskType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="taskType" data-rowid="' + task.id + '"> ' + options + ' </select>'
      //   }
      // },
      {
        name: "requiredDate", title: "Required Completion Date", label: "Target.Com..", width: 80, editor: ''
      }
      // , template: (task) => {
      //     let options = '<option value="">Select</option>';
      //     let projectTrade = JSON.parse(sessionStorage.getItem("projectDetails"));
      //     if (projectTrade['trade']) {
      //       projectTrade['trade'].forEach((data) => {
      //         options += task.trade == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option value="' + data + '">' + data + '</option>';
      //       });
      //     }

      //     ////
      //     if (task.trade) {
      //       return '<select class="trade-type private-drop" id="tradeType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="trade" data-rowid="' + task.id + '"  title="' + task.trade + '"> ' + options + ' </select>'
      //     } else {
      //       return '<select class="trade-type private-drop" id="tradeType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="trade" data-rowid="' + task.id + '"> ' + options + ' </select>'
      //     }
      //   }
      // }
    ];
    if (this.viewOnlyAccess == true) {
      gantt.config.columns = [
        {
          name: "delete", label: '', width: 25, resize: true, template: function (task) {
            return "<div class='gantt_delete_btn' title='delete' style='cursor:pointer;' onclick=\"gantt.$click.buttons['delete'](" + task.id + ")\"></div>";
          }
        },
        { name: "serialno", label: 'No', width: 30, resize: true, editor: '' },
        { name: "taskName", label: 'Task Description', tree: true, width: 190, resize: true },
        {
          name: "add", label: "", width: 28, template: function (task) {
            if (task.parent == 0) {
              '<div role="button" title="Add task" aria-label="New task" class="gantt_add" ></div>'
            } else {
              return "inner";
            }
          }
        },
        // {
        //   name: "taskType", label: "Task Type",  width: 90, template:  (task) => {
        //     let options = '<option value="">Select</option>';
        //     this.masterData['task_type'].forEach((data)=> {
        //       options += task.taskType == data ? '<option selected value="'+data+'">'+data+'</option>' : '<option value="'+data+'">'+data+'</option>';
        //     });
        //     return '<select class="task-type private-drop" id="taskType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="taskType" data-rowid="' + task.id + '"> ' + options + ' </select>'
        //     //return '<select class="task-type private-drop" id="taskType" data-fieldtype="taskType" data-rowid="' + task.id + '"> ' + options + ' </select>'
        //   }
        // },
        {
          name: "requiredDate", label: "Target.Com..", title: "R", width: 80, editor: ''
        }
        // let options = '<option value="">Select</option>';
        // let projectTrade = JSON.parse(sessionStorage.getItem("projectDetails"));
        // // //
        // if (projectTrade['trade']) {
        //   projectTrade['trade'].forEach((data) => {
        //     options += task.trade == data ? '<option selected value="' + data + '">' + data + '</option>' : '<option value="' + data + '">' + data + '</option>';
        //   });
        // }
        // ////
        // if (task.trade) {
        //   return '<select class="trade-type private-drop" id="tradeType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="trade" data-rowid="' + task.id + '"  title="' + task.trade + '"> ' + options + ' </select>'
        // } else {
        //   return '<select class="trade-type private-drop" id="tradeType" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="trade" data-rowid="' + task.id + '"> ' + options + ' </select>'
        // }
      ];
    }
    // let mouse_on_grid = false;
    // gantt.templates.tooltip_text = function(start,end,task){
    //   //
    //   return "<b>Task Description:</b> "+task.taskName;
    // };
    // gantt.attachEvent("onMouseMove", function (id, e){
    //   if (e.clientX >= gantt.config.grid_width) mouse_on_grid = true;
    //   else mouse_on_grid = false;
    // }); 

    gantt.locale.labels["section_progress"] = "Progress";
    var opts = [
      { key: 'High', label: 'High' },
      { key: 'Normal', label: 'Normal' },
      { key: 'Low', label: 'Low' }
    ];
    gantt.config.lightbox.sections = [
      { name: "description", height: 38, map_to: "text", type: "textarea", focus: true },
    ];

    // gantchart layout specification
    var secondGridColumns = {
      columns: [
        // {
        //   name: "accessTo", label: "Access To", template:  (task) => {
        //     let options = '';
        //     this.masterData['visibility'].forEach((data)=>{
        //       options += task.accessTo == data ? '<option selected value="'+data+'">'+data+'</option>' : '<option value="'+data+'">'+data+'</option>';
        //     });
        //     return '<select class="private-drop" id="accessTo" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="accessTo" data-rowid="' + task.id + '"> ' + options + ' </select>'
        //   }
        // },
        {
          name: "ballinthecourt", label: "Responsibility", align: "center", template: (task) => {
            if (gantt.projectRole == 'GC-Project Admin' || gantt.projectRole == 'GC-Project Manager') {
              const gcCourt = task.ballInCourt ? task.ballInCourt : this.projectCompany
              // let options = '<option value="">select</option>';
              let options;
              this.projectAssociatedCompany.forEach((data) => {
                options += task.ballInCourt == data.name ? '<option selected value="' + data.name + '">' + data.name + '</option>' : '<option value="' + data.name + '">' + data.name + '</option>';
              });
              return '<select class="private-drop responsibility-class" id="ballInCourt" data-parent="' + task.parent + '" data-tid="' + task.taskId + '" data-staskid="' + task.subTaskId + '" data-fieldtype="ballInCourt" data-rowid="' + task.id + '"  title="' + gcCourt + '"> ' + options + ' </select>'
            } else {
              let _ballintthecourtVal = task.ballInCourt ? task.ballInCourt : this.projectCompany;
              return '<input style="width:90px; height:15px;" class="ballinthecourt disble-responsibility" data-fieldtype="ballInTheCourt" data-rowid="' + task.id + '" value="' + _ballintthecourtVal + '" type="text" style="width:70px;padding:0px;height:30px;" title="' + task.ballInCourt + '">'
            }
          }
        },
        {
          name: "note", label: "Notes", align: "center", template: function (task) {
            let _notesVal = task.note ? task.note : "";
            return '<input style="width:100px; height:15px;" type="text" class="notes private-drop" id="notes" data-fieldtype="notes" data-rowid="' + task.id + '" value="' + _notesVal + '" >'
          }
        },
      ]
    };
    // gantt.config.columns = [secondGridColumns];
    gantt.config.layout = {
      css: "gantt_container",
      cols: [
        {
          width: "344",
          //width: "25%",
          css: "heygrid",
          rows: [
            { view: "grid", scrollX: "gridScroll", scrollable: true, scrollY: "scrollVer" },
            { resizer: true, width: 1 },
          ]
        },
        {
          width: "600",
          //width: "50%",
          rows: [
            { view: "timeline", scrollX: "scrollHor", scrollY: "scrollVer" },
            { view: "scrollbar", id: "scrollHor", group: "horizontal" },
          ],
        },
        {
          view: "grid",
          width: "277",
          //width: "25%",
          bind: "task", scrollY: "scrollVer", config: secondGridColumns
        },
        { view: "scrollbar", id: "scrollVer", }
      ]
    };
    //gantchart displaying
    gantt.init(this.ganttContainer.nativeElement);
    gantt.clearAll();
    
    this.loadGanttChart();
  }

  filter() {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.maxHeight) {
          content.style.maxHeight = null;
        } else {
          content.style.maxHeight = content.scrollHeight + "px";
        }
      });
    }
  }

  /* expand collapse for filter */
  expandCollapse(id) {
    if (!$("#" + id).hasClass('open')) {
      $("#" + id).addClass('open');
      $("#" + id + "Child").addClass('show');
    }
    else {
      $("#" + id).removeClass('open');
      $("#" + id + "Child").removeClass('show');
    }
  }

  getProjectCompany() {
    let projectId = { 'projectId': this.projectDetails.projectId };
    this.restservice.projectCompany(projectId).subscribe((success) => {
      this.projectCompanyList = success['payload']['projectCompanies'];
      sessionStorage.setItem('projectCompanyList', JSON.stringify(this.projectCompanyList));
    }, err => {
    });
  }

  /* get Filtered Data From DB */
  lookaheadFilter(type, value?, bol?) {
    this.click_pagination = false;
    this.checkedValue.push(value)
    this.filterArray = [];
    // this.taskids = this.getAllTask.taskIds;
    // this.subtaskids = this.getAllTask.subTaskIds;
    let totalHeight = $(window).height();
    let topSpace = 273;
    let topHeight = totalHeight - topSpace;
    let bottomSpace = 20;
    let ganttHeight = topHeight - bottomSpace;
    let ganttFullHeight = ganttHeight + 2;
    let ganttScrollHeight = ganttHeight - 87;
    let ganttGridHeight = ganttHeight - 90;
    setTimeout(function () {
      $('.gantt_layout_root').css('height', ganttFullHeight + 'px');
      $('.gantt_layout_y').css('height', ganttHeight + 'px');
      $('.gantt_layout_cell_border_transparent').css('height', ganttHeight + 'px');
      $('.gantt_layout_content').css('height', ganttHeight + 'px');
      $('.gantt_data_area').css('height', ganttGridHeight + 'px');
      $('.gantt_grid_data').css('height', ganttGridHeight + 'px');
      $('.gantt_ver_scroll').css('height', ganttScrollHeight + 'px');
    });

    /* condition for clear button */
    if (type == 'clear') {
      // this.currentvalue = [];
      this.selectedValue = "";
      this.query = "";
      this.searchedArray = [];
      this.columname = "";
      this.checksearch = false;
      this.searcharray = [];
      this.taskids = [];
      this.subtaskids = [];
      this.filterArray = [];
      this.filterItem = {
        "taskStatus": [],
        "accessibility": [],
        "ballInCourt": [],
        "task": [],
        "trade": [],
        "specDivision": [],
        "scheduleAvailability": "All"
      }
      for (var property in this.filterItem) {
        if (property == 'scheduleAvailability') {
          $(function () {
            var $radios = $('input:radio[value="all"]');
            if ($radios.is(':checked') === false) {
              $radios.filter('[value=all]').prop('checked', true);
            }
          });
        }
        if (property == 'trade') {
          $("#" + property + "Child" + " > div > div > label > input").prop("checked", false);
        } else if (property == 'specDivision') {
          $("#" + property + "Child" + " > div > div > label > input").prop("checked", false);
        } else {
          $("#" + property + "Child" + " > div > label > input").prop("checked", false);
        }
      }
    } else if (type == 'scheduleAvailability') {
      this.filterArray = [];
      if (!bol)
        this.filterItem[type] = "All";
      else
        this.filterItem[type] = value;
    } else {
      let index = this.filterItem[type].indexOf(value)
      if (index == -1) {
        this.filterItem[type].push(value);
      } else {
        this.filterArray = [];
        this.filterItem[type].splice(index, 1);
      }
    } this.refreshData();
  }

  /* Get all master Data from DB including filters */
  getMasterData() {
    this.masterData = this.route.snapshot.data['masterData']['payload'];
    gantt['taskType'] = this.masterData['task_type'];
    gantt['visibility'] = this.masterData['visibility'];
  }

  /* get Associated Company name */
  getProjectCompanyDetail() {
    // alert("getProjectCompanyDetail()")
    this.projectAssociatedCompany = this.route.snapshot.data['projectCompanyDetails']['payload']['projectCompanies'];
  }

  /* get the notesDesctiption in Gantt */
  getTaskDescriptionNotes() {
    // alert("notes")
    let data = gantt.serialize().data;
    this.requestPayload.tasks.forEach(reqItem => {
      data.forEach(dataItem => {
        if (reqItem.id == dataItem.taskId) {
          if (dataItem.parent == '0') {
            reqItem["description"] = dataItem.taskName;
            reqItem["note"] = dataItem.note;
          }
          reqItem.subTasks.forEach(reqSubTask => {
            if (reqSubTask.id == dataItem.subTaskId) {
              reqSubTask["description"] = dataItem.taskName;
              reqSubTask["note"] = dataItem.note;
              reqSubTask["accessTo"] = dataItem.accessTo;
              reqSubTask["taskType"] = dataItem.taskType;
            }
          })
        }
      });
    });
  }

  /* test notification */

  // testNotification() {
  //   // alert(this.baseUrl)
  //   this.http.get(this.baseUrl+'/pmc/taskSummaryMail/' + this.projectDetails.projectId).subscribe(dataInNotify => {
  //     // //
  //     if (!dataInNotify['success']) {
  //       alert("No Task Found")
  //     } else if (dataInNotify['success']) {
  //       alert("Mail Sent Successfully")
  //     } else {
  //       //
  //     }

  //   })

  // }

  /* Arrange a gantt data in pdf Format */
  printpdf() {
    const data = {
      "filter": this.filterItem,
      "lookaheadDate": this.pStartDate,
      "projectId": this.projectDetails.projectId,
      "userId": 0,
      "sortType": this.sortType,
      "columnName": this.columname,
      "search": this.checksearch,
      "taskIds": this.taskids,
      "subTaskIds": this.subtaskids
    }
    this.restservice.downloadPdf(data).subscribe(success => {
      const file = new Blob([success], { type: 'application/pdf' });
      let filename = 'lookahead.pdf';
      FileSaver.saveAs(file, filename);
    }, (err) => {
      if (err) {
        this.emitter.getMessage('No scheduled Task and sub Task in Lookahead');
      }
    });
  }

  /* arrange a Gantt Data in excel format */
  arrangeAsExcelFormat(data) {
    let totalData = [];
    let mainWorkDate = [];
    data.forEach((element, key) => {
      //if (element.child) {
      let data = {};
      data['id'] = element.id;
      data["Task ID"] = element.serialno.toString();
      data["Task Title"] = element.taskName;
      data["Status"] = "On Track";
      data["Assigned to"] = element.ballInCourt;
      data["Description"] = element.note;
      if (element.timeline && element.timeline.length > 0) {
        let workDate = [];
        element.timeline.forEach(timelineEle => {
          let x = new Date(timelineEle.columnDate);
          workDate.push(x);
        });
        let mindate = new Date(Math.min.apply(null, workDate));
        let maxdate = new Date(Math.max.apply(null, workDate));
        data["Start Date"] = this.datePipe.transform(mindate, 'dd-MMM-yyyy');
        data["Due Date"] = this.datePipe.transform(maxdate, 'dd-MMM-yyyy');
      }

      totalData.push(data);
      //}
      // if(element.subTasks && element.subTasks.length>0){
      //   element.subTasks.forEach(subElement => {
      //     let workDate = [];
      //     subElement.schedules.forEach(schedules => {
      //       let x = new Date(schedules.workingDate);
      //       workDate.push(x);
      //     });
      //     let mindate = new Date(Math.min.apply(null, workDate));
      //     let maxdate = new Date(Math.max.apply(null, workDate));
      //     let subData = {}
      //     subData["Task ID"] = element.child ? subElement.serialno : element.serialno;
      //     subData["Task Title"] = subElement.taskName;
      //     subData["Status"] = "On Track";
      //     subData["Assigned to"] = subElement.ballInCourt;
      //     subData["Description"] = subElement.note;
      //     subData["Start Date"] = this.datePipe.transform(mindate, 'dd-MMM-yyyy');
      //     subData["Due Date"] = this.datePipe.transform(maxdate, 'dd-MMM-yyyy');
      //     totalData.push(subData);
      //     //let afterOrderData = this.as.transform(totalData, 'asc');
      //   });
      // }
    });
    this.taskList = this.ascDesc.transform(totalData, 'asc', 'id');
    this.taskList.forEach(finalItem => {
      delete finalItem.id
    });
  }

}