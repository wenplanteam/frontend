import { Component, OnInit, Input } from '@angular/core';
import {RestserviceService} from '../restservice.service';
import {ActivatedRoute, Router} from '@angular/router';
import { EmitterService } from '../emitter.service';

declare let $:any;
@Component({
  selector: 'app-view-page',
  templateUrl: './view-page.component.html',
  styleUrls: ['./view-page.component.scss'],
 
})
export class ViewPageComponent implements OnInit {
  public routingId:any;
  public result:any;
  public resultData:any;
  public pageData:any;
  public tableData:any;
  public subscription:any;
  public advancedSearch:any;
  public buttonData: any;
  public header: any;
  public updateCustom: any;
  public customList: any;
  public deleteData: string [] = [];
  public privileges: any;
  public resultOrg: any;
  public page: any;
  public fetchRestServiceUrl: any;
  public filteredData: any;

  constructor(public restservice:RestserviceService, public router: Router, public route:ActivatedRoute, public emitter: EmitterService) { 
  }

  ngOnInit() {
    console.log("this.deleteData", this.deleteData);
  this.emitter.routerUrlSource.subscribe(item=>{
    this.emitter.getDataSource(this.deleteData);
  });
  //console.log("this.router", this.router.url);
  if(this.router.url.match("view")){
    this.page = "view";
  }
  let id;
  this.emitter.getRouterUrl();
  this.route.params.subscribe(params=>{
    id =params['id'];
    this.routingId=id;
  })
  this.getViewJson(this.fetchRestServiceUrl,id);

  this.subscription = this.emitter.currentMessage
  .subscribe(item => {
    if(item){
      this.deleteData=[];
      let pageName = {};
      this.getResponseData(this.fetchRestServiceUrl,pageName);
      console.log("pageName",pageName);
    }
  });

  $(document).ready(function(){
    $("#myBtn").click(function(){
      $("#myModal").modal();
    });
});
  }
  
  getViewJson(fetchRestServiceUrl,pageName){
    this.restservice.getRenderingJsonView(fetchRestServiceUrl,pageName).subscribe(success=>{
      let result = success;
      //let result = JSON.parse(success['payload']);
      //this.advancedSearch =result['advancedSearch'];
      this.fetchRestServiceUrl=result['fetchRestServiceUrl'];
     // this.filterData = result['filter'];
      this.buttonData = result;
      console.log("buttonData",this.buttonData);
      // result['panels'].forEach(item =>{
      //   this.header = item.items;
      //   this.result=item;
      //  })
      this.result=success;
      this.pageData = result;
      if(success) {
        this.getResponseData(this.fetchRestServiceUrl,pageName);
      }
  },err=>{
  });
  }
  getResponseData(fetchRestServiceUrl,pageName){
    console.log("fetchRestServiceUrl",fetchRestServiceUrl);
    console.log("pageName",pageName);
    this.restservice.getResponseData(fetchRestServiceUrl,pageName,{}).subscribe(success=>{
      this.filteredData = success['payload'];
      console.log('this.filteredData',this.filteredData);
    //  this.pagination = success['pagination'];
      if(success['message'] == "Not Found" || success['payload'].length === 0){
        this.resultData = [];
      }
      if(success['payload']) {
        this.resultOrg =success['payload'];
        this.result['responseObj']=success['payload'];
      }
    })
   }

}
