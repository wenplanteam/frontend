import { Component, OnInit, Input ,EventEmitter,Output} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { RestserviceService } from '../restservice.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EmitterService } from '../emitter.service';
import {FormValidationService} from '../form-validation.service';

@Component({
  selector: 'app-create-page',
  templateUrl: './create-page.component.html',
  styleUrls: ['./create-page.component.scss'],
  providers: [FormBuilder]
})
export class CreatePageComponent implements OnInit {

  form: FormGroup;
  public formHandler: any;
  public panels: any;
  public page: any;
  public routepage: any;
  public panelItems: any;
  public operation: any;
  public formObj;
  public panelData: any;
  public validation: any;
  @Output() notifyPanel: EventEmitter<string> = new EventEmitter<string>();

  constructor(public restservice: RestserviceService, private router: Router, private route: ActivatedRoute,
    public emitter: EmitterService, public fb: FormBuilder, public formValidation: FormValidationService) {
    this.formHandler = fb;
  }

  ngOnInit() {
    sessionStorage.removeItem("listId");
    this.emitter.getRouterUrl();
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    // console.log("router", this.router.url);
    let ss = this.router.url.split('/');
    console.log("ss", this.router.url);

    this.routepage = ss[ss.length-1];
    // console.log("this.routepage", this.routepage);

    this.emitter.projectDetails.subscribe(item=>{
      // console.log("item projectDetails", item);
      sessionStorage.setItem("projectCreate", JSON.stringify(item));
      if(this.routepage != "new-company"){
        sessionStorage.setItem("projectDetails", JSON.stringify(item));
      }
    });

    this.emitter.loginDetailData.subscribe(item=>{
      // console.log("items",item);
    })

    if(this.router.url.match("create")){
      this.page = "create";
    }
    this.getJsonDataForProject(this.page, this.routepage);
  }

  onhandleUserUpdated(e) {
   // alert()
    this.notifyPanel.emit(e);
  }

/* Remove visible false from rendering json*/

removeByAttr(arr, attr, value) {
  var i = arr.length;
  while(i--){
     if( arr[i] 
         && arr[i].hasOwnProperty(attr) 
         && (arguments.length > 2 && arr[i][attr] === value ) ){
         arr.splice(i,1);
     }
  }
  return arr;
}

/* Get UI Rendering JSON for create from Server */

getJsonDataForProject(operation, pageName){
  this.restservice.getRenderingJson(operation, pageName).subscribe(success =>{
    // console.log("success create", success);
    const buttonData = success;
    this.operation = buttonData;
    this.emitter.sendOperation(this.operation);
    this.panelData =buttonData;
    this.panelData.panels.forEach(item =>{
      this.removeByAttr(item.items, 'visible', '');
    })
    this.panels = this.panelData['panels'];
    // console.log("this.panels create", this.panels);
    this.panels.forEach(item =>{
      item['items'].forEach(panelItem =>{
        if(panelItem.name == 'newCompanyName'){
          let l = sessionStorage.getItem("newCompany");
          panelItem.value = l;
        }
      })
    });    
    this.form =this.formHandler.group(this.formValidation.toFormGroup(this.panels));
    // console.log("this.form create page", this.form);
  }, (err) =>{
    console.log("err", err);
  });
}

/* Add custom Field */
add(event){
  this.panels.panels.push(this.panels.panels[0]);
}
}