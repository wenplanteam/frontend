import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InputSelectboxComponent } from './input-selectbox.component';

describe('InputSelectboxComponent', () => {
  let component: InputSelectboxComponent;
  let fixture: ComponentFixture<InputSelectboxComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InputSelectboxComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InputSelectboxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
