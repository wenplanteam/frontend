import { Component, OnInit,Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-input-textarea',
  templateUrl: './input-textarea.component.html',
  styleUrls: ['./input-textarea.component.scss'],
  
})
export class InputTextareaComponent implements OnInit {

  @Input() textareaData;
  @Input() form: FormGroup;

  constructor() { }

  ngOnInit() {
    
  }

}
