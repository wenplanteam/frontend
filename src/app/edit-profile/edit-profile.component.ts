import { Component, OnInit } from '@angular/core';
import { RestserviceService } from '../restservice.service';
import { EmitterService } from '../emitter.service';
import { ActivatedRoute, Router } from '@angular/router';
declare const $:any;
@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.component.html',
  styleUrls: ['./edit-profile.component.scss']
})
export class EditProfileComponent implements OnInit {

  constructor(public restservice: RestserviceService, public emitter: EmitterService, public router: Router) { }
  public userDetails: any = {
    'firstName': '',
    'lastName': '',
    'companyName': '',
    'email': '',
    'contactNo': ''
  };
  public timeZone: any;

  ngOnInit() {
    this.restservice.timeZone().subscribe(success =>{
      console.log('success timeZone', success['payload']);
      this.timeZone = success['payload'];
    }, (err)=>{
      console.log('err', err)
    })
    this.userDetails = JSON.parse(sessionStorage.getItem('loginDetails'));
    console.log('USER DETAILS', this.userDetails);
  }


  saveProfile() {
    $(".cus-tooltip").removeAttr("data-original-title");
    //console.log('this.userDetails', this.userDetails);
    if (this.userDetails.lastName !== '') {
      this.restservice.saveProfile(this.userDetails).subscribe(success => {
        console.log('success xx', success);
        this.emitter.getMessage(success['message']);
        const ss = JSON.parse(sessionStorage.getItem('loginDetails'));
          ss.firstName = success['payload']['firstName'];
          ss.lastName = success['payload']['lastName'];
          ss.companyName = success['payload']['companyName'];
          ss.email = success['payload']['email'];
          ss.contactNo = success['payload']['contactNo'];
          ss.timeZone = success['payload']['timeZone'];
          this.emitter.getLoginDetails(ss);
          sessionStorage.setItem('loginDetails', JSON.stringify(ss));
          this.router.navigateByUrl('/home', { skipLocationChange: true }).then(() => {
            this.router.navigate(['/authenticate/edit-profile']);
          });
      }, (err) => {
        console.log('err', err);
      });
    }
  }

}
