import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpRequest, HttpParams } from '@angular/common/http';
import { Observable, empty } from 'rxjs';
import {GanttData} from '../model/gantt.model';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import 'rxjs/add/operator/map';



const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

@Injectable()
export class GanttServices {
    public basicUrl=environment.baseUrl;
   
    constructor(private http: HttpClient) { }

    getData(){
    var xhr = new XMLHttpRequest();
		xhr.open("GET", this.basicUrl+'getTaskDetails');
		xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send();
    xhr.onreadystatechange = function() {
			if (this.readyState == 4 && this.status == 200) {
        return this;
			}
    }
  }



   
//   transform(value:  Array<any>, args?: any): any {
//      let arr = []
//     if (!args || args === null ||  args === "") {
//       alert("Please Enter Any words to Search")
//     }else {
         
          
//       return value.filter((val) => {
//         //  console.log("val", val.taskName.toLowerCase())
//         // console.log("dd", args.toLowerCase())
// // 
//           //  console.log("val", value)
//         let rVal =   (val.taskName && val.taskName.replace(/\s/g, "").toLowerCase().includes(args.replace(/\s/g, "").toLowerCase())) ||
//                      (val.taskType && val.taskType.replace(/\s/g, "").toLowerCase().includes(args.replace(/\s/g, "").toLowerCase())) ||
//                      (val.serialno && val.serialno.toString().replace(/\s/g, "").toLowerCase().includes(args.replace(/\s/g, "").toLowerCase())) ||
//                       (val.ballInCourt && val.ballInCourt.replace(/\s/g, "").toLowerCase().includes(args.replace(/\s/g, "").toLowerCase())) || 
//                       (val.note && val.note.replace(/\s/g, "").toLowerCase().includes(args.replace(/\s/g, "").toLowerCase()))


//                       // console.log(rVal)

//                       if(rVal){
//                         // alert("yes")
//                         arr.push(value)
//                         return arr

//                       } else{
//                         alert("mo")
//                         return arr
//                       }

//         // return rVal
           
//       })
    

//     }
  
//   }


  addTask(data):Observable<GanttData>{
    return this.http.post<GanttData>(this.basicUrl+'pmc/lookahead', data);    
  }

  updateTask(data):Observable<GanttData>{
    return this.http.put<GanttData>(this.basicUrl+'pmc/lookahead', data);    
  }

  updateLookAheadPlan(subTaskId):Observable<GanttData>{
    return this.http.post<GanttData>(this.basicUrl+'deleteSubTaskIds', subTaskId);    
  }

  getTask(data){
    let jsonUrl = this.basicUrl+'pmc/lookaheads';
    return this.http.post(jsonUrl, data);
  }
  

  lookaheadDateInfo(data){
    let url = this.basicUrl+'pmc/lookaheadDateInfo?projectId='+data;
    return this.http.get(url);
  }

  lookaheadDeleteTask(task){
    let url;
    if(task.parent === 0){
      url = this.basicUrl+'pmc/lookaheadById?taskId='+task.taskId;
    }else{
      url = this.basicUrl+'pmc/lookaheadById?subTaskId='+task.subTaskId;
    }
    return this.http.delete(url);
  }
}
