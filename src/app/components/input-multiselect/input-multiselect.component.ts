import { Component, OnInit, Input, EventEmitter,Output} from '@angular/core';
import { FormGroup, FormBuilder, Validators} from '@angular/forms';
import { RestserviceService } from '../../restservice.service';
import {FormControl} from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-input-multiselect',
  templateUrl: './input-multiselect.component.html',
  styleUrls: ['./input-multiselect.component.scss']
})
export class InputMultiselectComponent implements OnInit {

  @Input() multiselectData: any;
  @Input() selectedData: any;
  @Input() form: FormGroup;
  @Input() pageData: any;
  @Input() panelItem: any
  @Input() noUsers: boolean
  public routepage: any;
  public formHandler: any;
  public restUrl: any;
  public module: any;
  public dropdownList = [];
  public dropdownSettings = {};
  public validation: any;
  public selectedoptions = [{}]
  public masterSelected: boolean;
  @Input() public checklist: any = [];
  multiselectDatas: any;
  checkedList: any = [];
  @Output() sendEmailList = new EventEmitter<string>();

  
  constructor(public restservice: RestserviceService, private route: ActivatedRoute, public formBuilder: FormBuilder) { 
    this.formHandler = formBuilder;
  }

  ngOnInit() {
    console.log("selectedAccoynr", this.selectedData);
    this.validation = JSON.parse(sessionStorage.getItem("validationMessages"));
    this.multiselectDatas = JSON.parse(sessionStorage.getItem("filterItem"));
    console.log("multiselectData",this.multiselectDatas);
    this.route.params.subscribe(params => {
      this.routepage = params.id;
    });
    if(this.multiselectData){
      this.restUrl = this.multiselectData.restServiceUrl;
    
    if(this.multiselectData.options && this.multiselectData.options.length == "0"){
      this.restservice.getOptions(this.restUrl).subscribe(success =>{
        this.dropdownList = success['payload']?success['payload']:success;
        console.log(this.dropdownList,'dropdownList')
      }, error => {
        console.log("error", error);
      })
    }else {
        this.dropdownList =this.multiselectData.options;
    }     
  }
}

  /* CheckAll UnCheckAll fuction */
  checkUncheckAll(data) {
    for (var i = 0; i < this.checklist.length; i++) {
      this.checklist[i].isSelected = this.masterSelected;
    }
    this.getCheckedItemList(data);
  }
  /* single selection function */
  isAllSelected(data) {
    let checkedValue = []
    this.masterSelected = this.checklist.every((item: any) => {
      return item.isSelected == true;
    })
    checkedValue.push(data)
    this.getCheckedItemList(checkedValue);
  }

  /* maintain the checked Value */
  getCheckedItemList(data) {
    console.log(data, "started to push array")
    for (var i = 0; i < data.length; i++) {
      if (data[i].isSelected) {
        this.checkedList.push(data[i].email);
      } else if (!data[i].isSelected) {
        this.checkedList.pop(data[i].email);
      }
    }
    console.log("checklist", this.checkedList)
    this.sendEmailList.emit(this.checkedList);
  }

  sendMessageToParent(message: string) {
    this.noUsers = false;
    this.sendEmailList.emit(message);
  }

}















































