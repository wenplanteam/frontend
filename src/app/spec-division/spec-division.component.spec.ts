import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SpecDivisionComponent } from './spec-division.component';

describe('SpecDivisionComponent', () => {
  let component: SpecDivisionComponent;
  let fixture: ComponentFixture<SpecDivisionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SpecDivisionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SpecDivisionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
