import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PercentageCompletionComponent } from './percentage-completion.component';

describe('PercentageCompletionComponent', () => {
  let component: PercentageCompletionComponent;
  let fixture: ComponentFixture<PercentageCompletionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PercentageCompletionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PercentageCompletionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
