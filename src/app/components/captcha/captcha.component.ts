import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmitterService } from '../../emitter.service';
declare let $: any;
@Component({
  selector: 'app-captcha',
  templateUrl: './captcha.component.html',
  styleUrls: ['./captcha.component.scss'],
  providers: [FormBuilder]
})
export class CaptchaComponent implements OnInit {
  @Input() captchaData: any;
  @Input() form: any;
  @Input() pageData: any;
  public queryCaptchaFeed: any;
  public txtCaptcha: any;
  public captchaField: any;
  public validation: any;
  formHandler: any;
  public checkCaptcha: any;
  constructor(public emitter: EmitterService, public fb: FormBuilder) {
    this.formHandler = fb;
  }

  ngOnInit() {
    this.validation = JSON.parse(sessionStorage.getItem('validationMessages'));
    console.log('captchaData', this.captchaData);
    console.log('form', this.form);
    console.log('pageData', this.pageData);
    this.getInTouchCaptcha();

    this.emitter.captchaEmittData.subscribe(item =>{
      console.log('item', item);
      this.checkCaptcha = item;
      this.getInTouchCaptcha();
    })
  }
  removeCheckCaptcha(){
    console.log('Hi removeCheckCaptcha');
    this.checkCaptcha = false;
  }
  getInTouchCaptcha() {    
    this.queryCaptchaFeed = "";
    let alpha = new Array('1','2','3','4','5','6','7','8','9','A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z');
    let a = alpha[Math.floor(Math.random() * alpha.length)];
    let b = alpha[Math.floor(Math.random() * alpha.length)];
    let c = alpha[Math.floor(Math.random() * alpha.length)];
    let d = alpha[Math.floor(Math.random() * alpha.length)];
    let e = alpha[Math.floor(Math.random() * alpha.length)];
    let f = alpha[Math.floor(Math.random() * alpha.length)];
    let g = alpha[Math.floor(Math.random() * alpha.length)];
    let code = a + b + c + d + e + f + g;
    this.captchaData.captchaValue = code;
  }
}
